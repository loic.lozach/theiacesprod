#!/usr/local/bin/python
# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.


import argparse
from subprocess import Popen, PIPE


PROJECTDIR="/work/python/"

def process_command(cmd):
    print("Starting : "+" ".join(cmd))
    
    cmd[1]=PROJECTDIR+cmd[1]

    p = Popen(cmd, stdout=PIPE)
#    p.wait()
    output = p.communicate()[0]
    if p.returncode != 0: 
        print("process failed %d : %s" % (p.returncode, output))
    print("#################################################")
    return p.returncode

    
if __name__ == "__main__":
    # Make parser object
    list_parser = argparse.ArgumentParser(description=
        """
        "Batch inversion pipeline over a directory of Sentinel-1 images"
        """)
    
    list_parser.add_argument('-modeldir', action='store', required=True, help='Directory to find both wet and dry tensorflow model')
    list_parser.add_argument('-modelchoice', action='store', required=False, default="dry", help='[Optional] dry, wet or text file resulting from gpm pipeline, default dry')
    list_parser.add_argument('-s1orthodir', action='store', required=True, help='Directory to find orthorectified Sentinel-1 .TIF images, (regex: "S1X_IW_GRDH_[ZONE]_AAAAMMDDTHHMMSS_[POLAR].TIF) ')
    list_parser.add_argument('-ndvidir', action='store', required=True, help='Monthly NDVI image directory computed from Sentinel-2 (regex: "NDVIXXX_[ZONE]_AAAAMMDD.TIF)')
    list_parser.add_argument('-filtlabels', action='store', required=True, help='Filtered agricultural plot labels image')
    list_parser.add_argument('-zone', action='store', required=True, help='Geographic zone reference for output files naming')
    list_parser.add_argument('-slope', action='store', required=False, help='[Optional] Slope image to filter SarVV')
    list_parser.add_argument('-slopevalue', action='store', required=False, default=20, help='[Optional] Max value of slope to use to filter SarVV, default 20')
    list_parser.add_argument('-polar', choices=['vv', 'vh','vvvh'],  default='vv', required=False, help='[Optional] Choose between VV, VH modes or both for input SAR image, default vv')
    list_parser.add_argument('-incid', choices=['loc','elli'], required=False, default="loc", help="[Optional] Use Local or from ellipsoid incidence angle image, default loc")
    list_parser.add_argument('-outformat', choices=['raster', 'csv'],  default='raster', required=False, help='[Optional] Format of output when calling otbcli_InvertSARModel, cvs or raster')
    list_parser.add_argument('-outdir', action='store', required=True, help='Directory of Soil moisture map over agricultural areas results')
    list_parser.add_argument('-overwrite', dest='overwrite', action='store_true', default=False, required=False, help='[Optional] Overwrite already existing files (default False)')
    
    args=list_parser.parse_args()
    
    cmd=["python", "manage.py", "runscript", "ServiceWrapper", "--traceback", "--script-args",
          "production", 
          "modeldir@"+args.modeldir,
          "modelchoice@"+args.modelchoice,
          "s1orthodir@"+args.s1orthodir,
          "ndvidir@"+args.ndvidir,
          "labels_file@"+args.filtlabels,
          "mrgstile@"+args.zone,
          "slope@"+args.slope,
          "slopevalue@"+str(args.slopevalue),
          "polar@"+args.polar,
          "incid@"+args.incid,
          "outformat@"+args.outformat,
          "outdir@"+args.outdir,
          "overwrite@"+str(int(args.overwrite))
        ]
    process_command(cmd)