from django.contrib.gis.db import models
from django.utils import timezone

# class EodagProduct(models.Model):
#     
#     # Returns the string representation of the model.
#     def __str__(self):
#         return self.id_parcel

class ParcelleAbstract(models.Model):
    pub_year = models.PositiveIntegerField()

    class Meta:
        abstract = True

class ParcelleGraphique(ParcelleAbstract):
    id_parcel = models.CharField(max_length=10)
    # surf_parc = models.FloatField()
    code_cultu = models.CharField(max_length=3)
    code_group = models.CharField(max_length=2)
    # culture_d1 = models.CharField(max_length=3, null=True, blank=True)
    # culture_d2 = models.CharField(max_length=3, null=True, blank=True)
 
    # GeoDjango-specific: a geometry field (MultiPolygonField)
    mpoly = models.MultiPolygonField(srid=4326)
 
    # Returns the string representation of the model.
    def __str__(self):
        return self.id_parcel

class PracelleTypeTransform(ParcelleAbstract):
    rpg_code_group = models.CharField(max_length=2)
    freeze_parcel_type = models.PositiveIntegerField()

    def __str__(self):
        return self.rpg_code_group + " => " + str(self.freeze_parcel_type)
        

class TileEnvelope(models.Model):
    # fid = models.BigIntegerField()
    # area = models.FloatField()
    # perimeter = models.FloatField()
    tile = models.CharField(max_length=10)
    geom = models.PolygonField(srid=4326)
    
    parcelles = models.ManyToManyField(ParcelleGraphique)
    
    def __str__(self):
        return self.tile + ":"+str(self.geom)

class Sentinel1Image(models.Model):
    tile = models.ForeignKey(TileEnvelope, on_delete=models.CASCADE)
    sentinel1_date = models.DateTimeField()
    sentinel1_pltf = models.CharField(max_length=3,null=True,blank=True)  
    sentinel1_orbit = models.CharField(max_length=3,null=True,blank=True)
    processed = models.BooleanField(default=False) 
    
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["sentinel1_date", 'tile'], 
                             name='datepartile_unique'
                             ),
            ]
        
    def __str__(self):
#         tz = timezone.utc
#         utcsd = tz.localize(self.sentinel1_date)
        return str(self.sentinel1_pltf +"_"+str(self.tile)+"_"+str(self.sentinel1_date)) #.isoformat(timespec='seconds')

    
class ParcelEvent(models.Model):
    parcelle = models.ForeignKey(ParcelleGraphique, on_delete=models.CASCADE)
    sentinel1 = models.ForeignKey(Sentinel1Image, on_delete=models.CASCADE)
    #FreezeDetect
    maxrefsigma40 = models.FloatField(null=True,blank=True)
    meansigma40 = models.FloatField(null=True,blank=True)
    meantemp = models.FloatField(null=True,blank=True)
    frozen_type = models.IntegerField(null=True,blank=True)
    parcelle_type = models.IntegerField()
    #SoilMoisture
    meanvvsigma0 = models.FloatField(null=True,blank=True)
    meanndvi = models.FloatField(null=True,blank=True)
    meantheta = models.FloatField(null=True,blank=True)
    soilmoist = models.FloatField(null=True,blank=True)
    
    
    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["parcelle", 'sentinel1'], 
                             name='parcelpardate_unique'
                             ),
            ]
        
    def __str__(self):
        return str(self.parcelle) +","+str(self.sentinel1)+","+str(self.meansigma40)+","+str(self.meantemp)
     

# data managt

# process managt
    

