import os, hashlib
from subprocess import Popen, PIPE
from datetime import datetime, timedelta
import logging 

logger = logging.getLogger(__name__)

def process_command(cmd):
    logger.debug("Starting : "+" ".join(cmd))
    p = Popen(cmd, stdout=PIPE, env=os.environ.copy())
#    p.wait()
    try:
        output = p.communicate()[0]
    except Exception: 
        raise
#         logger.error("process failed %d : %s" % (p.returncode, output))
#         raise Exception("process failed %d : %s" % (p.returncode, output))
    
    return p.returncode

def unzipfile(zipfile, outdir):
    
    cmd=["unzip","-o",zipfile,"-d", outdir]
    p = Popen(cmd, stdout=PIPE, env=os.environ.copy())
#    p.wait()
    try:
        output = p.communicate()[0]
    except Exception: 
        raise
    return p.returncode

        
def search_files(directory='.', resolution='NDVI', extension='tif', band="", fictype='f'):
    images=[]
    extension = extension.lower()
    resolution = resolution.lower()
    band = band.lower()
    for dirpath, dirnames, files in os.walk(directory):
        if fictype == 'f':
            for name in files:
    #            print(os.path.join(dirpath, name) + " test")
                if extension and name.lower().endswith(extension) and name.lower().find(resolution) >= 0 and name.upper().find(band) >= 0:
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, name))
                    images.append(abspath)
        elif fictype == 'd':
            for dirname in dirnames:
    #            print(os.path.join(dirpath, name) + " test")
                if dirname.lower().endswith(extension) and dirname.lower().find(resolution) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, dirname))
                    images.append(abspath)
        else:
            logger.error("search_files type error")
            raise Exception("search_files type error")
            
    return images

def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False

def get_S2L3Adates_from_S1dates(startdate:str,enddate:str):
        std = datetime.strptime(startdate,"%Y-%m-%d")
        edd = datetime.strptime(enddate,"%Y-%m-%d")
        
        td = timedelta(days=25)
        
        newstd = std - td
        newedd = edd + td
        
        return [newstd.strftime("%Y-%m-%d"),newedd.strftime("%Y-%m-%d")]

def get_S2Tiles_for_France():
    
    return ["T30UUU", "T30UVV", "T30UVU", "T30TVT", "T30UWA", "T30UWV", "T30UWU", "T30TWT", "T30TWS", 
    "T30TWP", "T30TWN", "T30UXA", "T30UXV", "T30UXU", "T30TXT", "T30TXS", "T30TXR", "T30TXQ", "T30TXP",
     "T30TXN", "T30UYA", "T30UYV", "T30UYU", "T30TYT", "T30TYS", "T30TYR", "T30TYQ", "T30TYP", "T30TYN",
      "T31UCS", "T31UCR", "T31UCQ", "T31UCP", "T31TCN", "T31TCM", "T31TCL", "T31TCK", "T31TCJ", "T31TCH",
       "T31UDS", "T31UDR", "T31UDQ", "T31UDP", "T31TDN", "T31TDM", "T31TDL", "T31TDK", "T31TDJ", "T31TDH",
        "T31TDG", "T31UES", "T31UER", "T31UEQ", "T31UEP", "T31TEN", "T31TEM", "T31TEL", "T31TEK", "T31TEJ",
         "T31TEH", "T31UFR", "T31UFQ", "T31UFP", "T31TFN", "T31TFM", "T31TFL", "T31TFK", "T31TFJ", "T31TFH",
          "T31UGR", "T31UGQ", "T31UGP", "T31TGN", "T31TGM", "T31TGL", "T31TGK", "T31TGJ", "T31TGH", "T32ULV",
           "T32ULU", "T32TLT", "T32TLS", "T32TLR", "T32TLQ", "T32TLP", "T32UMV", "T32UMU", "T32TMN", "T32TMM",
            "T32TNN", "T32TNM", "T32TNL"]

def is_tile_in_france(tile:str):
    if tile in get_S2Tiles_for_France():
        return True
    else:
        return False

def swapCoords(x):
    out = []
    for iter in x:
        if isinstance(iter, list):
            out.append(swapCoords(iter))
        else:
            return [x[1], x[0]]
    return out


def read_conf(file)->dict:

    conf_dict={}
    with open(file) as f:
        i=0
        for line in f:
            try:
                line.index("=")
            except ValueError:
                continue
            rc = line.index("\n")
            if rc >= 0 :
                line = line[:rc]
            sp_line = list(line.split("="))
            conf_dict[sp_line[0]]='{}'.format(sp_line[1])
    
    return conf_dict

def compute_md5(filename: str) -> str:
    """
    Compute md5sum of the contents of the given filename.

    Args:
        filename: file name

    Returns:
        MD5 checksum

    """
    hash_md5 = hashlib.md5()
    with open(filename, "rb") as file:
        for chunk in iter(lambda: file.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()
