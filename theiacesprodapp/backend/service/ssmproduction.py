import os, datetime,glob, re
import otbApplication
from typing import List
from theiacesprodapp.backend.data import Raster, S1OrthoRaster
import logging 

logger = logging.getLogger(__name__)

class SSMProduction:
    
    
    
    
    def __init__(self,params:dict):
        self.parametres = {
            "agriref":"",
            'landusemap':"",
            "tfmodel_dir": "",
            'tfmodel_choice':"dry", #["dry","wet"],
            's1_ortho_dir':"",
            'gapfndvidir':"",
            'labels_file':"",
            'mrgstile':"",
            "ssmfootprint_shpname":"",
            'slope_file':"",
            'slope_value':20,
            'agrivalues':"",
            'sentinel1_polar_mode':"vv", # choices=['vv', 'vh','vvvh'],
            'sentinel1_incidence_type':"loc", # choices=['loc','elli'],
            'outformat':"raster", #  choices=['raster', 'csv'],
            'data_output_dir':"",
            "tif_output_dir":"",
            "csv_output_dir":"",
            'overwrite':False
            }
        
        for key in self.parametres:
            if not key in params.keys() :
                continue
            self.parametres[key] = params[key]

        if self.parametres["outformat"] == "raster" and self.parametres["tif_output_dir"] == "":
            self.parametres["tif_output_dir"] = os.path.join(self.parametres["data_output_dir"],"TIF")
        if self.parametres["outformat"] == "csv" and self.parametres["csv_output_dir"] == "":    
            self.parametres["csv_output_dir"] = os.path.join(self.parametres["data_output_dir"],"CSV")
        

        if "debug" in params.keys() :
            if params["debug"]:
                logger.setLevel(logging.DEBUG)
        
            
    def start(self) -> List[str]:
        
        #test dirs
        try:
            if not os.path.exists(self.parametres["tif_output_dir"]) and self.parametres["outformat"] == "raster":
                os.makedirs(self.parametres["tif_output_dir"])
            if not os.path.exists(self.parametres["csv_output_dir"]) and self.parametres["outformat"] == "csv":
                os.makedirs(self.parametres["csv_output_dir"])
        except Exception as err :
            logger.error("Folder creation failed.")
            raise err
        
        #sar ortho type (snap, theiacesprod) TODO un autre jour
        #init labels and slope
        
        self.labels = Raster(self.parametres["labels_file"])
        logger.debug("Labels: "+self.labels.filepath)
        if self.parametres["slope_file"] != "" and os.path.exists(self.parametres["slope_file"]):
            self.slope = Raster(self.parametres["slope_file"])
            logger.debug("Slope: "+self.slope.filepath)
        else:
            self.slope = None
            
        #dbl polar
        polar2proc=[]
        #double process
        polarsuffix = "_VV"
        if self.parametres["sentinel1_polar_mode"] == "vvvh":
            polar2proc.append('vv')
            polar2proc.append('vh')
            polarsuffix = ""
        else:
            polar2proc.append(self.parametres["sentinel1_polar_mode"])
            polarsuffix = "_"+self.parametres["sentinel1_polar_mode"].upper()
        
        outext=""
        if self.parametres["outformat"] == "csv" :
            data_output_dir = self.parametres["csv_output_dir"]
            outext="csv"
        else:
            data_output_dir = self.parametres["tif_output_dir"]
            outext="TIF"
        
        outshpdir = os.path.join(self.parametres["data_output_dir"],"ssmfootprint")
        
        # get ndvis
        ndvis=[]
        ndvis=glob.glob(os.path.join(self.parametres["gapfndvidir"], 'NDVI*.TIF'))
        
        #list snetinel1
        sars=[]
        sars=glob.glob(os.path.join(self.parametres["s1_ortho_dir"], 'S1*'+polarsuffix+'.TIF'))
        outputlist=[]
        #loop over sar
        for sard in sars:
            #associates incidence
            self.s1ORTHO = S1OrthoRaster(sard)
            self.s1ORTHO.populate_auxfilepath(self.parametres["sentinel1_incidence_type"])
            #build output name
            output = os.path.join(data_output_dir, "MV_"+self.s1ORTHO.plateform.upper()
                                  +"_"+self.parametres["mrgstile"].upper()
                                  +"_"+self.s1ORTHO.s1date.upper()
                                  +"_"+self.s1ORTHO.polarmode.upper() 
                                  +"."+outext)
            
            if not os.path.exists(output) or self.parametres["overwrite"] :
                logger.info("SSM production for: "+self.s1ORTHO.filepath)
                self.s1INCI = Raster(self.s1ORTHO.auxfilespath["THETA"+self.parametres['sentinel1_incidence_type'].upper()])
                logger.debug("S1INCI: "+self.s1INCI.filepath)
                #associates ndvi
                ndvi = SSMProduction.find_closest_ndvi(sard, ndvis)
                self.NDVI = Raster(ndvi)
                logger.debug("NDVI: "+self.NDVI.filepath)
                #reproj WGS84
                self.s1ORTHO.reprojectRaster(4326,self.parametres["sentinel1_incidence_type"])
                
                #create inter geojson file
                #param["ssmfootprint_geojson"]
                
                
                self.parametres["ssmfootprint_shpname"] = os.path.join(outshpdir, os.path.basename(output).split(".")[0]+".shp")
                intersectionEnv = self.s1ORTHO.get_intersection_with_rasterlist([
                                    self.NDVI,
                                    self.labels
                                    ])
                if not os.path.exists(self.parametres["ssmfootprint_shpname"]) or self.parametres["overwrite"] :
                    logger.debug("SHP : "+self.parametres["ssmfootprint_shpname"])
                    self.s1ORTHO.create_footprint(self.parametres["ssmfootprint_shpname"], intersectionEnv)
                                
                #compute ssm
                logger.info("Starting SSM inversion :"+os.path.basename(output))
                try:
                    SSMProduction.compute_ssm(output, self.s1ORTHO, self.s1INCI, self.NDVI, self.labels, slope=self.slope,param=self.parametres)
                except RuntimeError:
                    logger.warn(f"""OTB failed to process {os.path.basename(self.s1ORTHO.filepath)}
                                    Passing {os.path.basename(output)}""")
                    continue

                if self.parametres["outformat"] == "raster" :
                    ssmraster = Raster(output)
                    if not ssmraster.iswgs84:
                        ssmraster.reprojectRaster(4326)
                
                outputlist.append(output)
            
            else:
                logger.debug(os.path.basename(output)+ " already processed. Passing "+str(self.s1ORTHO))
                outputlist.append(output)

            
        return outputlist
    
    @classmethod
    def find_closest_ndvi(cls, sar, ndvis):
        sarbase = os.path.basename(sar)
        fdate = re.findall("20\d{6}T\d{6}",sarbase)
        if not(fdate):
            logger.error("Impossible de trouver la date dans le nom de fichier : "+sarbase)
            raise Exception("Unable to find date in file : "+sarbase)
        sardate=datetime.datetime.strptime(fdate[0], "%Y%m%dT%H%M%S")
        ndvisdict={}
        for ndvi in ndvis:
            ndvibase = os.path.basename(ndvi)
            ndviexd = ndvibase[:-4].split("_")[2]
            ndate = ndviexd[:8]
    #         ndate = re.findall("20\d{6}",ndvibase)
            if not str(ndate).isdigit():
                logger.error("Impossible de trouver la date dans le nom de fichier : "+ndvibase)
                raise Exception("Unable to find date in file : "+ndvibase)
            
            ndvidate=datetime.datetime.strptime(ndate, "%Y%m%d")
            ndvisdict[ndvi]=abs((sardate - ndvidate).days)
        
        sortedndvis = sorted(ndvisdict, key=ndvisdict.get)
        
        return sortedndvis[0]
    
    @classmethod
    def get_model4invertion(cls, s1_ORTHO:Raster, modelchoicefile:str):
        
        result=None
        with open(modelchoicefile) as mc:
            for line in mc:
                sline = line.split(";")
                if os.path.basename(s1_ORTHO.filepath) in sline[0]:
                    if "wet" in sline[1]:
                        logger.info("Using model wet for S1 date:"+ os.path.basename(s1_ORTHO.filepath))
                        return "wet"
                    elif "dry" in sline[1]:
                        logger.info("Using model dry for S1 date:"+ os.path.basename(s1_ORTHO.filepath))
                        return "dry"
        
        if result == None:
            logger.warning("Using default dry model. Can't find modelchoice in file for S1 :"+ os.path.basename(s1_ORTHO.filepath))
            return "dry"
            
    @classmethod
    def compute_ssm(cls,output:str, s1_ORTHO:S1OrthoRaster, s1_INCI:Raster, ndvi:Raster,
                     labels:Raster,slope:Raster=None,param:dict=None):
        
        
        if param == None :
            logger.error("Paramter dict needed")
            raise Exception("parameter dict missing")
        
        logger.debug("SSM from : "+str(s1_ORTHO))
        #otb pipeline
        # The following line creates an instance of the Superimpose application
        app1 = otbApplication.Registry.CreateApplication("ExtractROI")
        #(-3.0002720958937834, 47.7564416347774, -1.6062662155008034, 48.75301300400097)
        # The following lines set all the application parameters:
        app1.SetParameterString("in", s1_ORTHO.filepath)
        app1.SetParameterString("mode", "fit")
        app1.SetParameterString("mode.fit.vect",param["ssmfootprint_shpname"])
#         app1.SetParameterFloat("mode.extent.ulx",intersectionEnv[0])
#         app1.SetParameterFloat("mode.extent.uly",intersectionEnv[3])
#         app1.SetParameterFloat("mode.extent.lrx",intersectionEnv[2])
#         app1.SetParameterFloat("mode.extent.lry",intersectionEnv[1])
#         app1.SetParameterString("mode.extent.unit","lonlat")
        app1.SetParameterString("out", "temp1.tif")
        
        # The following line execute the application
        app1.Execute() #ExecuteAndWriteOutput() 
        logger.debug("ExtractROI : "+os.path.basename(param["ssmfootprint_shpname"]))
        
        app2 = otbApplication.Registry.CreateApplication("Superimpose")
        
        # The following lines set all the application parameters:
        app2.SetParameterString("inm", s1_INCI.filepath)
        app2.SetParameterInputImage("inr", app1.GetParameterOutputImage("out")) #SetParameterString("inr", s1_ORTHO.filepath)
        app2.SetParameterString("interpolator","nn")
        app2.SetParameterString("out", "temp2.tif")
        
        # The following line execute the application
        app2.Execute() #ExecuteAndWriteOutput() 
        logger.debug("Superimpose : "+str(s1_INCI))
        
        app3 = otbApplication.Registry.CreateApplication("Superimpose")
        
        # The following lines set all the application parameters:
        app3.SetParameterString("inm", ndvi.filepath)
        app3.SetParameterInputImage("inr", app1.GetParameterOutputImage("out"))  #SetParameterString("inr", s1_ORTHO.filepath)
        app3.SetParameterString("interpolator","nn")
        app3.SetParameterString("out", "temp3.tif")
        
        # The following line execute the application
        app3.Execute() #ExecuteAndWriteOutput()
        logger.debug("Superimpose : "+str(ndvi))
        
        app4 = otbApplication.Registry.CreateApplication("Superimpose")
        
        # The following lines set all the application parameters:
        app4.SetParameterString("inm", labels.filepath)
        app4.SetParameterInputImage("inr", app1.GetParameterOutputImage("out"))  #SetParameterString("inr", s1_ORTHO.filepath)
        app4.SetParameterString("interpolator","nn")
        app4.SetParameterString("out", "temp4.tif")
        
        # The following line execute the application
        app4.Execute() #ExecuteAndWriteOutput() 
        logger.debug("Superimpose : "+str(labels))
           
        if not slope == None :
            slopevalue = str(param["slope_value"])
            
            app41 = otbApplication.Registry.CreateApplication("Superimpose")
        
            # The following lines set all the application parameters:
            app41.SetParameterString("inm", slope.filepath)
            app41.SetParameterInputImage("inr", app1.GetParameterOutputImage("out")) #SetParameterString("inr", s1_ORTHO.filepath) 
            app41.SetParameterString("interpolator","nn")
            app41.SetParameterString("out", "temp41.tif")
            
            # The following line execute the application
            app41.Execute() #ExecuteAndWriteOutput() 
            logger.debug("Superimpose : "+str(slope))
            
            appS = otbApplication.Registry.CreateApplication("BandMath")
            appS.AddImageToParameterInputImageList("il", app1.GetParameterOutputImage("out"))  #SetParameterStringList("il", [s1_ORTHO.filepath])
            # Define Input im2: Band Red (B4)
            appS.AddImageToParameterInputImageList("il", app41.GetParameterOutputImage("out"))
            appS.SetParameterString("out", "tempS.tif")
            appS.SetParameterString("exp", "im2b1<"+slopevalue+"?im1b1:0")

            logger.debug("BandMath : "+  "im2b1<"+slopevalue+"?im1b1:0")
            
            appS.Execute()
            
        
        if  not (param["tfmodel_choice"] == "dry" or param["tfmodel_choice"] == "wet" ):
            if not os.path.exists(param["tfmodel_choice"]):
                logger.error("Unable to find file :"+param["tfmodel_choice"])
                raise Exception("Unable to find file :"+param["tfmodel_choice"])
            
            choix = SSMProduction.get_model4invertion(s1_ORTHO, param["tfmodel_choice"])
            modelchoix = os.path.join(param["tfmodel_dir"],"SavedModel_"+choix)
        else:
            modelchoix = os.path.join(param["tfmodel_dir"],"SavedModel_"+param["tfmodel_choice"])
            
        # The following line creates an instance of the Superimpose application
        app5 = otbApplication.Registry.CreateApplication("InvertSARModel")
        app5.SetParameterInputImage("insarth", app2.GetParameterOutputImage("out") )
        app5.SetParameterInputImage("inndvi", app3.GetParameterOutputImage("out")) #ndvisup)
        app5.SetParameterInputImage("inlabels", app4.GetParameterOutputImage("out")) # labelssup)
        app5.SetParameterString("model.dir", modelchoix)
        
        if not slope == None:
            app5.SetParameterInputImage("insarvv", appS.GetParameterOutputImage("out"))
        else:
            app5.SetParameterInputImage("insarvv", app1.GetParameterOutputImage("out")) #SetParameterString("insarvv",  s1_ORTHO.filepath) 
            
        
        app5.SetParameterString("sarmode", s1_ORTHO.polarmode.lower())
            
        if param["outformat"] == "csv" :
            app5.SetParameterString("format", "csv")
            app5.SetParameterString("format.csv.out", output)
            
            app5.ExecuteAndWriteOutput()
            logger.debug("Inverting model to csv with modeldir : "+ modelchoix)
            
        else:
            app5.SetParameterString("format", param["outformat"])
            app5.SetParameterString("format.raster.out", "temp.tif")
            
            # The following line execute the application
            app5.Execute()
            logger.debug("Inverting model to raster with modeldir : "+ modelchoix)
            
            app6 = otbApplication.Registry.CreateApplication("BandMath")
            app6.AddImageToParameterInputImageList("il",app5.GetParameterOutputImage("format.raster.out"))
            app6.SetParameterString("exp", "im1b1>0?im1b1*5:0")
            app6.SetParameterInt("ram",4000)
            
            
                # The following line execute the application
            if param["agriref"] != "raster":
                app6.SetParameterString("out", output+"?&gdal:co:COMPRESS=DEFLATE&nodata=0")
                app6.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
                app6.ExecuteAndWriteOutput() #ExecuteAndWriteOutput() 
                logger.debug("BandMath : "+ "im1b1*5")
            else:
                app6.Execute() 
                logger.debug("BandMath : "+ "im1b1*5")

                app7 = otbApplication.Registry.CreateApplication("ExtractROI")
    
                # The following lines set all the application parameters:
                app7.SetParameterString("in", param["landusemap"])
                app7.SetParameterString("out", "tmp1.tif")
                app7.SetParameterString("mode", "fit")
                app7.SetParameterInputImage("mode.fit.im", app6.GetParameterOutputImage("out"))

                app7.Execute()
                logger.debug("ExtractROI from output ref : "+ param["landusemap"])
                
                app8 = otbApplication.Registry.CreateApplication("Superimpose")
                # The following lines set all the application parameters:
                app8.SetParameterInputImage("inm", app6.GetParameterOutputImage("out"))
                app8.SetParameterInputImage("inr", app7.GetParameterOutputImage("out")) 
                app8.SetParameterString("interpolator","linear")
                app8.SetParameterString("out", "tmp1.tif")
                # app8.SetParameterString("out", output+"?&gdal:co:COMPRESS=DEFLATE&nodata=0")
                # app8.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
                
                # The following line execute the application
                app8.Execute() #ExecuteAndWriteOutput() 
                logger.debug("Resample on ROI extracted from : "+param["landusemap"])
                
                im1b1 = "im1b1=="
                addor = " or "
                agricond = ""
                i=0
                for agriv in param["agrivalues"].split(" "):
                    i+=1
                    if i == len(param["agrivalues"].split(" ")):
                        agricond += im1b1 + agriv
                    else:
                        agricond += im1b1 + agriv + addor

                logger.debug(f"BandMath exp : im2b1 > 0 and ({agricond})?im2b1:0")
                app9 = otbApplication.Registry.CreateApplication("BandMath")
    
                # The following lines set all the application parameters:
                app9.AddImageToParameterInputImageList("il", app7.GetParameterOutputImage("out"))
                app9.AddImageToParameterInputImageList("il", app8.GetParameterOutputImage("out")) 
                app9.SetParameterString("exp", f"im2b1 > 0 and ({agricond})?im2b1:0")
                app9.SetParameterString("out", output+"?&gdal:co:COMPRESS=DEFLATE&nodata=0")
                app9.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)

                app9.ExecuteAndWriteOutput()
                logger.debug("Refilter on agrivalues : "+ param["agrivalues"])
            
        
            
            
            # app7.ExecuteAndWriteOutput()
            # logger.debug("ManageNoData : ")
            
class SSMConverter:
    
    
    def __init__(self,labels_file:str,csv_output_dir:str,tif_output_dir:str):
        
        self.csv_output_dir = csv_output_dir
        self.labels_file=labels_file
        self.tif_output_dir=tif_output_dir

        
            
    def start(self):
        ssmconv = []
        labelsimage = self.labels_file
        if not os.path.isdir(self.csv_output_dir):
            logger.error("erreur "+self.csv_output_dir+" n'est pas un dossier")
            raise Exception("erreur "+self.csv_output_dir+" n'est pas un dossier")
        
        if not os.path.exists(self.tif_output_dir):
            os.mkdir(self.tif_output_dir)
        
        csvs=glob.glob(os.path.join(self.csv_output_dir, 'MV_*.csv'))
        if len(csvs)==0 :
            logger.info("No CSV file found.")
            raise Exception("No CSV file found.")
            
        for csv in csvs:
            outmoist = os.path.join(self.tif_output_dir,os.path.basename(csv)[:-3]+"TIF")
            logger.info("\nProcessing file: "+csv)
            SSMConverter.csvtoraster_pipeline(labelsimage, csv, outmoist)
            ssmconv.append(Raster(outmoist))
        
        return ssmconv
    
    @classmethod        
    def csvtoraster_pipeline(cls, labelsimage, csvfile, outmoist):
    
        app1 = otbApplication.Registry.CreateApplication("ApplyCsvLabelsFile")
        
        # The following lines set all the application parameters:
        app1.SetParameterString("incsv", csvfile)
        app1.SetParameterString("inlabels", labelsimage)
        app1.SetParameterString("out", "temp1.tif")
        
        logger.info("Launching... Resampling")
        # The following line execute the application
        app1.Execute() #ExecuteAndWriteOutput() 
        logger.info("End of Resampling \n")
        
        app6 = otbApplication.Registry.CreateApplication("BandMath")
        app6.AddImageToParameterInputImageList("il",app1.GetParameterOutputImage("out"))
        app6.SetParameterString("out", "temp2.tif")
        app6.SetParameterString("exp", "im1b1>0?im1b1*5:0")
        
        logger.info("Launching... BandMath(im1b1*5)")
        # The following line execute the application
        app6.Execute() #ExecuteAndWriteOutput() 
        logger.info("End of BandMath \n")
    
    #    Ajouter ManageNoData    
        app7 = otbApplication.Registry.CreateApplication("ManageNoData")
        app7.SetParameterInputImage("in", app6.GetParameterOutputImage("out"))
        app7.SetParameterString("out", outmoist+"?&gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")#
        app7.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
        app7.SetParameterString("mode", "changevalue")
        
        
        logger.info("Launching... ManageNoData")
        app7.ExecuteAndWriteOutput()
        logger.info("Done \n")
        

            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
