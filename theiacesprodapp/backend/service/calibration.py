
import os, shutil, glob, datetime, math
from typing import List
import xml.etree.ElementTree as ET
import otbApplication
from theiacesprodapp.backend.data import Sentinel1Zip, Raster, S1OrthoRaster
from theiacesprodapp.backend.utils import process_command
from theiacesprodapp.backend.service.download import Esa_Srtm
import logging 

logger = logging.getLogger(__name__)

# otbApplication.PythonLogOutput..Logger_Instance().propagate=True
# ch = logging.StreamHandler(otbApplication.Logger_Instance())
# ch.setLevel(logging.DEBUG)
# formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s -%(message)s")
# ch.setFormatter(formatter)
# logger.addHandler(ch)
    
# otbApplication.Logger_Instance() 
class OrthorectifySentinel1:
    
    
    def __init__(self,params:dict, s1l1ziplist=None):
        self.s1l1ziplist = None
        self.parametres={
            "sentinel1_L1A_zip_dir" : None,
            "reference_image_file" : None,
            "mrgstile" : None,
            "s1_ortho_dir" : None,
            "sentinel1_polar_mode" : 'vv', #choices=['vv', 'vh', 'vvvh']
            "sentinel1_incidence_type" : 'loc', # ou 'elli'
            "urlsrmt":"http://step.esa.int/auxdata/dem/SRTMGL1/",
            "srtmhgtzip":"/data/SRTM/",
            "geoid":"/work/python/data/egm96.grd",
            "overwrite":False
            }
        
        self.refraster = None
        self.srtmhgtlist = None
        self.demraster = None
        self.sloperaster = None
        self.aspectraster = None
        self.s1zip = None
        self.s1otbinputs = None
        self.out_ORTHO = None
        self.orthoprod = None
        
        for key in self.parametres:
            self.parametres[key] = params[key]

        if s1l1ziplist != None :
            self.s1l1ziplist = s1l1ziplist
    
    def get_sloperaster(self):
        return self.sloperaster
    
    
    def start(self)->List[S1OrthoRaster]:
        self.orthoprod = []
        #monitor
        procstart = datetime.datetime.now()
        
        overwrite = self.parametres["overwrite"]
        
        self.refraster = Raster(self.parametres["reference_image_file"])
        logger.info("Using ref : "+os.path.basename(self.refraster.filepath))
        
        if self.refraster.iswgs84 :
            self.refraster.reprojectRaster(3857)
            
        #Prepare outdirs
        try:
            if not os.path.exists(self.parametres["s1_ortho_dir"]):
                os.mkdir(self.parametres["s1_ortho_dir"])
            demdir = os.path.join(self.parametres["s1_ortho_dir"],"DEM")
            tmpdemdir = os.path.join(demdir,"tmp")
            if not os.path.exists(demdir):
                os.mkdir(demdir)
            if not os.path.exists(tmpdemdir):
                os.mkdir(tmpdemdir)
            auxdir = os.path.join(self.parametres["s1_ortho_dir"],"auxfiles")
            if not os.path.exists(auxdir):
                os.mkdir(auxdir)
            auxtmpdir = os.path.join(auxdir,"tmp")
            if not os.path.exists(auxtmpdir):
                os.mkdir(auxtmpdir)
        except Exception as err :
            logger.error("Folder creation failed.")
            raise err
        #dem processing
        esa_proc = Esa_Srtm(self.refraster, self.parametres["srtmhgtzip"])
        self.srtmhgtlist = esa_proc.process()
        
        outdem = os.path.join(demdir,"SRTM_"+self.parametres["mrgstile"]+".TIF")
        outslope = os.path.join(demdir,"SLOPE_"+self.parametres["mrgstile"]+".TIF")
        outaspect = os.path.join(demdir,"ASPECT_"+self.parametres["mrgstile"]+".TIF")
        if not os.path.exists(outdem) or self.parametres["overwrite"] :
            self.create_ref_dem(outdem)
        if not os.path.exists(outslope) or self.parametres["overwrite"] :
            self.create_ref_slope(outdem,outslope)
        if not os.path.exists(outaspect) or self.parametres["overwrite"] :
            self.create_ref_aspect(outdem, outaspect)
        
        self.demraster = Raster(outdem)
        self.sloperaster = Raster(outslope)
        self.aspectraster = Raster(outaspect)
        logger.info("Ref DEM : "+os.path.basename(self.demraster.filepath))
        #list snetinel1
        if self.s1l1ziplist != None :
            indir = self.s1l1ziplist 
        else:
            indir=glob.glob(os.path.join(self.parametres["sentinel1_L1A_zip_dir"], '**','S1*.zip'), recursive=True)
        
        
        polar2proc=[]
        #double process
        if self.parametres["sentinel1_polar_mode"] == "vvvh":
            polar2proc.append('vv')
            polar2proc.append('vh')
        else:
            polar2proc.append(self.parametres["sentinel1_polar_mode"])
        
        #boucle sur listeS1 
        dejafait=[]
        for filez in indir:
            skips1zip = False
            #saut des doublons de mosaic
            if filez in dejafait:
                logger.info("Already processed. Passing "+os.path.basename(filez))
                continue
            
            logger.info("Processing S1 : "+os.path.basename(filez))
            
            # dblproc
            for polarmode in polar2proc :
                if skips1zip :
                    continue

                self.s1otbinputs=[]
                #parse S1 file name
                self.s1zip = Sentinel1Zip(filez, self.refraster, polarmode)
                
                #define outfile name + dblproc
                outfilebase = os.path.join(self.parametres["s1_ortho_dir"],
                                           self.s1zip.prefix+"_"+self.parametres["mrgstile"]
                                           +"_"+self.s1zip.acqdate_start)
                outfile = outfilebase +"_"+polarmode.upper() +".TIF"
                
                self.s1otbinputs.append(self.s1zip)
                #find if mosaic
                logger.debug("Finding mosaic for : "+os.path.basename(self.s1zip.filepath))
                sametrace_ind = [indir.index(i) for i in indir 
                        if (self.s1zip.acqdate_start.split('T')[0] in i) 
                        and (self.s1zip.absorbit in i) and (self.s1zip.plateforme in i)]
                
                if len(sametrace_ind) > 1 :
                    for stid in sametrace_ind :
                        if not self.s1zip.filepath in indir[stid] :
                            s1trck = Sentinel1Zip(indir[stid],self.refraster,self.parametres["sentinel1_polar_mode"])
                            self.s1otbinputs.append(s1trck)
                            logger.debug("Same S1 Track : "+os.path.basename(s1trck.filepath))
                    
#                 logger.debug("nbS1 = "+str(len(self.s1otbinputs)))
#                 if len(sametrace_ind) >= 2:
#                     s1zip2pass = self.checktrace()
#                     if len(s1zip2pass) > 0:
#                         for p in s1zip2pass:
#                             logger.info("Passing " + p.filepath)
#                             dejafait.append(p.filepath)
                logger.debug("Overlap filter nbS1 before = "+str(len(self.s1otbinputs)))
                #filter from intersection
                delsar=[]
                for s1otb in self.s1otbinputs :
                    logger.debug("Refpoly:"+str(self.refraster.enveloppe_wgs84.extent))
                    logger.debug(f"S1proc({self.s1otbinputs.index(s1otb)}):"+str(self.refraster.enveloppe_wgs84.extent))
                    overlap = s1otb.intersect_ref_poly.area / self.refraster.enveloppe_wgs84.area
                    logger.info("Overlapping ref = "+str(round(overlap*100))+"%")
                    if None == s1otb.intersect_ref_poly or s1otb.intersect_ref_poly.point_count == 0 or overlap < 0.02:
                        logger.info("Not using "+s1otb.filepath)
                        delsar.append(s1otb)
                        dejafait.append(s1otb.filepath)
                for dd in delsar:
                    self.s1otbinputs.pop(self.s1otbinputs.index(dd))
                    
                logger.debug("Overlap filter nbS1 after = "+str(len(self.s1otbinputs)))
                
                if len(self.s1otbinputs) == 0:
                    logger.error("ERROR: Sar files don't intersect reference.")
                    logger.error("Passing "+self.s1zip.filepath )
                    dejafait.append(self.s1zip.filepath)
                    skips1zip = True
                    continue
                
                if not os.path.exists(outfile) or self.parametres["overwrite"] :
                    #create tmp dem hgt dir
                    for s1 in  self.s1otbinputs :
                        # self.srtmhgtzip.update_srtm_with_geom(s1.extent)
                        # self.srtmhgtzip.prepare_tmpdemdir(tmpdemdir)
                        #unzip s1
                        s1.extract_in_tmp(auxtmpdir)
                        s1.find_annotiff_in_unzip(polarmode)
                    
                    #otb_s1_pipeline + dblproc
                    self.otb_s1_pipeline(outfile,self.parametres["srtmhgtzip"])
                else:
                    logger.info("Already processed. Passing "+os.path.basename(filez))
                    
                for s1otb in self.s1otbinputs :
                    dejafait.append(s1otb.filepath)
            
            if skips1zip :
                continue
            #init S1otho output
            self.out_ORTHO = S1OrthoRaster(outfile)
            #get annotation metadata
            #auxfiles creation
            self.out_ORTHO.create_aux_filepath(auxdir, auxtmpdir, self.parametres["sentinel1_incidence_type"])
            
            incitype = "THETA"+self.parametres["sentinel1_incidence_type"].upper()
            
            annofiles=[]
            if not os.path.exists(self.out_ORTHO.auxfilespath[incitype]) or  self.parametres["overwrite"] :
                #TODO mettre dans data et extraire seulement les annofiles
                for s1 in self.s1otbinputs:
                    if not s1.isunzip(auxtmpdir) :
                        s1.extract_in_tmp(auxtmpdir)
                    s1.find_annotiff_in_unzip(polarmode)
                    annofiles.append(s1.anno_file)
                
                self.create_incidenceAngle_from_surface_raster(annofiles)
                    
                if self.parametres["sentinel1_incidence_type"] == "elli":
                        self.create_incidenceAngle_from_ellipsoid_raster()
                if self.parametres["sentinel1_incidence_type"] == "loc":
                        self.create_local_incidenceAngle_raster(annofiles)
            
            self.orthoprod.append(self.out_ORTHO)
            #nettoyage tmpfiles
            logger.info("Removing temp files...")
            if os.path.exists(auxtmpdir):
                shutil.rmtree(auxtmpdir, ignore_errors=True)
            if not os.path.exists(auxtmpdir):
                os.mkdir(auxtmpdir)
        #nettoyage tmpfiles
        logger.info("Removing temp dem files...")
        if os.path.exists(tmpdemdir):
            shutil.rmtree(tmpdemdir, ignore_errors=True)     
            
        procend = datetime.datetime.now()
        proctime = procend -procstart
        logger.info("Total elapse time : "+str(proctime))    
        
        return self.orthoprod

    def start_sigma40(self, sigma40_dir:str)->List[S1OrthoRaster]:
        sigma40list = []
        for s1ortho in self.orthoprod:
            fname = os.path.basename(s1ortho.filepath)[:-3]+"_SIGMA40.TIF"
            outfile = os.path.join(os.path.dirname(s1ortho.filepath),fname)
            self.sigma0_at_40d(s1ortho.filepath,s1ortho.auxfilespath["THETALOC"],outfile)
            
            sigma40list.append(S1OrthoRaster(outfile))

        return sigma40list

                    
    def create_ref_dem(self,outdem:str):
        logger.info("Generating DEM file on ref:"+self.refraster.filepath)
        
        mosapp = otbApplication.Registry.CreateApplication("Mosaic")
        mosparams = {"il":self.srtmhgtlist, "out":"mosatemp.tif"}
        mosapp.SetParameters(mosparams)
        mosapp.Execute()
        
        supapp = otbApplication.Registry.CreateApplication("Superimpose")
        supapp.SetParameterString("inr", self.refraster.filepath)
        supapp.SetParameterInputImage("inm", mosapp.GetParameterOutputImage("out"))
        supapp.SetParameterString("out", outdem)
        
        
        supapp.ExecuteAndWriteOutput()
    
    def create_ref_slope(self,outdem:str, outslope:str):
        #Compute slope & aspect
        logger.info("Generating slope file...")
        cmd=['gdaldem', 'slope', outdem, outslope]
        
        process_command(cmd)
    
    def create_ref_aspect(self,outdem:str, outaspect:str):
        logger.info("Generating aspect file...")
        cmd=['gdaldem', 'aspect', outdem, outaspect]
        
        process_command(cmd)
        
    
    def checktrace(self):
        #recuperation des startdate et enddate
        dictcheck=[]
        for s1 in self.s1otbinputs :
            
#             if products[ind] == self.s1zip.filepath :
#                 prod = self.s1zip
#             else:
#                 prod = Sentinel1Zip(products[ind],self.refraster,self.parametres["sentinel1_polar_mode"])
#                  
#             acqdate_start = datetime.datetime.strptime(prod.acqdate_start, "%Y%m%dT%H%M%S")  
#             acqdate_end = datetime.datetime.strptime(prod.acqdate_end, "%Y%m%dT%H%M%S") 
             
            dictcheck.append({"instance":s1,"start":s1.acqdate_start,"end":s1.acqdate_end})
             
        #mise en ordre chronologique 
        orderedd = sorted(dictcheck, key = lambda i: i['start'])
        
        #recherche de la slice de départ 
        i=0
        mosaic=[]
        alone=[]
        while len(orderedd)>0:
             
            tzero = orderedd.pop(0)
            #ajout des slice de depart de la plus jeune a la plus vieille
            follows = {"first":tzero,"second":None}
            #recherche de la slice de fin
            j=0
            for j in range(len(orderedd)):
                if orderedd[j]["start"] == tzero["end"] :
                    follows["second"]=orderedd[j]
                     
            if follows["second"] != None:
                mosaic.append(follows)
            else:
                alone.append(tzero)
             
        prod2proc=[]
        prod2pass=[]
        if len(mosaic) == 1 :
            if mosaic[0]["second"]["instance"] in alone :
                alone.pop(alone.index(mosaic[0]["second"]))
            if len(alone) > 0:
                for a in alone :
                    prod2pass.append(self.s1otbinputs.pop(self.s1otbinputs.index(a["instance"])))
                    
        elif len(mosaic) == 0 :
            keep = alone.pop(0)
            for a in alone:
                prod2pass.append(self.s1otbinputs.pop(self.s1otbinputs.index(a["instance"])))
            
        else:
            logger.warning("This usecase should not happen")
            keep = alone.pop(0)
            for a in alone:
                prod2pass.append(prod2pass = self.s1otbinputs.pop(self.s1otbinputs.index(a["instance"])))
            
            
         
        return prod2pass
    
    
        
    def otb_s1_pipeline(self, outfile, demdir):
    
        
        outdir = os.path.dirname(outfile)
        appdict={"ExtractShp":[],"ExtractROI":[],"SARCalibration":[],"OrthoRectification":[],
                 "Mosaic":None,"BandMath":None,"ManageNoData":None
                 }
        i=0
        logger.debug(f"Calibration OTB pipeline start on {len(self.s1otbinputs)} file")
        for sar in self.s1otbinputs :
            
            interpoly = sar.intersect_ref_poly
    #         shpname = os.path.basename(sar)[:-5]
            tmpdir = os.path.join(os.path.dirname(sar.filepath),"REF_intersec")
            tmpshp = os.path.basename(sar.filepath).split(".")[0]
            appdict["ExtractShp"].append(sar.create_tmp_shp(tmpshp, interpoly, tmpdir)) #"tmppoly"+str(i)
            logger.debug("Intersecction shp :"+tmpshp+".shp")
            
            appdict["SARCalibration"].append(otbApplication.Registry.CreateApplication("SARCalibration"))
            appdict["SARCalibration"][i].IN = sar.tiff_file  #SetParameterString("in",sar.tiff_file)
#             appdict["SARCalibration"][i].SetParameterString("out",str(i)+"SARCalibration.tif?&writegeom=true") 
            
            appdict["SARCalibration"][i].Execute()
            logger.debug("SARCalibration :"+os.path.basename(sar.tiff_file))
            
            appdict["OrthoRectification"].append(otbApplication.Registry.CreateApplication("OrthoRectification"))
            appdict["OrthoRectification"][i].SetParameterInputImage("io.in",appdict["SARCalibration"][i].GetParameterOutputImage("out"))
#             appdict["OrthoRectification"][i].SetParameterString("io.out",str(i)+"OrthoRectification.tif?&writegeom=true")
            appdict["OrthoRectification"][i].SetParameterFloat("opt.gridspacing",40)
            appdict["OrthoRectification"][i].SetParameterString("elev.dem", demdir)
            appdict["OrthoRectification"][i].SetParameterString("elev.geoid", self.parametres["geoid"])
            appdict["OrthoRectification"][i].Execute()  
            logger.debug("OrthoRectification :"+str(sar))
            
            appdict["ExtractROI"].append(otbApplication.Registry.CreateApplication("ExtractROI"))
            appdict["ExtractROI"][i].SetParameterInputImage("in",appdict["OrthoRectification"][i].GetParameterOutputImage("io.out"))
#             appdict["ExtractROI"][i].SetParameterString("out",str(i)+"ExtractROI.tif?&writegeom=true") 
            appdict["ExtractROI"][i].SetParameterString("mode","fit") 
            appdict["ExtractROI"][i].SetParameterString("mode.fit.vect",appdict["ExtractShp"][i]) 
            appdict["ExtractROI"][i].Execute()
            logger.debug("ExtractROI :"+os.path.basename(appdict["ExtractShp"][i]))
            
            i+=1
            
        if len(self.s1otbinputs) > 1:
                
            appdict["Mosaic"] = otbApplication.Registry.CreateApplication("Mosaic")
            for j in range(len(self.s1otbinputs)):
                appdict["Mosaic"].AddImageToParameterInputImageList("il", appdict["ExtractROI"][j].GetParameterOutputImage("out"))
                logger.debug("Mosaic add image "+str(j))
#             appdict["Mosaic"].SetParameterString("out",  "mosatemp.tif?&writegeom=true")
            appdict["Mosaic"].Execute()
            
            
            appdict["BandMath"] = otbApplication.Registry.CreateApplication("BandMath")
            appdict["BandMath"].AddImageToParameterInputImageList("il", appdict["Mosaic"].GetParameterOutputImage("out"))
            appdict["BandMath"].OUT = outfile+"?&gdal:co:COMPRESS=DEFLATE&nodata=0"
            appdict["BandMath"].SetParameterString("exp", "im1b1<=0?0:im1b1")
            appdict["BandMath"].ExecuteAndWriteOutput()
            logger.debug("BandMath :"+"im1b1<=0?0:im1b1")
            
        else : 
            
            appdict["BandMath"] = otbApplication.Registry.CreateApplication("BandMath")
            appdict["BandMath"].AddImageToParameterInputImageList("il", appdict["ExtractROI"][0].GetParameterOutputImage("out"))
            appdict["BandMath"].OUT = outfile+"?&gdal:co:COMPRESS=DEFLATE&nodata=0"
            appdict["BandMath"].SetParameterString("exp", "im1b1<=0?0:im1b1")
            appdict["BandMath"].ExecuteAndWriteOutput()
            logger.debug("BandMath :"+"im1b1<=0?0:im1b1")
        
        # appdict["ManageNoData"] = otbApplication.Registry.CreateApplication("ManageNoData")
        # appdict["ManageNoData"].SetParameterInputImage("in", appdict["BandMath"].GetParameterOutputImage("out"))
        # appdict["ManageNoData"].OUT = outfile+"?&gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES"  #SetParameterString("out", outfile+"?&writegeom=true&gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")
        # appdict["ManageNoData"].SetParameterString("mode", "changevalue")
        # appdict["ManageNoData"].ExecuteAndWriteOutput()
        
        # logger.debug("ManageNoData :")
        
#         shapefiles = glob.glob(appdict["ExtractShp"][0][:-5]+"*")
#         for s in shapefiles:
#             os.remove(s)
        
        
        return 0

    #TODO merge sigma40 in calib
    def sigma0_at_40d(self, sigma0, incidloc, out40):
    
        app1 = otbApplication.Registry.CreateApplication("BandMath")
        app1.SetParameterStringList("il",[sigma0,incidloc])
        app1.SetParameterString("out", out40+"?gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")
        app1.SetParameterString("exp", "im1b1*(cos("+str(math.pi/180)+"*40)^2)/(cos("+str(math.pi/180)+"*im2b1)^2) > 0 ?"+
                                " im1b1*(cos("+str(math.pi/180)+"*40)^2)/(cos("+str(math.pi/180)+"*im2b1)^2): 0")
        app1.ExecuteAndWriteOutput()
        
        
    
    
    def create_incidenceAngle_from_surface_raster(self, annofiles):
        thetasurf = os.path.basename(self.out_ORTHO.auxfilespath["THETASURF"])
        tmpdir = os.path.dirname(self.out_ORTHO.auxfilespath["THETASURF"])
        
        vrtfilebase = thetasurf.replace("_THETASURF.TIF","_THETA.vrt")
        csvfilebase = thetasurf.replace("_THETASURF.TIF","_THETA.csv")
        vrtfile = os.path.join(tmpdir,vrtfilebase)
        csvfile = os.path.join(tmpdir,csvfilebase)
        layername = os.path.basename(vrtfile)[:-4]
        
        orthoimg = self.out_ORTHO.get_gdalraster()
        MINX, MINY, MAXX, MAXY = self.out_ORTHO.enveloppe_wgs84.extent
        
        with open(vrtfile,'w') as vrt:
            vrt.write("<OGRVRTDataSource>\n")
            vrt.write('    <OGRVRTLayer name="'+layername+'">\n')
            vrt.write('        <SrcDataSource>'+csvfile+'</SrcDataSource>\n')
            vrt.write("        <GeometryType>wkbPoint</GeometryType>\n")
            vrt.write("        <LayerSRS>WGS84</LayerSRS>\n")
            vrt.write('        <GeometryField encoding="PointFromColumns" x="Easting" y="Northing" z="incidence"/>\n')
            vrt.write("    </OGRVRTLayer>\n")
            vrt.write("</OGRVRTDataSource>\n")
        
        for annofile in annofiles:
            logger.debug("annofile: "+ os.path.basename(annofile))
            tree = ET.parse(annofile)
            root = tree.getroot()
            
            geoloclist = root.find(".//geolocationGridPointList")
            
            if geoloclist == None :
                logger.error("Can't find geolocationGridPointList tag in xml file.")
                raise Exception("Can't find geolocationGridPointList tag in xml file.")
            
            header = True
            if os.path.exists(csvfile):
                header = False
            
                
            with open(csvfile,'a') as csvf:
                if header:
                    csvf.write("Easting,Northing,incidence\n")
                for child in geoloclist:
                    lat = child.find(".//latitude").text
                    lon = child.find(".//longitude").text
                    alti = float(child.find(".//height").text)
                    incdem = float(child.find(".//incidenceAngle").text)
#                     inc = incdem - alti/693000.
                    csvf.write(lon+","+lat+","+str(incdem)+"\n")
     
    
        cmd = ["gdal_grid","-a","linear","-txe", str(MINX), str(MAXX), "-tye", str(MINY), str(MAXY), 
               "-outsize", str(orthoimg.width), str(orthoimg.height), "-of", "GTiff", "-ot", "Float32", 
               "-l", layername, vrtfile, self.out_ORTHO.auxfilespath["THETASURF"]]
    
        try:
            process_command(cmd)
        except Exception as err:
            logger.error("gdal_grid process failed : "+err)
            raise
            
    def create_incidenceAngle_from_ellipsoid_raster(self):
        
        logger.debug("Processing "+self.out_ORTHO.auxfilespath["THETAELLI"])
        app41 = otbApplication.Registry.CreateApplication("Superimpose")
        # The following lines set all the application parameters:
        app41.SetParameterString("inm", self.out_ORTHO.auxfilespath["THETASURF"])
        app41.SetParameterString("inr", self.out_ORTHO.filepath)
        app41.SetParameterString("interpolator","bco")
        app41.SetParameterString("out", "temp41.tif")
        
        
        # The following line execute the application
        app41.Execute()
        
        app42 = otbApplication.Registry.CreateApplication("Superimpose")
        # The following lines set all the application parameters:
        app42.SetParameterString("inm", self.demraster.filepath)
        app42.SetParameterString("inr", self.out_ORTHO.filepath)
        app42.SetParameterString("interpolator","bco")
        app42.SetParameterString("out", "temp41.tif")
        
        # The following line execute the application
        app42.Execute()
        
        appS = otbApplication.Registry.CreateApplication("BandMath")
        appS.SetParameterStringList("il", [self.out_ORTHO.filepath])
        appS.AddImageToParameterInputImageList("il", app42.GetParameterOutputImage("out"))
        appS.AddImageToParameterInputImageList("il", app41.GetParameterOutputImage("out"))
        # Define Input im2: Band Red (B4)
        appS.SetParameterString("out", self.out_ORTHO.auxfilespath["THETAELLI"]+"?&gdal:co:COMPRESS=DEFLATE&nodata=0")
        appS.SetParameterString("exp", "im1b1 == 0?0:im3b1 - im2b1/693000" )
        
        appS.ExecuteAndWriteOutput()
        
        
    
    def create_local_incidenceAngle_raster(self, annofiles:list):
        logger.debug("Processing "+self.out_ORTHO.auxfilespath["THETALOC"])
        
        inslope = self.sloperaster.filepath
        inaspect = self.aspectraster.filepath
    
        plateformheading=[]
        orbite = None
        for annofile in annofiles:
        
            tree = ET.parse(annofile)
            root = tree.getroot()
            
            plateformheading.append(float(root.find(".//platformHeading").text))

            if orbite == None :
                orbite = root.find(".//pass").text

        if orbite.lower == "ascending" :
            satdir = 90
        else :
            satdir = -90
            
        if len(plateformheading) == 0:
            logger.error("Error: Can't find plateformHeading in annotation files")
            raise Exception("Error in plateformHeading in file : " +annofile)
        elif len(plateformheading) == 1:
            # azimuthsat = (plateformheading[0]+90) % 360
            azimuthsat = (plateformheading[0]+satdir) % 360
        else:
            # azimuthsat = (math.fsum(plateformheading)/len(plateformheading)+90) % 360
            azimuthsat = (math.fsum(plateformheading)/len(plateformheading)+satdir) % 360
            
    
        
        app41 = otbApplication.Registry.CreateApplication("Superimpose")
        # The following lines set all the application parameters:
        app41.SetParameterString("inm", self.out_ORTHO.auxfilespath["THETASURF"])
        app41.SetParameterString("inr", self.out_ORTHO.filepath)
        app41.SetParameterString("interpolator","bco")
        app41.SetParameterString("out", "temp41.tif")
        
        
        # The following line execute the application
        app41.Execute()
        
        
        app43 = otbApplication.Registry.CreateApplication("Superimpose")
        # The following lines set all the application parameters:
        app43.SetParameterString("inm", inslope)
        app43.SetParameterString("inr", self.out_ORTHO.filepath)
        app43.SetParameterString("interpolator","bco")
        app43.SetParameterString("out", "temp43.tif")
        
        
        # The following line execute the application
        app43.Execute()
        
        
        app44 = otbApplication.Registry.CreateApplication("Superimpose")
        # The following lines set all the application parameters:
        app44.SetParameterString("inm", inaspect)
        app44.SetParameterString("inr", self.out_ORTHO.filepath)
        app44.SetParameterString("interpolator","bco")
        app44.SetParameterString("out", "temp44.tif")
        
        
        # The following line execute the application
        app44.Execute()
        
        
        appS = otbApplication.Registry.CreateApplication("BandMath")
        #im1b1 -> vv
        #im2b1 -> incidence surf
        #im3b1 -> slope
        #im4b1 -> aspect
        CONVRAD=str(math.pi/180)
        CONVDEG=str(180/math.pi)
        appS.SetParameterStringList("il", [self.out_ORTHO.filepath])  
        appS.AddImageToParameterInputImageList("il", app41.GetParameterOutputImage("out")) 
        appS.AddImageToParameterInputImageList("il", app43.GetParameterOutputImage("out")) 
        appS.AddImageToParameterInputImageList("il", app44.GetParameterOutputImage("out"))
        appS.SetParameterString("out", self.out_ORTHO.auxfilespath["THETALOC"]) #"tempS.tif")
        appS.SetParameterString("exp", "im1b1 == 0?0:"+
                                    "acos("+
                                        f"cos(im3b1*{CONVRAD})*cos(im2b1*{CONVRAD})"+
                                        f"+sin(im3b1*{CONVRAD})*sin(im2b1*{CONVRAD})"+
                                        f"*cos(({azimuthsat}-im4b1)*{CONVRAD})"+
                                    f")*{CONVDEG}")
        
        # appS.Execute()
        appS.ExecuteAndWriteOutput()
        # +"?gdal:co:COMPRESS=DEFLATE"
        # appM = otbApplication.Registry.CreateApplication("ManageNoData")
        # appM.SetParameterInputImage("in", appS.GetParameterOutputImage("out"))
        # appM.SetParameterString("out", self.out_ORTHO.auxfilespath["THETALOC"])
        # appM.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_float)
        # appM.SetParameterString("mode", "changevalue")
        # appM.ExecuteAndWriteOutput()
        
        
