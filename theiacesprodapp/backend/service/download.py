import os, glob, json, osgeo, math, shutil, yaml, time
from os.path import abspath, join, dirname
from datetime import datetime, timedelta
import cdsapi, planetary_computer, requests
import rasterio
from rasterio import mask
from pystac_client import Client
from pystac import Item
from theiacesprodapp.backend.data import ERA5, Raster, S1OrthoRaster, Sentinel2PCStac, AOIvector, ImageZip, Sentinel1Zip
from theiacesprodapp.backend import utils
from theiacesprodapp import models as mod
from django.contrib.gis.gdal.geometries import OGRGeometry
from django.contrib.gis.gdal import SpatialReference, CoordTransform
from django.contrib.gis.geos import GEOSGeometry
from eodag.utils.logging import setup_logging
from eodag.api.core import EODataAccessGateway
from eodag.api.search_result import SearchResult
from eodag.utils import ProgressCallback
from tqdm import tqdm
from tqdm.contrib.logging import logging_redirect_tqdm
from typing import List, Tuple
import logging 

logger = logging.getLogger(__name__)
user_conf_file_path = join(dirname(abspath(__file__)),"resources/user_conf.yml")

class SentinelEodag():
    user_conf_file_path = user_conf_file_path
    product_types = ['S1_SAR_GRD','S2_MSI_L2A','S2_MSI_L3A_WASP']
    
    setup_logging(verbose=2,no_progress_bar=False)

    def __init__(self) -> None:

        #Test user_conf_file 
        with open(self.user_conf_file_path,'r', encoding='utf-8') as ucf:
            yaml_content = yaml.safe_load(ucf)
            logger.info("Loading user conf...")
#             logger.debug(str(yaml_content))
            if yaml_content["scihub"]["api"]["credentials"]["username"] == "":
                logger.error(f"Credentials not set for scihub provider in resources/user_conf.yml")
                raise
            # if yaml_content["peps"]["auth"]["credentials"]["username"] == "":
            #     logger.error(f"Credentials not set for peps provider in resources/user_conf.yml")
            #     raise
            if yaml_content["theia"]["auth"]["credentials"]["ident"] == "":
                logger.error(f"Credentials not set for theia provider in resources/user_conf.yml")
                raise
        
        self.mEODataAccessGateway = EODataAccessGateway(user_conf_file_path=self.user_conf_file_path)
        
        logger.info("EODataAccessGateway initialized.")

    # def __init__(self, provider, login, passwd) -> None:

    #     os.environ[f"EODAG__{provider}__API__CREDENTIALS__USERNAME"] = login
    #     os.environ[f"EODAG__{provider}__API__CREDENTIALS__PASSWORD"] = passwd

    def get_geom_tile(self, mrgstile:str)->GEOSGeometry:
        qtiles = mod.TileEnvelope.objects.filter(tile__exact = mrgstile)
        if len(qtiles) != 1 :
            logger.error(f"Database request for tile {mrgstile} failed.")
            raise 

        return qtiles[0].geom
    
    def search_and_download_from_wkt(self, prodtypenum:int, zonename:str, wktpolygon:str, startdate:str, enddate:str, downdir:str, logdir=None):
        self.prodtypenum = prodtypenum
        self.startdate = startdate
        self.enddate = enddate
        self.downdir = downdir
        self.logdir = logdir
        self.mrgstile = zonename

        self.geomsearch = wktpolygon

        logger.info("Eodag request : {}, {}, {}, {}".format(
                    self.product_types[self.prodtypenum],
                    self.startdate,
                    self.enddate,
                    self.geomsearch
        ))

        return self.__process()
    
    def search_and_download_from_tile(self, prodtypenum:int, mrgstile:str, startdate:str, enddate:str, downdir:str, logdir=None):
        self.prodtypenum = prodtypenum
        self.mrgstile = mrgstile
        self.startdate = startdate
        self.enddate = enddate
        self.downdir = downdir
        self.logdir = logdir

        
        geomtile = self.get_geom_tile(mrgstile)
        
        logger.debug(geomtile.wkt)

        if self.prodtypenum  == 0:            
            self.geomsearch = geomtile.wkt
        elif self.prodtypenum in [1,2]:
            self.geomsearch = geomtile.centroid.wkt

        logger.info("Eodag request : {}, {}, {}, {}".format(
                    self.product_types[self.prodtypenum],
                    self.startdate,
                    self.enddate,
                    self.geomsearch
        ))

        return self.__process()

    def order_online_first(self, products_scihub,products_peps) -> Tuple[dict,int,int]:
        nbsci_online=0  
        nbpeps_online=0
        products_availability={}

        for ps in products_scihub :
            found=False
            for pp in products_peps :
                if pp.properties["id"] == ps.properties["id"]:
                    if ps.properties["storageStatus"] == "ONLINE" and pp.properties["storageStatus"] == "ONLINE":
                        products_availability[ps.properties["id"]] = [pp,ps]
                        nbsci_online += 1
                        nbpeps_online += 1
                    elif ps.properties["storageStatus"] == "ONLINE" and pp.properties["storageStatus"] == "OFFLINE":
                        products_availability[ps.properties["id"]] = [ps,pp]
                        nbsci_online += 1
                    elif ps.properties["storageStatus"] == "OFFLINE" and pp.properties["storageStatus"] == "ONLINE":
                        products_availability[ps.properties["id"]] = [pp,ps]
                        nbpeps_online += 1
                    elif ps.properties["storageStatus"] == "OFFLINE" and pp.properties["storageStatus"] == "OFFLINE":
                        products_availability[ps.properties["id"]] = [ps,pp]
                        
                    found=True
                    break
            if found == False:
                products_availability[ps.properties["id"]] = [ps]
                if ps.properties["storageStatus"] == "ONLINE":
                    nbsci_online += 1
        
        for pp in products_peps :
            found=False
            
            if not pp.properties["id"] in products_availability.keys():
                products_availability[pp.properties["id"]] = [pp]
                if pp.properties["storageStatus"] == "ONLINE":
                    nbpeps_online += 1
        

        return products_availability, nbsci_online, nbpeps_online

    def check_productsId(self, products:SearchResult):

        prodorderbydates={}
        for p in products:
            pdate = p.properties["id"].split("_")[4].split("T")
            pcapt = p.properties["id"].split("_")[0]
            if pcapt + pdate[0] in prodorderbydates :
                prodorderbydates[pcapt + pdate[0]] = [p]
            else :
                continue

            for t in products:
                tdate = p.properties["id"].split("_")[4].split("T")
                tcapt = p.properties["id"].split("_")[0]
                if tdate[0] == pdate[0] and tdate[1] == pdate[1] and tcapt == pcapt :
                    prodorderbydates[pcapt + pdate[0]].append(t)
    
        conflictIds={}
        for d in prodorderbydates :
            trackstart=[]
            trackend=[]
            if len(prodorderbydates[d]) > 2:
                for p in prodorderbydates[d] :
                    trackstart.append(p.properties["id"].split("_")[4].split("T")[1])
                    trackend.append(p.properties["id"].split("_")[5].split("T")[1])
                
                if len(prodorderbydates[d]) == 3:
                    popfind = None
                    for i in range(3) :
                        if not (trackend[i] in trackstart or trackstart[i] in trackend):
                            popfind = d[3:]+"T"+trackstart[i]+"_"+d[3:]+"T"+trackend[i]
                            popind = None
                            for p in products :
                                if popfind in p.properties["id"]:
                                    popind = products.index(p)
                                    logger.info("Duplicate ID found. Removing third product :"+p.properties["id"])
                                    products.pop(popind)
                                    break
                            break

                    
                
                if len(prodorderbydates[d]) > 3:
                    logger.error("Error")
                    raise Exception()
    
    def check_s2_tile(self, products:SearchResult):
        if not self.mrgstile.startswith("T"):
            raise Exception(f"{self.mrgstile} is not a valid MRGS tile")
        conflictIds=[]
        for prod in products :
            if not self.mrgstile in prod.properties["id"] :
                conflictIds.append(products.index(prod))
                logger.warn(f"Wrong Tile in superposition. Removing {prod.properties['id']}")
        i=0
        for popind in conflictIds :
            products.pop(popind-i)    
            i+=1



    def __process(self):
  

        if None == self.logdir:
            self.logdir = self.downdir

        logbase = os.path.join(self.logdir,self.product_types[self.prodtypenum]+"_"+self.mrgstile+"_"+self.startdate+"_"+self.enddate)
        
        products_availability={}
        if self.prodtypenum in [0,1]:
            logger.info("Search on Scihub...")
            self.mEODataAccessGateway.set_preferred_provider("scihub")
            
            
            products_scihub = self.mEODataAccessGateway.search_all(
                    productType=self.product_types[self.prodtypenum],
                    start=self.startdate,
                    end=self.enddate,
                    geom=self.geomsearch
                )

            self.check_productsId(products_scihub)
            if self.prodtypenum == 1 :
                self.check_s2_tile(products_scihub)

            logger.info(
                    f"The Scihub search has found an estimated number of {len(products_scihub)} products matching your criteria "
                )
            logger.info("Search on Peps...")
            self.mEODataAccessGateway.set_preferred_provider("peps")
            
            products_peps = self.mEODataAccessGateway.search_all(
                    productType=self.product_types[self.prodtypenum],
                    start=self.startdate,
                    end=self.enddate,
                    geom=self.geomsearch
                )
            self.check_productsId(products_peps)
            if self.prodtypenum == 1 :
                self.check_s2_tile(products_peps)

            logger.info(
                    f"The PEPS search has found an estimated number of {len(products_peps)} products matching your criteria "
                )
            
            products_availability, nbsci_online, nbpeps_online = self.order_online_first(products_scihub, products_peps)
            
            logger.info(f"Scihub has got {nbsci_online} ONLINE products over {len(products_scihub)}")
            logger.info(f"PEPS has got {nbpeps_online} ONLINE products over {len(products_peps)}")
            
        elif self.prodtypenum == 2:
            logger.info("Search on Theia...")
            self.mEODataAccessGateway.set_preferred_provider("theia")
            
            products_theia = self.mEODataAccessGateway.search_all(
                    productType=self.product_types[self.prodtypenum],
                    start=self.startdate,
                    end=self.enddate,
                    geom=self.geomsearch
                )
            self.check_s2_tile(products_theia)

            logger.info(
                    f"The THEIA search has found an estimated number of {len(products_theia)} products matching your criteria "
                )
            nbtheia_online=0
            for pt in products_theia :
                products_availability[pt.properties["id"]] = [pt]
                if pt.properties["storageStatus"] == "ONLINE":
                        nbtheia_online += 1
            
            logger.info(
                    f"THIEA has got {nbtheia_online} ONLINE products over {len(products_theia)}"
                )
            
        else:
            logger.error("error")
            return
        
        self.products_availability = products_availability
        
        self.start_downloads(logbase)

        filelist=[]
        for pd in self.products_downloaded :
            filelist.append(self.products_downloaded[pd][0].location)

        return filelist

    def __export_geojson(self,logbase, remain):
        # nowstr = datetime.now().strftime("%Y%m%dT%H%M%S")
        serialfile = logbase+"_DownloadedProducts.geojson"
        remainserialfile = logbase+"_RemainingProducts.geojson"
        altremainserialfile = logbase+"_AlternateRemainingProducts.geojson"
        
        if len(self.products_downloaded) > 0 :

            downloadedprods = []
            for d in self.products_downloaded.keys() :
                downloadedprods.append(self.products_downloaded[d][0])
            
            logger.info(f"Serializing downloaded products to {serialfile}")
            dps = SearchResult(downloadedprods)
            self.mEODataAccessGateway.serialize(dps, serialfile)

        
        if remain == 1 :
            
            remainingprods = []
            altremainingprods = []
            for d in self.products_availability.keys() :
                remainingprods.append(self.products_availability[d][0])
                if len(self.products_availability[d]) == 2 :
                    altremainingprods.append(self.products_availability[d][1])
            
            logger.info(f"Serializing downloaded products to {remainserialfile}")
            rps = SearchResult(remainingprods)
            self.mEODataAccessGateway.serialize(rps, remainserialfile)
            
            if len(altremainingprods) > 0 :
                logger.info(f"Serializing alternate products to {altremainserialfile}")
                arps = SearchResult(altremainingprods)
                self.mEODataAccessGateway.serialize(arps, altremainserialfile)

            
        
        

    def import_geojson(self,products_geojson,alternate_products_geojson=None):
        
        self.products_availability = {}
        deserialized_products = self.mEODataAccessGateway.deserialize(products_geojson)
        
        for pt in deserialized_products :
            self.products_availability[pt.properties["id"]] = [pt]
        
        if not alternate_products_geojson == None :
            deserialized_alt_products = self.mEODataAccessGateway.deserialize(alternate_products_geojson)
            for pp in deserialized_alt_products :
                if pp.properties["id"] in self.products_availability.keys():
                    self.products_availability[pp.properties["id"]].append(pp)
                else:
                    self.products_availability[pp.properties["id"]] = [pp]
                    
        logger.info("Geojson files successfully imported")
        logger.info("Use start_downloads() to launch downloads")
    
    
    def start_downloads(self,logbase):
        
        if not os.path.exists(self.downdir):
            os.makedirs(self.downdir)
        if not os.path.exists(os.path.dirname(logbase)):
            os.makedirs(os.path.dirname(logbase))
        
        self.products_downloaded={}
        passwait=[5,30,60]
        still2download = True
        nbProducts2d = len(self.products_availability)
        nb_allretry = 0
        
        
            
        while still2download:
            for p in sorted(self.products_availability, key=lambda k: len(self.products_availability[k]), reverse=True):
                
                for providerprod in self.products_availability[p] :
                    provider = providerprod.provider
                    logger.info(f"Starting Download {p} from {provider}...")
                    self.mEODataAccessGateway.set_preferred_provider(provider)
                    failed=False
                    try:
                        self.mEODataAccessGateway.download(providerprod, extract=False, outputs_prefix=self.downdir,wait=2, timeout=5)
                    except Exception as err:
                        failed=True
                        logger.error(err)
                    
                    if providerprod.location[:5] == "file:":
                        if len(self.products_availability[p]) == 2:
                            self.products_availability[p]=[providerprod]
                        self.products_downloaded[p] = self.products_availability[p]
                        break

            for p in self.products_downloaded.keys():
                if p in self.products_availability.keys():
                    self.products_availability.pop(p)
                    
            nb_allretry += 1
             
            if len(self.products_downloaded) == nbProducts2d :
                still2download = False
                logger.info(
                    f"All downloads successful."
                    )
                self.__export_geojson(logbase, 0)
                
                logger.info("Done.")
                
            elif nb_allretry == len(passwait) :
                still2download = False
                logger.info(
                    """
                    Max number of retries has been completed.
                    There is still products to download.
                    Use import_geoson() with _RemainingProducts.geojson file to restart downloads.
                    """
                    )
                self.__export_geojson(logbase, 1)
                
                logger.info("Done.")
            else:
                logger.info(f"Pass n#{nb_allretry} has reached {len(self.products_downloaded)} over {nbProducts2d} expected")
                logger.info(f"Waiting {passwait[nb_allretry-1]} minutes before retrying...")
                restartd = datetime.now()+timedelta(minutes=passwait[nb_allretry-1])
                restartstr = restartd.strftime("%Y/%m/%d-%H:%M:%S")
                logger.info(f"Restart at {restartstr}")
                time.sleep(passwait[nb_allretry-1]*60)
             


    

class ClimateDataSystem:

    
    def __init__(self, s1ortholist:List[S1OrthoRaster], imgref:Raster, outdir:str, gap:str='3m'):

        
        self.s1ortholist = s1ortholist
        self.imgref = imgref
        self.outdir = outdir
        self.gap = gap
        

    def download_cds_3m(self, m_year, m_month, m_day, m_time, m_lonmin, m_latmin, m_lonmax, m_latmax, outfile):
        c = cdsapi.Client()
        requ = (
        'reanalysis-era5-land',
        {
            'format': 'grib',
            'variable': '2m_temperature',
            'year': m_year,#AAAA
            'month': m_month,#MM
            'day': m_day,#DD
            'time': m_time,#HH:00
            'area': [
                m_latmax, m_lonmin, m_latmin,
                m_lonmax,
            ],
        },
        outfile)
        try:
            c.retrieve(requ[0],requ[1],requ[2])
        except Exception as err:
            print(err)
            return requ
        
        return requ

    def download_cds_6d(self, m_year, m_month, m_day, m_time, m_lonmin, m_latmin, m_lonmax, m_latmax, outfile):
        c = cdsapi.Client()
        requ = (
        'reanalysis-era5-pressure-levels',
        {
            'product_type': 'reanalysis',
            'format': 'grib',
            'variable': 'temperature',
            'pressure_level': '1000',
            'year': m_year,#AAAA
            'month': m_month,#MM
            'day': m_day,#DD
            'time': m_time,#HH:00
            'area': [
                m_latmax, m_lonmin, m_latmin,
                m_lonmax,
            ],
        },
        outfile)
        try:
            c.retrieve(requ[0],requ[1],requ[2])
        except Exception as err:
            logger.error(err)
            requ[0]=1
            return requ
        
        return requ
    
    def round_s1time(self, s1time):
        m_hour = int(s1time[:2])
        m_minu = int(s1time[2:4])
        m_seco = int(s1time[4:6])
        
        if m_seco >= 30 :
            m_minu += 1
        if m_minu >= 30 :
            m_hour += 1
        if m_hour == 24:
            m_hour -= 1
        st_hour = str(m_hour)
        if m_hour < 10 :
            st_hour = "0"+st_hour
        return st_hour+":00"
        
    def start(self)->List[ERA5]:
        
        if not os.path.exists(self.outdir):
            os.makedirs(self.outdir)
        
        extend = self.imgref.enveloppe_wgs84.extent
        zone = self.imgref.mgrstilename
        if zone == None:
            logger.error("SRTM tile name not found in reference image name")
            raise
    
        era5list=[]
        for s1 in self.s1ortholist:
            
            stdate = s1.s1date.split("T")[0]
            sttime = s1.s1date.split("T")[1]
            rdtime = self.round_s1time(sttime) 
            
            if self.gap == '3m':
                outfile = os.path.join(self.outdir,"ERA5-2m_"+zone+"_"+s1.s1date+".grib")
                logger.info("Downloading CDS 'reanalysis-era5-land' : "+outfile)
                request = self.download_cds_3m(stdate[:4], stdate[4:6], stdate[6:8], rdtime, extend[0], extend[1], extend[2], extend[3], outfile)
            elif self.gap == '6d':
                outfile = os.path.join(self.outdir,"ERA5-1000hpa_"+zone+"_"+s1.s1date+".grib")
                print("Downloading CDS 'reanalysis-era5-pressure-levels' : "+outfile)
                request = self.download_cds_6d(stdate[:4], stdate[4:6], stdate[6:8], rdtime, extend[0], extend[1], extend[2], extend[3], outfile)
            else:
                logger.error("Wrong gap argument")
                raise

            era5list.append(ERA5(outfile))

        return era5list

class Dataspace_Copernicus_Catalog():
    BASE_URL = "https://catalogue.dataspace.copernicus.eu/odata/v1/Products?$filter="
    collections = ["SENTINEL-1","SENTINEL-2"]
    
    def __init__(self, data_collection:str, start_date:str, end_date:str, aoi_wkt:str, mrgstile:str, outdir:str) -> None:
        self.start_date = start_date
        self.end_date = end_date
        self.data_collection = data_collection
        self.aoi_wkt = aoi_wkt
        self.mrgstile = mrgstile
        self.outdir = outdir

        if not os.path.exists(self.outdir):
            os.makedirs(self.outdir)

        with open(user_conf_file_path,'r', encoding='utf-8') as ucf:
            yaml_content = yaml.safe_load(ucf)
            logger.info("Loading user conf...")
#             logger.debug(str(yaml_content))
            if yaml_content["scihub"]["api"]["credentials"]["username"] == "":
                logger.error(f"Credentials not set for scihub provider in resources/user_conf.yml")
                raise
            self.username = yaml_content["scihub"]["api"]["credentials"]["username"]
            self.password = yaml_content["scihub"]["api"]["credentials"]["password"]

    def get_wkt_tile(mrgstile:str)->GEOSGeometry:
        qtiles = mod.TileEnvelope.objects.filter(tile__exact = mrgstile)
        if len(qtiles) != 1 :
            logger.error(f"Database request for tile {mrgstile} failed.")
            raise 

        return qtiles[0].geom
    
    def filter_s2l2a_on_proddiscrim_time(self, json_results:List[dict])->List[dict]:
        selected_json=[]
        class_prods={}
        for prod in json_results:
            proddate = prod['Name'].split("_")[2]
            discdate = prod['Name'].split("_")[6].split(".")[0]
            class_prods.setdefault(proddate,[]).append([discdate, prod])
        
        for k in class_prods:
            sort_classified = sorted(class_prods[k], reverse=True)
            selected_json.append(sort_classified[0][1])
        
        return selected_json

    
    def search_dataspace_odata(self):
        filter_collection = f"Collection/Name eq '{self.data_collection}'"
        filter_aoi = f"OData.CSC.Intersects(area=geography'SRID=4326;{self.aoi_wkt}')"
        filter_date = f"ContentDate/Start gt {self.start_date}T00:00:00.000Z and ContentDate/Start lt {self.end_date}T23:00:00.000Z"
        filter_base = "not contains(Name,'_COG') and contains(Name,'.SAFE')"
        filter_cloud = f"Attributes/OData.CSC.DoubleAttribute/any(att:att/Name eq 'cloudCover' and att/OData.CSC.DoubleAttribute/Value le 50.00)"
        filter_s2l2a = "contains(Name,'MSIL2A')"
        filter_tile = f"contains(Name,'{self.mrgstile}')"
        filter_s1grd = "contains(Name,'IW_GRDH')"

        request_url = " and ".join([self.BASE_URL+filter_collection,filter_aoi,filter_date,filter_base])
        if self.data_collection == self.collections[1]:
            request_url = " and ".join([request_url,filter_cloud,filter_s2l2a,filter_tile])
        elif self.data_collection == self.collections[0]:
            request_url = " and ".join([request_url,filter_s1grd])
        else:
            raise Exception("Unknown Collection")

        request_url += "&$top=500"

        try:
            r = requests.get(request_url)
            r.raise_for_status()

        except Exception as e:
            logger.error(e)
            raise Exception(
                f"OData request failed."
            )
        
        self.search_results = r.json()['value']

        if self.data_collection == self.collections[1]:
            selected = self.filter_s2l2a_on_proddiscrim_time(self.search_results)
            self.search_results = selected


        logger.info(f"{len(self.search_results)} products found:")
        for prod in self.search_results:
            logger.info(f"{prod['Id']} / {prod['Name']}")

    def get_access_token(self, username: str, password: str) -> str:
        data = {
            "client_id": "cdse-public",
            "username": username,
            "password": password,
            "grant_type": "password",
        }
        try:
            r = requests.post(
                "https://identity.dataspace.copernicus.eu/auth/realms/CDSE/protocol/openid-connect/token",
                data=data,
            )
            r.raise_for_status()
        except Exception as e:
            logger.error(e)
            logger.error(f"You have exceeded your rate limit. Waiting 2 minutes before retrying.")
            time.sleep(120)
            return "exceed"
        return r.json()["access_token"]
    
    def download_data(self):

        access_token = self.get_access_token(self.username, self.password)
        if access_token == "exceed":
            retry=1
            while retry == 5 or access_token != "exceed":
                access_token = self.get_access_token(self.username, self.password)
                retry+=1

        if access_token == "exceed":
            raise Exception("Failed to get access token from Dataspace.")

        token_end_time = datetime.now()+timedelta(seconds=600)
        token_refresh_end_time = datetime.now()+timedelta(seconds=3600)

        download_list=[]
        for prod in self.search_results :
            if datetime.now() > token_end_time or datetime.now() > token_refresh_end_time :
                access_token = self.get_access_token(self.username, self.password)
                if access_token == "exceed":
                    retry=1
                    while retry == 5 or access_token != "exceed":
                        access_token = self.get_access_token(self.username, self.password)
                        retry+=1
                if access_token == "exceed":
                    raise Exception("Failed to get access token from Dataspace.")
        
                token_end_time = datetime.now()+timedelta(seconds=600)
                if datetime.now() > token_refresh_end_time :
                    token_refresh_end_time = datetime.now()+timedelta(seconds=3600)

            productid = prod["Id"]
            outfile = os.path.join(self.outdir,prod["Name"].split(".")[0]+".zip")
            outchecksum = os.path.join(self.outdir,prod["Name"]+".md5")

            checkmd5=None
            for alg in prod['Checksum'] :
                if alg['Algorithm'] == "MD5":
                    checkmd5 = alg['Value']	
            if checkmd5:
            #     downfile_checksum = utils.compute_md5(outfile)
            #     if checkmd5 != downfile_checksum :
            #         logger.error(f"Checksum failed for archive {}")
                logger.info(f"Writting checksum md5 file for {prod['Name']}")
                with open(outchecksum,"w") as md5 :
                    md5.write(checkmd5)
            else:
                logger.error(f"No checksum found for {prod['Name']}")

            if os.path.exists(outfile):
                logger.warning(f"{outfile} already exists. Skipping.")
                download_list.append(outfile)
                continue

            url = f"https://zipper.dataspace.copernicus.eu/odata/v1/Products({productid})/$value"

            headers = {"Authorization": f"Bearer {access_token}"}

            try:
                session = requests.Session()
                session.headers.update(headers)
                response = session.get(url, headers=headers, stream=True)
                response.raise_for_status()
                total = int(response.headers.get('content-length', 0))
                with logging_redirect_tqdm():
                    with open(outfile, "wb") as file, tqdm(
                        desc=os.path.basename(outfile),
                        total=total,
                        unit='iB',
                        unit_scale=True,
                        unit_divisor=1024,
                    ) as bar:
                        for chunk in response.iter_content(chunk_size=8192):
                            if chunk:
                                size = file.write(chunk)
                                bar.update(size)

                download_list.append(outfile)

            except Exception as e:
                logger.error(f"Download failed with error: {e}")
                continue
        
        rmfile=[]
        logger.info(f"## Verifying downloaded images checksums")

        for testf in download_list:
            zfile = ImageZip(testf)
            try:
                if zfile.verify_checksum_zip():
                    logger.info(f"Checksum succeded on {os.path.basename(testf)}")
                else:
                    logger.info(f"Checksum failed on {os.path.basename(testf)}. Deleting file")
                    rmfile.append(testf)
            except Exception as e:
                continue
        
        if len(rmfile) > 0:
            for dz in rmfile:
                download_list.pop(download_list.index(dz))
                os.remove(dz)
            
        return download_list
            
        
class Microsoft_Stac_Catalog():

    def __init__(self, collection:str, aoivect:AOIvector, time_range:str, outdir:str):
        self.collection = collection
        self.aoivect = aoivect
        self.bbox = aoivect.get_geom(4326).extent
        self.time_range = time_range
        self.outdir = outdir
        self.items = None
        self.maxcloud = 50
        # filter_name = {"op":"=","args":[{"property":"s2:mgrs_tile"},"31TGM"]}
        # query_tile = {"s2:mgrs_tile": {"=": "31TGM"}}
        
    def search(self):

        logger.debug("Search params :")
        logger.debug(self.bbox)
        logger.debug(self.time_range)

        catalog = Client.open("https://planetarycomputer.microsoft.com/api/stac/v1")

        search = catalog.search(collections=[self.collection], 
                                bbox=self.bbox, 
                                datetime=self.time_range,
                                query={"eo:cloud_cover": {"lt": self.maxcloud}}
                                )
        self.items = search.item_collection()
        logger.info(f"Nbr of {self.collection} found : "+ str(len(self.items)))
        

    def download(self)->Tuple[bool,List[Sentinel2PCStac]]:
        MAX_DOWNLOAD_FILE=5
        downfile = []
        geojson=None
        downloaded_files=0
        for it in self.items :
            skip=False
            #
            if self.collection == "sentinel-2-l2a":
                assetslist = ["B04","B08","SCL"]
            elif self.collection == "sentinel-1-grd":
                assetslist = ["vv","schema-product-vv"]

            proid = it.id
            pdir = os.path.join(self.outdir,proid)
            if not os.path.exists(pdir):
                os.makedirs(pdir)

            product = Sentinel2PCStac(pdir)
            already_done = False

            for band in assetslist :
                file = it.assets[band].href.split("/")[-1]
                outfile = os.path.join(pdir,file)
                logger.info(f"Downloading crop in file : {outfile}")

                if os.path.exists(outfile) :
                    logger.info(f"File already exists. Passing.")
                    product.set_band(band,outfile)
                    if product.is_all_bands_ok():
                        already_done = True
                    continue
                
                try:
                    with rasterio.open(planetary_computer.sign(it).assets[band].href ) as ds:
                        
                        if geojson == None :
                            geom = self.aoivect.get_geom(ds.crs.to_epsg())
                            geojson = json.loads(geom.json)
                            logger.info(f"Using AOI : {geojson}")

                        out_image, out_transform = mask.mask(ds, [geojson], crop=True)
                        out_meta = ds.meta
                        
                        out_meta.update({"driver": "GTiff",
                                "height": out_image.shape[1],
                                "width": out_image.shape[2],
                                "transform": out_transform})                                           

                    with rasterio.open(outfile, "w", **out_meta) as dest:
                        dest.write(out_image)
                except ValueError as ve :
                    logger.warn(str(ve))
                    logger.warn("Skipping :"+proid)
                    skip=True
                    break
                except Exception as ex:
                    logger.error(str(ex))
                    logger.error("Error while downloading : "+proid)
                    logger.error("Skipping :"+proid)
                    skip=True
                    break
                product.set_band(band,outfile)

            if skip:
                if os.path.exists(pdir):
                    shutil.rmtree(pdir)
                continue

            downfile.append(product)
            if not already_done :
                downloaded_files+=1
            if downloaded_files == MAX_DOWNLOAD_FILE:
                return (True, downfile)

        return (False, downfile)

                
class Esa_Srtm():
    URLSRTM = "http://step.esa.int/auxdata/dem/SRTMGL1/"
    srtmsuf = ".SRTMGL1.hgt.zip"
    srtmarch = ".hgt"  

    def __init__(self, refraster:Raster, outdir:str):
        self.refraster = refraster
        self.outdir = outdir
        xmin,ymin,xmax,ymax = refraster.enveloppe_wgs84.extent
        self.tileslist = self.find_srtm_hgt_name(xmin-4, ymin-3, xmax+4, ymax+3 )
        self.failedtiles = []
        self.srtmtileszip=[]
        self.srtmtileshgt=[]
        

    def find_srtm_hgt_name(self, llx:float, lly:float , urx:float, ury:float ) -> List[str]:
        
        tilesname=[]
        # logger.debug("Get SRTM files for LowerLeft="+pointLL.ExportToWkt()+";UpperRight="+pointUR.ExportToWkt())
        
        for o in range(math.ceil(urx) - math.floor(llx)):
            for a in range(math.ceil(ury) - math.floor(lly)):
                lat =  math.floor(lly) + a
                lon =  math.floor(llx) + o
                if lat >= 0 :
                    hem = 'N'
                else : 
                    hem = 'S'
                    lat = abs(lat)
                if lon >= 0 :
                    grw = 'E'
                else : 
                    grw = 'W'
                    lon = abs(lon)
                tilesname.append(hem+f'{lat:02}'+grw+f'{lon:003}')
        
        return tilesname 

    def process(self)->List[str]:
        logger.info("Checking SRTM tiles: "+str(self.tileslist))
        
        if len(self.tileslist) == 0:
            raise Exception("Tiles name list is empty")
        
        
        self.srtmtileszip=[]
        for tile in self.tileslist :
            hgttile = os.path.join(self.outdir,tile+self.srtmarch)
            if os.path.exists(hgttile):
                logger.info(f"SRTM tile {tile} already exist. passing.")
                self.srtmtileshgt.append(hgttile)
                continue

            if tile in self.failedtiles :
                continue

            tilezip = self.download(tile)

            if tilezip == None:
                continue

            unzip = self.unzip_and_replace(tile,tilezip,hgttile)
            
            if unzip == None:
                continue

            self.srtmtileshgt.append(unzip)

        return self.srtmtileshgt


    def download(self, tilename:str)->str:
        logger.info("Downloading SRTM tile : "+tilename)
        durl = self.URLSRTM+tilename+self.srtmsuf
        
        cmd=["wget","-P", self.outdir,durl]
        srtmzip = os.path.join(self.outdir,tilename+self.srtmsuf)
        srtminzip = os.path.join(srtmzip,tilename+self.srtmarch)
        
        try:
            utils.process_command(cmd)
            if not os.path.exists(srtmzip):
                logger.warning("Failed to download. Not using: "+tilename)
                self.failedtiles.append(tilename)
                return None
            
        except Exception as err :
            logger.warning("Failed to download. Not using: "+tilename)
            self.failedtiles.append(tilename)
            return None

        return srtmzip

    def unzip_and_replace(self, tilename:str, srtmzip:str, replacesrtm:str)->str:
        logger.info("Unzip SRTM tile : "+srtmzip)
        srtmunzip = os.path.join(srtmzip[:-4],tilename+self.srtmarch)
        logger.debug(srtmunzip)
        if not os.path.exists(replacesrtm):
            try:
                utils.unzipfile(srtmzip, self.outdir)
            except Exception as warn:
                logger.warning(f"Unzip SRTM tile failed : {srtmzip}")
                logger.warning(str(warn))
                logger.warning("Deleting...")
                self.failedtiles.append(tilename)
                if os.path.exists(srtmzip):
                    os.remove(srtmzip)
                return None
        
        if not os.path.exists(replacesrtm):
            logger.warning(f"Unzip SRTM tile not found : {replacesrtm}")
            self.failedtiles.append(tilename)
            if os.path.exists(srtmzip):
                logger.debug(f"Deleting zip : {srtmzip}")
                os.remove(srtmzip)
            return None
        else:
            if os.path.exists(srtmzip):
                logger.debug(f"Deleting zip : {srtmzip}")
                os.remove(srtmzip)
            return replacesrtm

        #     if not os.path.exists(srtmunzip):
        #         logger.warning(f"Unzip SRTM tile not found : {srtmunzip}")
        #         if not os.path.exists(srtmzip):
        #             logger.warning("Deleting...")
        #             os.remove(srtmzip)
        #         self.failedtiles.append(tilename)
        #         return None

        # logger.info("Move SRTM tile : "+replacesrtm)
        # try:
        #     shutil.move(srtmunzip, replacesrtm)
        # except Exception as warn:
        #     logger.warning(f"Moving SRTM tile failed : {srtmunzip}")
        #     logger.warning("Deleting...")
        #     self.failedtiles.append(tilename)
        #     if os.path.exists(os.path.dirname(srtmunzip)):
        #         shutil.rmtree(os.path.dirname(srtmunzip))
        #     if os.path.exists(replacesrtm):
        #         os.remove(replacesrtm)
        #     return None
        
        # if os.path.exists(os.path.dirname(srtmunzip)):
        #     shutil.rmtree(os.path.dirname(srtmunzip))

        

    def get_downloaded_listfile(self)->List[str]:
        return self.srtmtileshgt

    def __str__(self) -> str:
        tiles=[]
        for tile in self.srtmtileshgt:
            tiles.append(os.path.basename(tile))
        return str(tiles)
