
import os, datetime, time, math, glob, shutil
from typing import List
import otbApplication
import rasterio
from osgeo import gdal, osr, ogr
from django.contrib.gis.gdal import CoordTransform, SpatialReference
from django.contrib.gis.gdal.datasource import DataSource
from theiacesprodapp import models as mod
from theiacesprodapp.backend import utils
from theiacesprodapp.backend.utils import search_files, process_command
import logging 
from theiacesprodapp.backend.data import Sentinel2Zip, Raster, Sentinel2PCStac
from theiacesprodapp.backend.service.download import Esa_Srtm

logger = logging.getLogger(__name__)


                    
    
class ndvi_pipeline():
    
    
    def __init__(self,params:dict, s2_file_list=None):
        self.s2_file_list = None
        self.parametres = {
            's2l2zipdir':"",
            # 'format':"S2-L3A", #['S2-MUSCATE','S2-SEN2COR','S2-L3A','L8-OLI/TIRS','S2-PLANETCOMP']
            'ndvidir':"",
            'mrgstile':"",
            'overwrite':False
            }
    
        for key in self.parametres:
            self.parametres[key] = params[key]

        if None != s2_file_list :
            self.s2_file_list = s2_file_list

        if not os.path.exists(self.parametres['ndvidir']):
            try:
                os.makedirs(self.parametres['ndvidir'])
            except Exception as err:
                raise

    
    def process(self):
        ndviprod = []
        if None != self.s2_file_list :
            zipfiles = self.s2_file_list
        else:
            zipfiles = glob.glob(os.path.join(self.parametres["s2l2zipdir"],"*.zip"))
            
        if len(zipfiles) == 0:
            logger.error(f"Unable to find *.zip in {self.parametres['s2l2zipdir']}")
            raise Exception(f"Unable to find *.zip in {self.parametres['s2l2zipdir']}")
        
        
        for zf in zipfiles:
            
            s2zip = Sentinel2Zip(zf) #, self.parametres["format"]
            
            outfile = "NDVI_"+self.parametres['mrgstile']+"_"+s2zip.date+".TIF"
    
            absoutfile = os.path.join(self.parametres['ndvidir'], outfile)
            
            logger.info("### Creating NDVI ###")
            logger.info("using red = " + s2zip.bandredvsi)
            logger.info("using nir = " + s2zip.bandnirvsi)
            logger.info("using mask = " + s2zip.bandmaskvsi)
            logger.info("using out = " + absoutfile)
            
            goodvalue = Sentinel2Zip.searchfileparam[s2zip.fileformat]["mask"]["goodvalue"]
            
            if not os.path.exists(absoutfile) or self.parametres["overwrite"]:
                try:
                    ndvi_pipeline.ndvi_pipeline( goodvalue, s2zip.bandnirvsi, s2zip.bandredvsi, s2zip.bandmaskvsi, absoutfile)
                except Exception as err:
                    logger.error("ndvi_pipeline failed :"+err)
                    raise
            ndviprod.append(Raster(absoutfile))
            logger.info("### NDVI done ###")
        
        return ndviprod

    def process_pcstac(self):
        tmpndviprod:List[str] = []
        ndviprod:List[Raster] = []
        s2pcstac_list:List[Sentinel2PCStac]=[]
        if None != self.s2_file_list :
            s2pcstac_list = self.s2_file_list
        else:
            s2dir_list = glob.glob(os.path.join(self.parametres["s2l2zipdir"],"*_MSIL2A_*"))
            for s2dir in s2dir_list:
                s2pcstac_list.append(Sentinel2PCStac(s2dir))

        if len(s2pcstac_list) == 0:
            logger.error(f"s2pcstac_list is empty")
            raise Exception(f"s2pcstac_list is empty")
        
        if self.parametres['mrgstile'] == "aoi":
            self.parametres['mrgstile'] = os.path.basename(
                                        os.path.dirname(
                                        os.path.dirname(
                                        os.path.dirname(
                                            s2pcstac_list[0].bandred
                                        )))).split("_")[0]
        
        #get srs 
        s2r = Raster(s2pcstac_list[0].get_bandred())
        epsgref = s2r.srs.srid
        self.check_s2_proj(s2pcstac_list,epsgref)

        s2bydatedict={}
        for prod in s2pcstac_list:
            s2date = prod.get_date().split("T")[0]
            s2bydatedict.setdefault(s2date, []).append(prod)
        

        for mosadate in s2bydatedict:
            #mrgstile = nom genere par coord centroid AOI
            outfile = "NDVI_"+self.parametres['mrgstile']+"_"+s2bydatedict[mosadate][0].get_date()+".TIF"
    
            absoutfile = os.path.join(self.parametres['ndvidir'], outfile)

            logger.info(f"### Creating NDVI for date {mosadate} ###")
            logger.info(f"Number of images to process : {str(len(s2bydatedict[mosadate]))}")
            for prod in s2bydatedict[mosadate]:
                logger.info(f"using products = " + str(prod))
            
            logger.info("using out = " + absoutfile)
            
            if not os.path.exists(absoutfile) or self.parametres["overwrite"]:
                try:
                    ndvi_pipeline.s2pcstac_ndvi(s2bydatedict[mosadate], absoutfile)
                except Exception as err:
                    logger.error("ndvi_pipeline failed :"+err)
                    raise
            tmpndviprod.append(absoutfile)
        
        logger.info(f"### Superimpose all NDVI on largest ###")
        largestindex = ndvi_pipeline.get_largest_raster_index(tmpndviprod)
        refndvi = tmpndviprod[largestindex]
        logger.info(f"Using NDVI reference : {refndvi}")
        for ndvi in tmpndviprod:
            out = ndvi[:-4]+"_SUP.TIF"
            if ndvi == refndvi :
                continue

            app0 = otbApplication.Registry.CreateApplication("Superimpose")
            app0.SetParameterString("inm",ndvi)
            app0.SetParameterString("inr",refndvi)
            app0.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
            app0.SetParameterString("out", out+"?&gdal:co:COMPRESS=DEFLATE&nodata=0")#
            app0.SetParameterString("interpolator", "nn")
        #    app0.SetParameterInt("ram",4000)
            app0.ExecuteAndWriteOutput()
        
        app0 = None

        for ndvi in tmpndviprod:
            nb_delay=0
            out = ndvi[:-4]+"_SUP.TIF"
            if ndvi == refndvi :
                ndviprod.append(Raster(refndvi))
            else:
                shutil.rmtree(ndvi,ignore_errors=True)
                if not os.path.exists(out):
                    logger.info(f"File {ndvi} does not exists")
                    while not os.path.exists(out) or nb_delay==30:
                        logger.info(f"Waiting {ndvi} 1 sec")
                        time.sleep(1)
                        nb_delay+=1
                if not os.path.exists(out) and nb_delay==30:
                    raise FileNotFoundError(f"30 seconds delay for file creation reach : {out}")
                
                shutil.move(out, ndvi)
                ndviprod.append(Raster(ndvi))

        
        logger.info("### NDVI done ###")
        
        return ndviprod

    def check_s2_proj(self, pcstac_list:List[Sentinel2PCStac], epsgref:int):
        logging.info(f"##Checking projection##")
        for s2stac in pcstac_list:
            s2r = Raster(s2stac.get_bandred())
            if s2r.srs.srid == epsgref :
                logging.info(f"Projection Ok")
                continue
            
            logging.info(f"Reprojection needed")
            s2r = None
            for b in s2stac.bands:
                s2r = Raster(s2stac.get_band(b))
                s2r.reprojectRaster(epsgref)

    @classmethod
    def get_largest_raster_index(cls, ndvifilelist: List[str]) -> int :
            largest_index = 0
            widthref = 0
            heightref = 0
            surfref = 0
            for i in range(len(ndvifilelist)) :
                dataset = rasterio.open(ndvifilelist[i])
                if i == 0 :
                    widthref = dataset.width
                    heightref = dataset.height
                    surfref = widthref * heightref
                else:
                    if dataset.width * dataset.height > surfref :
                        widthref = dataset.width
                        heightref = dataset.height
                        surfref = widthref * heightref
                        largest_index = i
            
            return largest_index


    @classmethod
    def ndvi_pipeline(cls, valuemask, nir, red, mask, out):
        
        app0 = otbApplication.Registry.CreateApplication("Superimpose")
        app0.SetParameterString("inm",mask)
        app0.SetParameterString("inr",nir)
        app0.SetParameterString("out", "temp0.tif")
        app0.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
        app0.SetParameterString("interpolator", "nn")
    #    app0.SetParameterInt("ram",4000)
        app0.Execute()
        
        app1 = otbApplication.Registry.CreateApplication("BandMath")
        # Define Input im2: Band Red (B4)
    #    app1.AddParameterStringList("il",[nir,red])
        app1.AddParameterStringList("il",nir)
        app1.AddParameterStringList("il",red)
        app1.SetParameterString("out", "temp1.tif")
        app1.SetParameterString("exp", "(im1b1-im2b1)/(im1b1+im2b1)>0?(im1b1-im2b1)/(im1b1+im2b1)*100:0")
    #    app1.SetParameterInt("ram",4000)
        app1.Execute()
        
            
        
        app2 = otbApplication.Registry.CreateApplication("BandMath")
        app2.AddImageToParameterInputImageList("il",app1.GetParameterOutputImage("out"))
        # Define Input im2: Band Red (B4)
        app2.AddImageToParameterInputImageList("il", app0.GetParameterOutputImage("out"))
        app2.SetParameterString("out", out+"?&gdal:co:COMPRESS=DEFLATE&nodata=0")#
        app2.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
        app2.SetParameterString("exp", "(im2b1=="+valuemask+")?im1b1:0")
    #    app2.SetParameterInt("ram",4000)
        app2.ExecuteAndWriteOutput()
 

    @classmethod
    def s2pcstac_ndvi(cls, s2mosalist:List[Sentinel2PCStac], out:str):
        
        valuemask = "im3b1 != 10 && im3b1 != 9 && im3b1 != 8" # && im3b1 != 7" class 7 remove clouds & fields

        appdict={"Superimpose":[],"BandMath":[],
                 "Mosaic":None,"ManageNoData":None
                 }
        i = 0
        for s2 in s2mosalist :
            appdict["Superimpose"].append(otbApplication.Registry.CreateApplication("Superimpose"))
            appdict["Superimpose"][i].SetParameterString("inm",s2.get_mask())
            appdict["Superimpose"][i].SetParameterString("inr",s2.get_bandred())
            appdict["Superimpose"][i].SetParameterString("out","temp0.tif")
            appdict["Superimpose"][i].SetParameterString("interpolator","nn")
            appdict["Superimpose"][i].Execute()

            appdict["BandMath"].append(otbApplication.Registry.CreateApplication("BandMath"))
            appdict["BandMath"][i].AddParameterStringList("il",s2.get_bandnir())
            appdict["BandMath"][i].AddParameterStringList("il",s2.get_bandred())
            appdict["BandMath"][i].AddImageToParameterInputImageList("il",appdict["Superimpose"][i].GetParameterOutputImage("out"))
            appdict["BandMath"][i].SetParameterString("out","temp1.tif")
            #Verifier la condition sur la valeur du mask
            appdict["BandMath"][i].SetParameterString("exp","(im1b1-im2b1)/(im1b1+im2b1)>0 && "+valuemask+" ?(im1b1-im2b1)/(im1b1+im2b1)*100:0")
            appdict["BandMath"][i].Execute()

            i+=1
        
        if len(s2mosalist) > 1:
                
            appdict["Mosaic"] = otbApplication.Registry.CreateApplication("Mosaic")
            for j in range(len(s2mosalist)):
                appdict["Mosaic"].AddImageToParameterInputImageList("il", appdict["BandMath"][j].GetParameterOutputImage("out"))
                logger.debug("Mosaic add image "+str(j))
            appdict["Mosaic"].SetParameterString("out",  "mosatemp.tif")
            appdict["Mosaic"].Execute()

        appdict["ManageNoData"] = otbApplication.Registry.CreateApplication("ManageNoData")
        if len(s2mosalist) > 1:
            appdict["ManageNoData"].SetParameterInputImage("in", appdict["Mosaic"].GetParameterOutputImage("out"))
        else:
            appdict["ManageNoData"].SetParameterInputImage("in", appdict["BandMath"][0].GetParameterOutputImage("out"))
        appdict["ManageNoData"].OUT = out+"?&gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES"  #SetParameterString("out", outfile+"?&writegeom=true&gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")
        appdict["ManageNoData"].SetParameterString("mode", "changevalue")
        appdict["ManageNoData"].SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)

        appdict["ManageNoData"].ExecuteAndWriteOutput()
        
       
    

class gapfilling_pipeline:
    
    
    def __init__(self,params:dict):
        self.parametres = {
            'ndvidir':"",
            'gapfmaskdir':"",
            'interpolation':"linear",
            'gapfndvidir':"",
            'overwrite':False
            }
        
        for key in self.parametres:
            self.parametres[key] = params[key]
    
    def process(self):
        gapfprod = []
        def create_mask(ndvi,mask):
    
            app1 = otbApplication.Registry.CreateApplication("BandMath")
            app1.AddParameterStringList("il",ndvi)
            app1.SetParameterString("out", mask)
            app1.SetParameterString("exp", "im1b1==0?1:0")
            app1.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
            app1.ExecuteAndWriteOutput()

        def get_largest_raster_index(ndvifilelist) -> int :
            largest_index = 0
            widthref = 0
            heightref = 0
            surfref = 0
            for i in range(len(ndvifilelist)) :
                dataset = rasterio.open(ndvifilelist[i])
                if i == 0 :
                    widthref = dataset.width
                    heightref = dataset.height
                    surfref = widthref * heightref
                else:
                    if dataset.width * dataset.height > surfref :
                        widthref = dataset.width
                        heightref = dataset.height
                        surfref = widthref * heightref
                        largest_index = i
            
            return largest_index

            
        if not os.path.exists(self.parametres["gapfndvidir"]):
            os.makedirs(self.parametres["gapfndvidir"])
        if not os.path.exists(self.parametres["gapfmaskdir"]):
            os.makedirs(self.parametres["gapfmaskdir"])
            
        ndvifiles=search_files(self.parametres["ndvidir"], resolution='NDVI', extension='tif')
        
        parameters=[]
        for ndvi in ndvifiles: 
            maskname = os.path.join(self.parametres["gapfmaskdir"],os.path.basename(ndvi)[:-4]+"_MASK.TIF")
            parameters.append([os.path.basename(ndvi).split("_")[2].split("T")[0],ndvi,maskname])
            
        parameters.sort(key=lambda tup: tup[0])
        
        listinput_date = [row[0] for row in parameters]
        listinput_img = [row[1] for row in parameters]
        listinput_mask = [row[2] for row in parameters]
        
        listoutput_date = gapfilling_pipeline.create_outputdates(listinput_date)
        logger.info("Writting input and output dates in text file")
        inputdatesfile = os.path.join(self.parametres["gapfndvidir"],"InputDates.txt")
        outputdatesfile = os.path.join(self.parametres["gapfndvidir"],"OutputDates.txt")
        with open(inputdatesfile,'w') as indatefile:
            for text in listinput_date:
                indatefile.write(text+"\n")
                
        with open(outputdatesfile,'w') as outdatefile:
            for text in listoutput_date:
                outdatefile.write(text+"\n")
        
        prefsp = os.path.basename(ndvifiles[0]).split('_')
        outprefix = os.path.join(self.parametres["gapfndvidir"],"NDVIGAPF_"+prefsp[1])
        nb=0
        for gapdate in listoutput_date:
            outfile = outprefix+"_"+gapdate+".TIF"
            if os.path.exists(outfile):
                nb+=1
        if self.parametres["overwrite"] or nb < len(listoutput_date) :
            
            logger.info("Creating MASK files from input NDVIs")
            for param in parameters:
                if not os.path.exists(param[2]) or self.parametres["overwrite"]:
                    create_mask(param[1],param[2])
                
            # Concatenate
            logger.info("Concatenate NDVI files : ")
            [logger.info("    "+nm) for nm in listinput_img]
            #appConcatImg = concate_images(listinput_img, otbApplication.ImagePixelType_uint8, "tempimg.tif")
            
            refindex = get_largest_raster_index(listinput_img)

            apps=[]
            refndvi = listinput_img[refindex]
            for img in listinput_img:
                if img == refindex:
                    apps.append("REF")
                else:
                    app0 = otbApplication.Registry.CreateApplication("Superimpose")
                    app0.SetParameterString("inm", img)
                    app0.SetParameterString("out", "supimp.tif")
                    app0.SetParameterString("inr", refndvi)
                    app0.SetParameterString("interpolator", "nn")
                    app0.Execute()

                    apps.append(app0)
            
            appConcatImg = otbApplication.Registry.CreateApplication("ConcatenateImages")

            for a in apps:
                if a == "REF":
                    appConcatImg.AddParameterStringList("il",refndvi)
                else:
                    appConcatImg.AddImageToParameterInputImageList("il",a.GetParameterOutputImage("out"))

        #    appConcatImg.SetParameterStringList("il", listinput_img)
            appConcatImg.SetParameterString("out", "tempimg.tif")
            appConcatImg.Execute()
            
            logger.info("Concatenate MASK files : ")
            [logger.info("    "+nm) for nm in listinput_mask]
            #appConcatMask = concate_images(listinput_mask, otbApplication.ImagePixelType_uint8, "tempmask.tif")
            appsm=[]
            refmask = listinput_mask[refindex]
            for img in listinput_mask:
                if img == refmask:
                    appsm.append("REF")
                else:
                    app0 = otbApplication.Registry.CreateApplication("Superimpose")
                    app0.SetParameterString("inm", img)
                    app0.SetParameterString("out", "supimp.tif")
                    app0.SetParameterString("inr", refmask)
                    app0.SetParameterString("interpolator", "nn")
                    app0.Execute()
                    appsm.append(app0)
                
            appConcatMask = otbApplication.Registry.CreateApplication("ConcatenateImages")
            for a in appsm: 
                if a == "REF":
                    appConcatMask.AddParameterStringList("il",refmask)
                else:
                    appConcatMask.AddImageToParameterInputImageList("il",a.GetParameterOutputImage("out"))
        #    appConcatMask.SetParameterStringList("il", listinput_mask)
            appConcatMask.SetParameterString("out", "tempmask.tif")
            appConcatMask.Execute()
            
            
            # Gapfill
            logger.info("Gapfilled NDVIs from input dates in "+inputdatesfile+" : ")
            [logger.info("    "+nm) for nm in listinput_date]
            logger.info("Gapfilled NDVIs to output dates in "+outputdatesfile+" : ")
            [logger.info("    "+nm) for nm in listoutput_date]
            appGapFill = otbApplication.Registry.CreateApplication("ImageTimeSeriesGapFilling")
            appGapFill.SetParameterInputImage("in", appConcatImg.GetParameterOutputImage("out"))
            appGapFill.SetParameterInputImage("mask", appConcatMask.GetParameterOutputImage("out"))
            appGapFill.SetParameterInt("comp", 1)
            appGapFill.SetParameterString("it", self.parametres["interpolation"])
            appGapFill.SetParameterString("id", inputdatesfile)
            appGapFill.SetParameterString("od", outputdatesfile)
            appGapFill.SetParameterString("out", "tempgapfill.tif")
            appGapFill.Execute()
    
            # Splitimage
            logger.info("Spliting gapfilled image to monoband images")
            SplitImage = otbApplication.Registry.CreateApplication("SplitImage")
            SplitImage.SetParameterInputImage("in", appGapFill.GetParameterOutputImage("out"))
            SplitImage.SetParameterString("out", outprefix+".TIF")
            SplitImage.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
            SplitImage.ExecuteAndWriteOutput()
            
            # Rename image
            logger.info("Renaming files with dates")
            i=0
            for gapdate in listoutput_date:
                os.rename(outprefix+"_"+str(i)+".TIF",outprefix+"_"+gapdate+".TIF")
                i+=1
        
        for gapdate in listoutput_date:
            gapfprod.append(Raster(outprefix+"_"+gapdate+".TIF"))
            
        logger.info("Done")
        return gapfprod
        
    
        

        
        
    @classmethod            
    def create_outputdates(cls, listinput_date):
        
        def monthrange(start_date, end_date):
        #    logger.info("MonthRange: "+str (math.floor((end_date - start_date).days/31)))
        #    for n in range(int (math.floor((end_date - start_date).days/31))+1):
        #        yield start_date + datetime.timedelta(days=n*31)
            
            qannee = end_date.year - start_date.year
            
            dmois = end_date.month +12*(qannee) - start_date.month
            
            yield start_date
            
            
            anneesuiv = start_date.year
            for m in range(dmois-1):
                m+=1
                moissuiv = (start_date.month + m) % 12
                if moissuiv == 0 :
                    moissuiv = 12
                if moissuiv == 1 :
                    anneesuiv = anneesuiv+1
                
                yield datetime.date(anneesuiv,moissuiv,1)
            
            yield end_date
    #    stdate = datetime.date(int(startdate[:4]),int(startdate[4:6]),1)
    #    eddate = datetime.date(int(enddate[:4]),int(enddate[4:6]),15)
        inst = datetime.date(int(listinput_date[0][:4]),int(listinput_date[0][4:6]),int(listinput_date[0][6:8]))
        ined = datetime.date(int(listinput_date[-1][:4]),int(listinput_date[-1][4:6]),int(listinput_date[-1][6:8]))
        
        if inst.day > 15:
            if inst.month == 12 :
                stdate = datetime.date(inst.year+1,1,1)
            else:
                stdate = datetime.date(inst.year,inst.month +1,1)
        else:
            stdate = datetime.date(inst.year,inst.month,15)
        if ined.day >= 15:
            eddate = datetime.date(ined.year,ined.month,15)
        else:
            eddate = datetime.date(ined.year,ined.month,1)
            
        
        exformat = "%Y%m%d"
        logger.info("Start date: "+stdate.strftime(exformat))
        logger.info("End date: "+eddate.strftime(exformat))
        
        outputdates=[]
        #outputdates.append(stdate.strftime(exformat))
        procd = stdate
        for dstep in monthrange(stdate, eddate):
            procd = dstep
            if dstep == stdate:
                if stdate.day == 1:
                    outputdates.append(procd.strftime(exformat))
                    procd = datetime.date(dstep.year,dstep.month,15)
                    outputdates.append(procd.strftime(exformat))
                else:
                    outputdates.append(procd.strftime(exformat))
            elif dstep == eddate:
                if eddate.day == 1:
                    outputdates.append(procd.strftime(exformat))
                else:
                    procd = datetime.date(dstep.year,dstep.month,1)
                    outputdates.append(procd.strftime(exformat))
                    procd = datetime.date(dstep.year,dstep.month,15)
                    outputdates.append(procd.strftime(exformat))
            else:
                procd = datetime.date(dstep.year,dstep.month,1)
                outputdates.append(procd.strftime(exformat))
                procd = datetime.date(dstep.year,dstep.month,15)
                outputdates.append(procd.strftime(exformat))
            
        return outputdates
        
        
        ##################################"
        #     MAIN
        ##################################
        
        
        
        
class stack_pipeline():
    
    def __init__(self,params:dict):
        self.parametres = {
            "ndvidir":"",
            "lulc":"",
            "agrivalues":"",
            "output":""
            }
    
        for key in self.parametres:
            self.parametres[key] = params[key]
            
    def process(self):
        
        
        def ndvistackandmask_pipeline(ndvis,lulc,agrivalues, out):
    
            app0 = otbApplication.Registry.CreateApplication("ConcatenateImages")
                
            app0.SetParameterStringList("il",ndvis)    
            app0.SetParameterString("out", "temp0.tif")
        #    app0.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
        #    app0.SetParameterInt("ram",4000)
            app0.Execute()
            
            
            # The following line creates an instance of the Superimpose application
            app4 = otbApplication.Registry.CreateApplication("SoilMoistureXBandLULCMask")
            
            # The following lines set all the application parameters:
            app4.SetParameterInputImage("inr", app0.GetParameterOutputImage("out"))
            app4.SetParameterString("inm", lulc)
            if agrivalues != None:
        #        params = {"labels":labels, "lulc":lulc, "agrivalues":agrivalues.split(" "), "out":maskedlabels}
                app4.SetParameterStringList("agrival", agrivalues.split(" "))
            app4.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
            app4.SetParameterString("out", out+"?&gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")
        #    app4.SetParameters(params)
            
            logger.info("Masking NDVI stack")
            # The following line execute the applicationw
            app4.ExecuteAndWriteOutput()
            logger.info("End of Masking \n")
        
        ##################################"
        #     MAIN
        ##################################
        
            
        if not os.path.isdir(self.parametres["ndvidir"]):
            logger.info("Error : "+self.parametres["ndvidir"]+" is not a folder")
            raise Exception("Error : "+self.parametres["ndvidir"]+" is not a folder")
        
        ndvis=[]
        ndvis=search_files(self.parametres["ndvidir"])
       
        lulc = self.parametres["lulc"]
        agrivalues = self.parametres["agrivalues"]
        output = self.parametres["output"]
        logger.info("Starting stack and mask pipeline")
        ndvistackandmask_pipeline(ndvis,lulc,agrivalues, output)


class slope_pipeline():
    def __init__(self,params:dict):
        self.parametres = {
                "srtmhgtzip":"",
                "reference_image_file":"",
                "mrgstile":"",
                "outdir":""
                }
        
        for key in self.parametres:
            self.parametres[key] = params[key]
    
    def process(self, args):
 
        ##################################"
        #     MAIN
        ##################################
        #get tiles
        refimg = Raster(self.parametres["reference_image_file"])
        esa_proc = Esa_Srtm(refimg, self.parametres["srtmhgtzip"])
        srtmlist = esa_proc.process()
        
        logger.info("Using tile files: ")
        [logger.info(tilefile) for tilefile in srtmlist]
        outdem = os.path.join(self.parametres["outdir"],"SRTM_"+self.parametres["mrgstile"]+".TIF")
        mosapp = otbApplication.Registry.CreateApplication("Mosaic")
        mosparams = {"il":srtmlist, "out":"mosatemp.tif"}
        mosapp.SetParameters(mosparams)
        mosapp.Execute()
        
        supapp = otbApplication.Registry.CreateApplication("Superimpose")
        supapp.SetParameterString("inr", self.parametres["reference_image_file"])
        supapp.SetParameterInputImage("inm", mosapp.GetParameterOutputImage("out"))
        supapp.SetParameterString("out", outdem)
        supapp.ExecuteAndWriteOutput()
                
        #Compute slope
        logger.info("Generating slope file...")
        outslope = os.path.join(self.parametres["outdir"],"SLOPE_"+self.parametres["mrgstile"]+".TIF")
        cmd='gdaldem slope '+outdem+' '+outslope
        logger.info("command : "+cmd)
        process_command(cmd)
        logger.info("Done.")
        

class gpm_pipeline():
    
    def __init__(self,params:dict):
        self.parametres = {
                "s1_ortho_dir":"",
                "GPM_DIR":"",
                "mrgstile":"",
                "modelchoice_outdir":""
                }
        
        for key in self.parametres:
            self.parametres[key] = params[key]
    
    
    def process(self):
        
        def get_meteodates_from_sardate(sardate):
            exformat = "%Y%m%d"
            saryear,sarmonth,sarday = sardate[:4],sardate[4:6],sardate[6:8]
            fsardate = datetime.date(int(saryear),int(sarmonth),int(sarday))
            oneday = datetime.timedelta(days=1)
            sardatem1 = (fsardate - oneday).strftime(exformat)
            sardatem2 = (fsardate - oneday - oneday).strftime(exformat)
            return [sardate,sardatem1,sardatem2]
        
        def download_gpm(sarlist, gpmdir):
            
            if not os.path.exists(gpmdir):
                os.mkdir(gpmdir)
                
            cmd = "wget --user=loic.lozach@irstea.fr --password=loic.lozach@irstea.fr \
                    ftp://arthurhou.pps.eosdis.nasa.gov/gpmdata/"
            arg0 = "wget"
            arg1 = "--user=loic.lozach@irstea.fr"
            arg2 = "--password=loic.lozach@irstea.fr"
            arg3 = "ftp://arthurhou.pps.eosdis.nasa.gov/gpmdata/"
            
            for sar in sarlist:
                sardata = os.path.basename(sar)
                sarsplit = sardata.split('_')
                sardate = sarsplit[4][:8]
                sar3dates = get_meteodates_from_sardate(sardate)
                
                tifexits = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[0]+"-S000000-E235959*.tif"))
                if len(tifexits) >= 1 :
                    continue
                
                tifexitsm1 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[1]+"-S000000-E235959*.tif"))
                if len(tifexitsm1) >= 1 :
                    continue
                
                tifexitsm2 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[2]+"-S000000-E235959*.tif"))
                if len(tifexitsm2) >= 1 :
                    continue
                
                
                gpmtif = sar3dates[0][:4] +"/"+ sar3dates[0][4:6] +"/"+ sar3dates[0][6:8] +"/gis/"+ "3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[0]+"-S000000-E235959*.t*"
                gpmtifm1 = sar3dates[1][:4] +"/"+ sar3dates[1][4:6] +"/"+ sar3dates[1][6:8] +"/gis/"+  "3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[1]+"-S000000-E235959*.t*"
                gpmtifm2 = sar3dates[2][:4] +"/"+ sar3dates[2][4:6] +"/"+ sar3dates[2][6:8] +"/gis/"+  "3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[2]+"-S000000-E235959*.t*"
                cmdlist=[]
                cmdlist.append([arg0,arg1,arg2,arg3 + gpmtif])
                cmdlist.append([arg0,arg1,arg2,arg3 + gpmtifm1])
                cmdlist.append([arg0,arg1,arg2,arg3 + gpmtifm2])
                
                for c in cmdlist:
                    process_command(c)
                        
#         def process_gpm(sar, sar3dates, gpmdir):
#             outdir = os.path.join( os.path.dirname(gpmdir),"MaskWetOrDry")
#             outfile = os.path.join(outdir,"MaskWetOrDry"+args.mrgstile+"_"+sar3dates[0]+".tif")
#             if os.path.exists(outfile):
#                 logger.warn("Already exists : "+outfile)
#                 logger.warn("Passing...")
#                 return
#             if not os.path.exists(outdir):
#                 os.mkdir(outdir)
#                 
#             gpmfiles1 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[0]+"*.tif"))
#             gpmfiles2 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[1]+"*.tif"))
#             gpmfiles3 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[2]+"*.tif"))
#             
#             if  len(gpmfiles1) == 0 :
#                 logger.error("Error: Can't find gpm file "+"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[0]+"*.tif")
#                 raise Exception("Error: Can't find gpm file "+"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[0]+"*.tif")
#             if  len(gpmfiles2) == 0 :
#                 logger.error("Error: Can't find gpm file "+"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[1]+"*.tif")
#                 raise Exception("Error: Can't find gpm file "+"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[1]+"*.tif")
#             if  len(gpmfiles3) == 0 :
#                 logger.error("Error: Can't find gpm file "+"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[2]+"*.tif")
#                 raise Exception("Error: Can't find gpm file "+"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[2]+"*.tif")
#             
#             gpmfiles=[gpmfiles1[0],gpmfiles2[0],gpmfiles3[0]]
#             apps=[]
#             for f in gpmfiles :
#                 
#                 app = otbApplication.Registry.CreateApplication("Superimpose")
#                 
#                 app.SetParameterString("inm", f)
#                 app.SetParameterString("inr", sar)
#                 app.SetParameterString("interpolator","bco")
#                 app.SetParameterString("out", "temp2.tif")
#                 
#                 app.Execute() #ExecuteAndWriteOutput() 
#                 apps.append(app)
#             
#             app0 = otbApplication.Registry.CreateApplication("BandMath")
#             app0.AddImageToParameterInputImageList("il", apps[0].GetParameterOutputImage("out"))
#             app0.AddImageToParameterInputImageList("il", apps[1].GetParameterOutputImage("out"))
#             app0.AddImageToParameterInputImageList("il", apps[2].GetParameterOutputImage("out"))
#             app0.SetParameterString("out", outfile)
#             app0.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
#             app0.SetParameterString("exp", "im1b1+im2b1+im3b1")
#             app0.ExecuteAndWriteOutput()
#             
            
        def process_gpm_noresampling(sar, sar3dates, gpmdir, zone):
            outdir = os.path.join( gpmdir,"CumulPluie")
            outfile = os.path.join(outdir,"CumulPluie_"+zone+"_"+sar3dates[0]+".tif")
            if os.path.exists(outfile):
                logger.info("Already exists : "+outfile)
                return
            
            if not os.path.exists(outdir):
                os.mkdir(outdir)
                
            gpmfiles1 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[0]+"*.tif"))
            gpmfiles2 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[1]+"*.tif"))
            gpmfiles3 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[2]+"*.tif"))
            
            if  len(gpmfiles1) == 0 :
                logger.info("Error: Can't find gpm file "+"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[0]+"*.tif")
                return 1
            if  len(gpmfiles2) == 0 :
                logger.info("Error: Can't find gpm file "+"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[1]+"*.tif")
                return 1
            if  len(gpmfiles3) == 0 :
                logger.info("Error: Can't find gpm file "+"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[2]+"*.tif")
                return 1
            
            gpmfiles=[gpmfiles1[0],gpmfiles2[0],gpmfiles3[0]]
            
            app0 = otbApplication.Registry.CreateApplication("BandMath")
            app0.SetParameterStringList("il",gpmfiles)
            app0.SetParameterString("out", "temp2.tif")
            app0.SetParameterString("exp", "im1b1+im2b1+im3b1")
            app0.Execute()
            
            app = otbApplication.Registry.CreateApplication("ExtractROI")    
            app.SetParameterInputImage("in", app0.GetParameterOutputImage("out"))
            app.SetParameterString("mode.fit.im", sar)
            app.SetParameterString("mode","fit")
            app.SetParameterString("out", outfile)
            app.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint16)
            app.ExecuteAndWriteOutput() 
            
            
        def select_tfmodel_from_gpm(sardate,gpmdir,zone):
            cumul = glob.glob(os.path.join( os.path.join( os.path.dirname(gpmdir),"CumulPluie"), "CumulPluie_"+zone+"_"+sardate+".tif"))
            
            if len(cumul) != 1 :
                logger.error("Error : Can't find file "+"CumulPluie_"+zone+"_"+sardate+".tif")
                raise Exception("Error : Can't find file "+"CumulPluie_"+zone+"_"+sardate+".tif")
                
            raster = gdal.Open(cumul[0])
            srcband = raster.GetRasterBand(1)
            stats = gdal.Band.GetStatistics(srcband,1,1)
            
            if stats[2] > 5 :
                return str(stats[2]),"wet"
            else :
                return str(stats[2]),"dry"    
         
        ##################################"
        #     MAIN
        ##################################
        sars=[]
        datepos=4
        sarsvv=search_files(self.parametres["s1_ortho_dir"], resolution="S1", extension="tif", band="VV")
        
        sarsvh=search_files(self.parametres["s1_ortho_dir"], resolution="S1", extension="tif", band="VH")
        
        if len(sarsvv) == 0 and len(sarsvh) == 0:
            logger.error("Unable to find Sentinel1 Ortho files in "+self.parametres["s1_ortho_dir"])
            raise Exception("Unable to find Sentinel1 Ortho files in "+self.parametres["s1_ortho_dir"])
        elif len(sarsvv) > 0 :
            sars = sarsvv
        elif len(sarsvv) == 0 :
            sars = sarsvh
        
        download_gpm(sars, self.parametres["GPM_DIR"])
        outtxt = os.path.join(self.parametres["modelchoice_outdir"],"TF_modelchoice_"+self.parametres["zone"]+".txt")
        with open(outtxt, 'w') as affout:
            for sar in sars :
                sardata = os.path.basename(sar)
                sarsplit = sardata.split('_')
                sardatetime = sarsplit[datepos]
                sardate = sardatetime.split("T")[0]
                sar3dates = get_meteodates_from_sardate(sardate)
                
                process_gpm_noresampling(sar, sar3dates, self.parametres["GPM_DIR"], self.parametres["zone"])
                
                res=select_tfmodel_from_gpm(sardate, self.parametres["GPM_DIR"], self.parametres["zone"])
                
                affout.write(sar+";"+res[0]+";"+res[1]+"\n")
                
        #TODO clean CumulPluie dir in GPM dir

class processrgp_pipeline():
    
        
    def __init__(self,params:dict):
        self.parametres = {
                "rgpshp":"",
                "labelsfield":"ID_PARCEL",
                "landusefield":"CODE_GROUP",
                "agrivalues": "1 2 3 4 5 6 7 8 9 14 15 16 17 18 19 20 21 22 23 25 26",
                "reference_image_file":"",
                "mrgstile":"",
                "processrgpdir":"",
                "overwrite":False
                }
    
        for key in self.parametres:
            self.parametres[key] = params[key]
    
    def process(self):
        
        

        def rasterize(clip,ref,lfield, out):
        
            app1 = otbApplication.Registry.CreateApplication("Rasterization")
            app1.SetParameterString("in",clip)
            app1.SetParameterString("out", out)
            app1.SetParameterString("im", ref)
            app1.SetParameterString("mode", "attribute")
            app1.SetParameterString("mode.attribute.field", lfield)
            app1.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint32)
            app1.ExecuteAndWriteOutput()
            
        def create_mask(labels,mask):
        
            app1 = otbApplication.Registry.CreateApplication("BandMath")
            app1.AddParameterStringList("il",labels)
            app1.SetParameterString("out", mask)
            app1.SetParameterString("exp", "im1b1>0?1:0")
            app1.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
            app1.ExecuteAndWriteOutput()
        
        def agri_filtering(labels,lulc,agrivalues, maskedlabels):
        
            app4 = otbApplication.Registry.CreateApplication("SoilMoistureSegmentationFiltering")
            params = {"labels":labels, "lulc":lulc, "agrivalues":agrivalues.split(" "), "out":maskedlabels}
            app4.SetParameters(params)
            
            logger.info("Masking segmentation labels")
            # The following line execute the applicationw
            app4.ExecuteAndWriteOutput()
            logger.info("End of Masking \n")
            
        def allin(clip, ref, agrivalues, out):
            #TODO manage labelsfield and landusefield parameters
            procfields = ["ID_PARCEL","CODE_GROUP"]
            apps=[]
            i=0
#             for procf in procfields :
                
            
            appr = otbApplication.Registry.CreateApplication("Rasterization")
            appr.SetParameterString("in",clip)
            appr.SetParameterString("out", "labtemp.tif")
            appr.SetParameterString("im", ref)
            appr.SetParameterString("mode", "attribute")
            appr.SetParameterString("mode.attribute.field", "ID_PARCEL")
            appr.Execute()
            
            if agrivalues == "all":  
                app1 = otbApplication.Registry.CreateApplication("BandMath")
                app1.AddImageToParameterInputImageList("il",appr.GetParameterOutputImage("out"))
                app1.SetParameterString("out", "mastmp.tif")
                app1.SetParameterString("exp", "im1b1>0?1:0")
                app1.Execute()
                agrivalues="1"
            else:
                app1 = otbApplication.Registry.CreateApplication("Rasterization")
                app1.SetParameterString("in",clip)
                app1.SetParameterString("out", "mastmp.tif")
                app1.SetParameterString("im", ref)
                app1.SetParameterString("mode", "attribute")
                app1.SetParameterString("mode.attribute.field", "CODE_GROUP")
                app1.Execute()
            
            
            
            app4 = otbApplication.Registry.CreateApplication("SoilMoistureSegmentationFiltering")
#             params = {"labels":apps[0].GetParameterOutputImage("out"), 
#                       "lulc":apps[1].GetParameterOutputImage("out"), 
#                       "agrivalues":agrivalues.split(" "), s
#                       "out":out
#                       }
#             app4.SetParameters(params)
            app4.SetParameterInputImage("labels",appr.GetParameterOutputImage("out"))
            app4.SetParameterInputImage("lulc",app1.GetParameterOutputImage("out"))
            app4.SetParameterStringList("agrivalues",agrivalues.split(" "))
            app4.SetParameterString("out",out)
            
            logger.info("Masking segmentation labels")
            # The following line execute the applicationw
            app4.ExecuteAndWriteOutput()
            logger.info("End of Masking \n")
    
        ##################################"
        #     MAIN
        ##################################
        
        if not os.path.exists(self.parametres["processrgpdir"]):
            os.makedirs(self.parametres["processrgpdir"])
        
        logger.info("Clipping RPG shapefile...")
        ds = DataSource(self.parametres["rgpshp"])
        srs = ds[0].srs
        
        refraster = Raster(self.parametres["reference_image_file"])
        clipsrc = refraster.reproject_enveloppe_polygon(srs)
        
        outshp = os.path.join(self.parametres["processrgpdir"],"RGP_"+self.parametres["mrgstile"]+".shp")
        
        ds = None
        
        logger.info("Extract AOI from shapefile...")
        if not os.path.exists(outshp) or self.parametres["overwrite"]:
            cmd=['ogr2ogr', '-clipsrc', clipsrc.wkt, outshp, self.parametres["rgpshp"]]
            process_command(cmd)
        
#         logger.info("Rasterizing clip...")
#         outlabels = os.path.join(self.parametres["processrgpdir"],"RGP_"+args.zone+".TIF")
#         if not os.path.exists(outlabels):
#             rasterize(outshp,args.inref,args.labelsfield, outlabels)
#         
#         logger.info("Creating agricultural mask...")
#         outmask = os.path.join(args.outdir,"RGP_"+args.zone+"_MASK.TIF")
#         if not os.path.exists(outmask):
#             create_mask(outlabels,outmask)
        
        logger.info("Filtering and masking labels with agrivalues...")
        outmaskedlabels = os.path.join(self.parametres["processrgpdir"],"FILTLABELS_"+self.parametres["mrgstile"]+".TIF")
        if not os.path.exists(outmaskedlabels) or self.parametres["overwrite"]:
            allin(outshp, self.parametres["reference_image_file"], self.parametres["agrivalues"], outmaskedlabels)
        
        logger.info("Done.")
        
        return Raster(outmaskedlabels)

class processagrilu_pipeline():
    
        
    def __init__(self,params:dict):
        self.parametres = {
                "rgpshp":"",
                "labelsfield":"ID_PARCEL",
                "mrgstile":"",
                "processrgpdir":"",
                "overwrite":False
                }
    
        for key in self.parametres:
            self.parametres[key] = params[key]
    
    def process(self):
        pass



        

#TODO merge this for freeze
@classmethod
def get_envelope_from_db(cls, tilename, labelshpfile):
    
    ##TODO SQL a verifier
    cmd = ["ogr2ogr", "-f", "ESRI Shapefile", labelshpfile, "-nln", os.path.basename(labelshpfile)[:-4], "-nlt", "MULTIPOLYGON",
           "PG:dbname='postgres' host='db' port='5432' user='postgres' password='postgres'",
            "-sql", """SELECT pg.* FROM frozenapp_parcellegraphique pg, frozenapp_tileenvelope te, frozenapp_tileenvelope_parcelles pgte 
            WHERE pgte.parcellegraphique_id=pg.id AND pgte.tileenvelope_id=te.id AND te.tile="""+"'"+tilename+"'" 
            ]
    code = process_command(cmd)
    if code != 0 :
        exit()

@classmethod
def clip_RGP_from_tile(cls, rpgfile,tilename,outdir,overwrite=False):
    logger.info("Clipping RPG shapefile...")
    ds = DataSource(rpgfile)
    srs = ds[0].srs
    qtiles = mod.TileEnvelope.objects.filter(tile__eq = tilename)
    if len(qtiles) != 1 :
        logger.error(f"Database request for tile {tilename} failed.")
        raise Exception()

    qtiles[0].geom.transform(srs)
    clipsrc = qtiles[0].ewkt.split(";")[1]
    logger.debug("Clip source = "+str(clipsrc))
    outshp = os.path.join(outdir,"RGP_"+tilename+".shp")
    
    del ds
    
    logger.info("Extract AOI from shapefile...")
    if not os.path.exists(outshp) or overwrite:
        cmd=['ogr2ogr', '-clipsrc', clipsrc.wkt, outshp, rpgfile]
        utils.process_command(cmd)

    if not os.path.exists(outshp):
        logger.error(f"File not found : {outshp}")
        raise Exception()

    return outshp

@classmethod 
def create_labelimage_ref(cls, tilename, outdir):

    labelshpfile = os.path.join(outdir,"RPG_"+tilename+".shp")
    labeltifout = os.path.join(outdir,"RPG_"+tilename+".TIF")
    labelsfield="id_parcel"
    
    print("#Creating Reference"+"RPG_"+tilename+".shp")
    
    get_envelope_from_db(tilename, labelshpfile)  
    
    driver = ogr.GetDriverByName('ESRI Shapefile')
    
    dataSource = driver.Open(labelshpfile, 0)
    
    layer = dataSource.GetLayer()
    spatialRef = layer.GetSpatialRef()
    #TODO add try block
    epsgcode = int(spatialRef.GetAttrValue("AUTHORITY", 1))
    #
    shpextent = layer.GetExtent() # [xmin,xmax,ymin,ymax]
    print(shpextent)
    szx = int((shpextent[1] - shpextent[0])/10)
    szy = int((shpextent[3] - shpextent[2])/10)

    print("#Creating Reference"+"RPG_"+tilename+".TIF")
    
    app1 = otbApplication.Registry.CreateApplication("Rasterization")
    app1.SetParameterString("in", labelshpfile)
    app1.SetParameterString("out", labeltifout)
    app1.SetParameterInt("szx", szx)
    app1.SetParameterInt("szy", szy)
    app1.SetParameterInt("epsg", epsgcode)
    app1.SetParameterFloat("orx", shpextent[0])
    app1.SetParameterFloat("ory", shpextent[3])
    app1.SetParameterFloat("spx", 10.)
    app1.SetParameterFloat("spy", -10.)
    app1.SetParameterString("mode", "attribute")
    app1.SetParameterString("mode.attribute.field", labelsfield)
    app1.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint32)
    app1.ExecuteAndWriteOutput()
    
    return labeltifout

class FreezeStatsGeneration():

    def __init__(self,params:dict):
        self.parametres = {
                "tiled_rgp_img":"",
                "s1_ortho_s40_dir":"",
                "ERA5_dir":"",
                "gen_csv_dir": ""
                }
    
        for key in self.parametres:
            self.parametres[key] = params[key]


    def gen_csv_otb(self, datelist, sarfiles,erafiles, ref, outsigmacsv, outtempcsv, eratypes):
    
        print("processing.......................................................")
        
        afa = otbApplication.Registry.CreateApplication("AgriFrozenAreas")
        s1apps=[]
        eraapps=[]
        manapps=[]
        bmxapps=[]
        i=0
        for sarfile in sarfiles:
            
            s1apps.append(otbApplication.Registry.CreateApplication("Superimpose"))
            s1apps[i].SetParameterString("inm",sarfile)
            s1apps[i].SetParameterString("out", str(i)+"temp.tif")
            s1apps[i].SetParameterString("interpolator","nn")
            s1apps[i].SetParameterString("inr",ref)
            s1apps[i].Execute()
            
    #       otbcli_ManageNoData -in /data/BRETAGNE_T30UVU/CDS/ERA5-2m_T30UVU_20200121T175644.grib 
    #        -out /data/BRETAGNE_T30UVU/CDS/ERA5-2m_T30UVU_20200121T175644_no.tif 
    #        -mode changevalue -mode.changevalue.newv  283 
            manapps.append(otbApplication.Registry.CreateApplication("ManageNoData"))
            manapps[i].SetParameterString("in",erafiles[i])
            manapps[i].SetParameterString("out", str(i+100)+"temp.tif")
            manapps[i].SetParameterString("mode","changevalue")
            manapps[i].SetParameterFloat("mode.changevalue.newv",283)
            manapps[i].Execute()
            
    #       otbcli_BandMathX -il /data/BRETAGNE_T30UVU/CDS/ERA5-2m_T30UVU_20200121T175644.grib 
    #        -out /data/BRETAGNE_T30UVU/CDS/ERA5-2m_T30UVU_20200121T175644_bmx.tif 
    #        -exp "(im1b1 == 283 ? im1b1 = mean(im1b1N5x5):im1b1)"
            bmxapps.append(otbApplication.Registry.CreateApplication("BandMathX"))
            bmxapps[i].AddImageToParameterInputImageList("il",manapps[i].GetParameterOutputImage("out"))
            bmxapps[i].SetParameterString("out", str(i+200)+"temp.tif")
            if eratypes[i] == "2m":
                bmxapps[i].SetParameterString("exp","(im1b1 == 283 ? im1b1 = mean(im1b1N5x5):im1b1)")
            elif eratypes[i] == "1000hpa":
                bmxapps[i].SetParameterString("exp","(im1b1 + 273.15)")
            else:
                print("ERA5 files type not recognize. exiting")
                exit()
                
            bmxapps[i].Execute()
            
            eraapps.append(otbApplication.Registry.CreateApplication("Superimpose"))
            eraapps[i].SetParameterInputImage("inm",bmxapps[i].GetParameterOutputImage("out"))
            eraapps[i].SetParameterString("out", str(i+300)+"temp.tif")
            eraapps[i].SetParameterString("interpolator","bco")
            eraapps[i].SetParameterString("inr",ref)
            eraapps[i].Execute()
            
            afa.AddImageToParameterInputImageList("ilsigma",s1apps[i].GetParameterOutputImage("out"))
            afa.AddImageToParameterInputImageList("iltemp",eraapps[i].GetParameterOutputImage("out"))
            i+=1
        
        afa.SetParameterStringList("datelist",datelist)
        afa.SetParameterString("inlabels",ref)
        afa.SetParameterString("out.sigma",outsigmacsv) 
        afa.SetParameterString("out.temp",outtempcsv) 
        afa.ExecuteAndWriteOutput()
    
    
        

    def gen_csv(self):
        
        if not os.path.exists(self.parametres["s1_ortho_s40_dir"]):
            logger.error("Error: Directory does not exist "+self.parametres["s1_ortho_s40_dir"])
            raise
        if not os.path.exists(self.parametres["ERA5_dir"]):
            logger.error("Error: Directory does not exist "+self.parametres["ERA5_dir"])
        if not os.path.exists(self.parametres["gen_csv_dir"]):
            os.mkdir(self.parametres["gen_csv_dir"])
            
            
        s1calfiles = utils.search_files(self.parametres["s1_ortho_s40_dir"],resolution='S1', extension='TIF', fictype='f')
        erafiles = utils.search_files(self.parametres["ERA5_dir"],resolution='ERA', extension='grib', fictype='f')
        if len(erafiles) != len(s1calfiles):
            logger.error("Error: number of sentinel1 files and era5 files doesn't match")
            raise
        
        tile_str=None
        s1strdates=[]
        s1datetimes=[]
        erafilesord=[]
        eratypes=[]
        i=0
        for s1 in s1calfiles:
            strdate=os.path.basename(s1)[:-4].split("_")[4]
            s1strdates.append(strdate)
            s1datetimes.append(datetime.datetime.strptime(strdate, "%Y%m%dT%H%M%S"))
            erafound=False
            for era in erafiles:
                if strdate in era:
                    erafilesord.append(era)
                    if "-2m" in era:
                        eratypes.append("2m")
                    elif "-1000hpa" in era:
                        eratypes.append("1000hpa")
                    else:
                        logger.error("ERA5 files type not recognize for "+era+". exiting")
                        raise
                    erafound=True
                    break
            if not erafound:
                logger.error("Error: Can't find matching date for era file "+strdate)
                raise
            tile_name = os.path.basename(s1)[:-4].split("_")[3]
            if tile_str == None :
                tile_str = tile_name
            else:
                if tile_str != tile_name:
                    logger.error("Error: wrong tile_str in image filename: "+s1)
                    raise
            i+=1
        
        mindate = min(s1datetimes)
        maxdate = max(s1datetimes)
        out_sigma40_csv = os.path.join(self.parametres["gen_csv_dir"],"MEANSIGMA_"+tile_str+"_"+mindate.strftime("%Y%m%dT%H%M%S")
                                                                    +"_"+maxdate.strftime("%Y%m%dT%H%M%S")
                                                                    +".csv")
        out_temperature_csv = os.path.join(self.parametres["gen_csv_dir"],"MEANTEMP_"+tile_str+"_"+mindate.strftime("%Y%m%dT%H%M%S")
                                                                    +"_"+maxdate.strftime("%Y%m%dT%H%M%S")
                                                                    +".csv")
        
        self.gen_csv_otb(s1strdates,s1calfiles,erafilesord, self.parametres["tiled_rgp_img"], out_sigma40_csv, out_temperature_csv, eratypes)
        
        if not (os.path.exists(out_sigma40_csv) and  os.path.exists(out_temperature_csv)):
            logger.error("Failed to generate csv files")
            raise
        return (out_sigma40_csv, out_temperature_csv)