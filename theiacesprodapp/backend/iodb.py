import os, glob, csv, datetime, zipfile
from typing import List
import xml.etree.ElementTree as ET
import logging
from django.contrib.gis.gdal import CoordTransform, SpatialReference
from django.contrib.gis.gdal.datasource import DataSource
from theiacesprodapp import models as mod
from theiacesprodapp.backend import utils
from theiacesprodapp.backend.data import S1OrthoRaster, Sentinel1Zip, TiledRGP

logger = logging.getLogger(__name__)

parcellegraphique_mapping = {
    'id_parcel' : 'ID_PARCEL',
    # 'surf_parc' : 'SURF_PARC',
    'code_cultu' : 'CODE_CULTU',
    'code_group' : 'CODE_GROUP',
    # 'culture_d1' : 'CULTURE_D1',
    # 'culture_d2' : 'CULTURE_D2',
    'mpoly' : 'MULTIPOLYGON',
}




class AddParcels:

            
    @classmethod
    def loadByTile(cls, tiledrpg:TiledRGP):
        logger.info("Verifying database...")

        tquery = mod.TileEnvelope.objects.filter(tile__exact = tiledrpg.mgrstilename)
        if len(tquery) != 1 :
            logger.error(f"Tile {tiledrpg.mgrstilename} not found in db")
            raise Exception()

        qtest = mod.ParcelleGraphique.objects.filter(pub_year__exact = tiledrpg.pub_year, tileenvelope = tquery[0])
        if len(qtest) > 0:
            logger.error(f"RPG {str(tiledrpg.pub_year)} for tile {tiledrpg.mgrstilename} already processed in db")
            raise Exception()
        

        logger.info("Fill DB with extracted AOI...")
        ds = DataSource(tiledrpg.rpgvector)
        lyr = ds[0]
        srs = lyr.srs
        ct = CoordTransform(srs, SpatialReference(4326))
        for feat in lyr:
            fgeom = feat.geom
            fgeom.transform(ct)
            fquery = mod.ParcelleGraphique.objects.filter(pub_year__exact = tiledrpg.pub_year, id_parcel__exact = feat.get('ID_PARCEL'))
            if len(fquery) == 0:
                
                pg = mod.ParcelleGraphique(pub_year = tiledrpg.pub_year, id_parcel=feat.get('ID_PARCEL'), code_cultu=feat.get('CODE_CULTU'),
                                            code_group=feat.get('CODE_GROUP'), mpoly=fgeom)
                pg.tileenvelope_set.add(tquery[0])
                pg.save()
            else:
                tileset = fquery[0].tileenvelope_set.all()
                if not tquery[0] in tileset :
                    fquery[0].tileenvelope_set.add(tquery[0])
                    fquery[0].save()
                else:
                    logger.warning(f"Feature already in db")


class AddSentinel1Images():

    
    @classmethod
    def loadByTile(cls, tile_str:str, s1ortholist:List[S1OrthoRaster] ,ZIP_S1_dir:str):
        
        #get TileEnveloppe
        qs_tile = mod.TileEnvelope.objects.filter(tile__exact=tile_str)
        if len(qs_tile) != 1:
            logger.error(tile_str+" not found")
            raise Exception()
        #list dir for cals1
        for s1ortho in s1ortholist:
            
            #find associate zip
            grd_dir = glob.glob(os.path.join(ZIP_S1_dir,'*_'+s1ortho.s1date+'*.zip'))
            if len(grd_dir) == 0:
                logger.error("Can't find "+os.path.join(ZIP_S1_dir,'*_'+s1ortho.s1date+'*.zip')+" file")
                raise Exception()

            s1zip = Sentinel1Zip(grd_dir[0],'vh')
            orbpass = s1zip.get_pass_direction()
            tif_date = s1ortho.s1date
            tif_pltf = s1ortho.plateform

        #create object
            obj,created = mod.Sentinel1Image.objects.get_or_create(tile=tile_str, 
                                            sentinel1_date= tif_date,
                                            sentinel1_pltf=tif_pltf,
                                            sentinel1_orbit=orbpass
                                            )
            
            if created :
                logger.debug(str(obj)+" has been created")
            else:
                logger.debug(str(obj)+" already in database")



class AddParcelEvents():

    
    @classmethod
    def loadFreezeByTile(cls, rpg_pubyear:str,tile_str:str,sigma40_csv:str,temperature_csv:str):
        

        startproc = datetime.datetime.now()
        qs_tile = mod.TileEnvelope.objects.filter(tile__exact = tile_str)
        if len(qs_tile) != 1 :
            logger.error(f"Tile {tile_str} not found in db")
            raise Exception()

        logger.info("Reading CSVs....")
        s_data=[] 
        s_date=[]
        s_parc=[]
        with open(sigma40_csv) as f:
            i=0
            for line in f:
                rc = line.index("\n")
                if rc >= 0 :
                    line = line[:rc]
                sp_line = list(line.split(";"))
                
                    
                if i == 0:
                    s_date = sp_line[1:]
                elif len(sp_line) > 1 :
                    s_parc.append(sp_line[0])
                    s_data.append(sp_line[1:])
                i+=1
                
                
        t_data=[]  
        t_date=[]
        t_parc=[]
        with open(temperature_csv) as t:
            i=0
            for line in t:
                rc = line.index("\n")
                if rc >= 0 :
                    line = line[:rc]
                sp_line = list(line.split(";"))
                
                    
                if i == 0:
                    t_date = sp_line[1:]
                elif len(sp_line) > 1 :
                    t_parc.append(sp_line[0])
                    t_data.append(sp_line[1:])
                i+=1
        
        if len(s_data) != len(t_data):
            logger.error("Sigma and temp data count not equal. s_data = "+str(len(s_data))+" t_data = "+str(len(t_data)))
            raise Exception()
        if len(s_date) != len(t_date):
            logger.error("Sigma and temp date count not equal. s_date = "+str(len(s_date))+" t_date = "+str(len(t_date)))
            raise Exception()
        if len(s_parc) != len(t_parc):
            logger.error("Sigma and temp parcels count not equal. s_parc = "+str(len(s_parc))+" t_parc = "+str(len(t_parc)))
            raise Exception()

        
        logger.info ("Getting sentinel date from db...")
        nbdates=0
        dbimages=[]
        for col in s_date: 
            if not col == t_date[nbdates]:
                logger.error("Sigma csv date order and temp csv date order are not the same. Exiting")
                raise Exception()

            s1_date = datetime.datetime.strptime(col, "%Y%m%dT%H%M%S")
            qs_s1date = mod.Sentinel1Image.objects.filter(tile=qs_tile[0], 
                                                        sentinel1_date=s1_date
                                                        )
            if len(qs_s1date) != 1:
                logger.error("Sentinel1 date "+col+" not found in database. Exiting")
                raise Exception()
            
            dbimages.append(qs_s1date[0])
            
            nbdates+=1
        
        if len(s_date) != len(dbimages):
            logger.error("db request found different number of dates. exiting")
            raise Exception()

        
        logger.info('Loading db Objects in memory...')
        ls_data = len(s_parc)
        i=-1
        opbar=0
        objstocreate=[]
        nbc=0
        for sp in s_parc:
            i+=1
            
            qs_pg = mod.ParcelleGraphique.objects.filter(pub_year__exact = rpg_pubyear, id_parcel__exact=sp)
            if len(qs_pg) != 1:
                logger.warn("Parcel with id "+sp+" not found. Skipping...")
                continue
            
            qs_ptt = mod.PracelleTypeTransform.objects.filter(pub_year__exact = rpg_pubyear, rpg_code_group__exact = qs_pg[0].code_group)
            if len(qs_ptt) != 1:
                logger.warn(f"Parcel with id {sp} got wrong code_group={qs_pg[0].code_group}. Skipping...")
                continue

            parceltype = qs_ptt[0]

            try:
                indp_temp = t_parc.index(sp)
            except:
                logger.warn("Cant find Parcel with id "+sp+" for temperature csv. Skipping...")
                continue

            j=-1
            for od in dbimages:
                j+=1
                
                sigma = s_data[i][j]
                if utils.isfloat(sigma):                
                    v_meansigma40 = float(sigma)
                    if v_meansigma40 <= 0:
                        continue
                else:
                    continue
                
                v_meantemp = t_data[indp_temp][j]
                
                if utils.isfloat(v_meantemp):
                    v_meantemp = float(v_meantemp)
                else:
                    continue
                
                nbc+=1
                objstocreate.append(mod.ParcelEvent(parcelle=qs_pg[0], sentinel1=od, 
                                                                meansigma40 = v_meansigma40, 
                                                                meantemp = v_meantemp, 
                                                                parcelle_type = parceltype
                                                                ))

            pbar = int((i+1)/ls_data*100)
            if pbar % 5 == 0 and pbar > opbar:
                opbar = pbar
                #Test
                fobj = objstocreate[0]
                lobj = objstocreate[-1]
                fqs_pe = mod.ParcelEvent.objects.filter(parcelle=fobj.parcelle, sentinel1=fobj.sentinel1 )
                lqs_pe = mod.ParcelEvent.objects.filter(parcelle=lobj.parcelle, sentinel1=lobj.sentinel1 )
                if len(fqs_pe) != 1 and len(lqs_pe) != 1 :     
                    logger.info('Writing memory in database...')
                    mod.ParcelEvent.objects.bulk_create(objstocreate)
                    del objstocreate
                    objstocreate=[]
                    periodproc = datetime.datetime.now() - startproc
                    logger.info(str(pbar)+" percent done ( "+str(i+1)+" / "+str(ls_data)+" )")
                    logger.info("Elapse time : "+str(periodproc))
                    logger.info("Remaining   : "+str(periodproc/(pbar/100)-periodproc))
                    
                elif len(fqs_pe) == 1 and len(lqs_pe) == 1 :
                    objstocreate=[]
                    logger.info("bulk already recorded. Passing...")
                    periodproc = datetime.datetime.now() - startproc
                    logger.info(str(pbar)+" percent done ( "+str(i)+" / "+str(ls_data)+" )")
                    logger.info("Elapse time : "+str(periodproc))
                    logger.info("Remaining   : "+str(periodproc/(pbar/100)-periodproc))
                
                else:
                    logger.error("Error: bulk not fully recorded. Cleaning database is needed. Existing ")
                    raise Exception()

        if ls_data == (i+1):
            logger.info("Process done.")
        else :
            if len(objstocreate) != 0 :
                logger.info('Writing last bulk in database...')
                mod.ParcelEvent.objects.bulk_create(objstocreate)
            logger.info("Process done with anomaly in last bulk.")
        
        
    @classmethod
    def loadSSMByTile(cls, rpg_pubyear:str,tile_str:str,ssm_csvlist:List[str]):

        startproc = datetime.datetime.now()
        qs_tile = mod.TileEnvelope.objects.filter(tile__exact = tile_str)
        if len(qs_tile) != 1 :
            logger.error(f"Tile {tile_str} not found in db")
            raise Exception()

        logger.info("Reading CSVs....")
        s_data=[] 
        ssmhead=""
        opbar=0
        objstocreate=[]
        nbc=0
        i=-1
        for ssm_csv in ssm_csvlist:
            i+=1
            with open(ssm_csv) as f:
                h=0
                for line in f:
                    rc = line.index("\n")
                    if rc >= 0 :
                        line = line[:rc]
                    sp_line = list(line.split(";"))
                    
                        
                    if i == 0:
                        ssmhead = sp_line
                    elif len(sp_line) > 1 :
                        s_data.append(sp_line)
                    h+=1
                
            logger.info ("Getting sentinel date from db...")
            csv_date = os.path.basename(ssm_csv).split("_")[3]

            s1_date = datetime.datetime.strptime(csv_date, "%Y%m%dT%H%M%S")
            qs_s1date = mod.Sentinel1Image.objects.filter(tile=qs_tile[0], 
                                                        sentinel1_date=s1_date
                                                        )
            if len(qs_s1date) != 1:
                logger.error("Sentinel1 date "+csv_date+" not found in database. Exiting")
                raise Exception()
            

            logger.info('Loading db Objects in memory...')
            
            
            for sp in s_data:
                
                
                qs_pg = mod.ParcelleGraphique.objects.filter(pub_year__exact = rpg_pubyear, id_parcel__exact=sp[0])
                if len(qs_pg) != 1:
                    logger.warn("Parcel with id "+sp[0]+" not found. Skipping...")
                    continue

                objstocreate.append(mod.ParcelEvent(parcelle=qs_pg[0], sentinel1=qs_s1date[0], 
                                                                    meanvvsigma0 = sp[4], 
                                                                    meanndvi = sp[3], 
                                                                    meantheta = sp[2],
                                                                    soilmoist = sp[5]
                                                                    ))

            pbar = int((i+1)/len(ssm_csvlist)*100)
            logger.info('Writing memory in database...')
            mod.ParcelEvent.objects.bulk_create(objstocreate)
            del objstocreate
            objstocreate=[]
            if pbar % 10 == 0 and pbar > opbar:
                opbar = pbar
                periodproc = datetime.datetime.now() - startproc
                logger.info(str(pbar)+" percent done ( "+str(i+1)+" / "+str(len(ssm_csvlist))+" )")
                logger.info("Elapse time : "+str(periodproc))
                logger.info("Remaining   : "+str(periodproc/(pbar/100)-periodproc))

        logger.info("Process done.")
 