import os, glob, zipfile, shutil, re, math
from typing import List
import xml.etree.ElementTree as ET
from django.contrib.gis.gdal import SpatialReference, CoordTransform
from django.contrib.gis.gdal.geometries import OGRGeometry, Polygon
from django.contrib.gis.gdal.datasource import DataSource
from django.contrib.gis.gdal.raster.source import GDALRaster
from theiacesprodapp import models as mod
from theiacesprodapp.backend import utils
from theiacesprodapp.backend.utils import unzipfile, process_command
from osgeo import gdal, osr, ogr
from shapely.geometry import shape, mapping
import json, rasterio, otbApplication

import logging 

logger = logging.getLogger(__name__)

VSIZIP = '/vsizip/'

urlsrmt = "http://step.esa.int/auxdata/dem/SRTMGL1/"
srtmhgtzip = "/data/SRTM/SRTMHGTZIP"

class Raster:
    
    def __init__(self, filepath:str):
        self.filepath = filepath
        try:
            self.gdalraster = GDALRaster(filepath, write=False)
        except Exception as err:
            logger.error(f"Unable to open raster {filepath}")
            raise
        self.srs = self.gdalraster.srs
        self.enveloppe = self.__get_env_poly()
        self.enveloppe_wgs84 = self.enveloppe.clone()
        self.iswgs84 = True
        if self.gdalraster.srid != 4326 :
            self.iswgs84 = False
            wgs = SpatialReference(4326)
            ct = CoordTransform(self.srs , wgs)
            self.enveloppe_wgs84.transform(ct)
        
        self.gdalraster = None
        self.mgrstilename = None
        searchtile = re.findall("_T\d{2}[A-Z]{3}_",os.path.basename(filepath))
        if len(searchtile) != 0 :
            self.mgrstilename = searchtile[0]
    
    def __str__(self) -> str:
        return os.path.basename(self.filepath)

    def set_mgrstilename(self, tilename:str):
        self.mgrstilename = tilename
    
    def get_gdalraster(self) -> GDALRaster:
        return GDALRaster(self.filepath, write=False)
    
    def reproject_enveloppe_polygon(self,dst_proj:SpatialReference) -> OGRGeometry:
        
        ct = CoordTransform(self.srs , dst_proj)
        newenv = self.enveloppe.transform(ct, clone=True)
        
        return newenv
        
    def __get_env_poly(self) -> OGRGeometry:
        
        upx, xres, xskew, upy, yskew, yres = self.gdalraster.geotransform
        cols = self.gdalraster.width
        rows = self.gdalraster.height
         
        ulx = upx + 0*xres + 0*xskew
        uly = upy + 0*yskew + 0*yres
         
        llx = upx + 0*xres + rows*xskew
        lly = upy + 0*yskew + rows*yres
         
        lrx = upx + cols*xres + rows*xskew
        lry = upy + cols*yskew + rows*yres
         
        urx = upx + cols*xres + 0*xskew
        ury = upy + cols*yskew + 0*yres
        
        wktpoly = f"POLYGON(({llx} {lly},{ulx} {uly},{urx} {ury},{lrx} {lry},{llx} {lly}))"

        # # Create polygon
        # imgpoly = ogr.CreateGeometryFromWkt(wktpoly)
        
        # pointLL, pointUL, pointUR, pointLR = ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint)
        # pointLL.AddPoint(llx, lly)
        # pointUR.AddPoint(urx, ury)
        # pointUL.AddPoint(ulx, uly)
        # pointLR.AddPoint(lrx, lry)
        
        # ring = ogr.Geometry(ogr.wkbLinearRing)
        # ring.AddPoint(pointLL.GetX(), pointLL.GetY())
        # ring.AddPoint(pointUL.GetX(), pointUL.GetY())
        # ring.AddPoint(pointUR.GetX(), pointUR.GetY())
        # ring.AddPoint(pointLR.GetX(), pointLR.GetY())
        # ring.AddPoint(pointLL.GetX(), pointLL.GetY())
        
        # # Create polygon
        # imgpoly = ogr.Geometry(ogr.wkbPolygon)
        # imgpoly.AddGeometry(ring)
        
        ogrpoly = OGRGeometry(wktpoly,srs=self.srs)
        
            
        return ogrpoly
    
            
    def reprojectRaster(self,epsg:int, res=None, resampling=None):
        
        srid = self.get_gdalraster().srs.srid
        if srid == epsg:
            return self

        #create tmpfile
        dirname = os.path.dirname(self.filepath)
        tmpdir = os.path.join(dirname,"tmp_EPSG"+str(epsg))
        try:
            if not os.path.exists(tmpdir):
                os.mkdir(tmpdir)
        except Exception as err :
            logger.error("Reproject temp folder creation failed.")
            raise err
        
        filename = os.path.basename(self.filepath).split(".")[0]
        ext=os.path.basename(self.filepath).split(".")[1]
        
        oldfile = os.path.join(tmpdir,".".join([filename+"_old",ext]))
        tmpfile = os.path.join(tmpdir,".".join([filename+"_EPSG"+str(epsg),ext]))
        
        os.rename(self.filepath,oldfile)
        
        cmd = ['gdalwarp', '-t_srs', "EPSG:"+str(epsg), oldfile, tmpfile]

        if res != None :
            cmd.insert(1,str(res))
            cmd.insert(1,str(res))
            cmd.insert(1,"-tr")

        if resampling != None :
            cmd.insert(1,resampling)
            cmd.insert(1,"-r")
        
        try:
            process_command(cmd)
            os.rename(tmpfile,self.filepath)
            shutil.rmtree(tmpdir, ignore_errors=True)
        
        except Exception as err:
            os.rename(oldfile,self.filepath)
            if os.path.exists(tmpdir):
                shutil.rmtree(tmpdir, ignore_errors=True)
            logger.error(f"gdal_wrap failed to process {tmpfile}")
            raise 
        
        
        logger.info("gdalwarp succeeded on : "+self.filepath)
        self.__init__(self.filepath)
        
    def get_intersection_with_rasterlist(self, rasters:List['Raster']) -> OGRGeometry:
        intersection = None
        i=0
        for i in range(len(rasters)-1):
            if i == 0:
                intersection = self.enveloppe_wgs84.intersection(rasters[i].enveloppe_wgs84)
                continue
            else:
                poly1 = intersection
            poly2 = self.enveloppe_wgs84.intersection(rasters[i].enveloppe_wgs84)
            
            intersection = poly1.intersection(poly2)
            
        return intersection
    
    def export_polygon_to_geojson(self, polygon:OGRGeometry, geooutput:str):
        try:
            with open(geooutput,'w') as tstream :
                json.dump(polygon.json,tstream)
        except Exception :
            logger.error("Unable to write file : "+geooutput)
            raise 
        
    def create_footprint(self,filepath:str, poly:OGRGeometry) -> str:
        outdir = os.path.dirname(filepath)
        if not os.path.exists(outdir):
            os.mkdir(outdir)

        shpfile = os.path.basename(filepath)
        name = shpfile.split(".")[0]
        
        driver = ogr.GetDriverByName("ESRI Shapefile")
            
        # create the data source
        data_source = driver.CreateDataSource(filepath)
        
        # create the spatial reference, WGS84
        srs = osr.SpatialReference()
        srs.ImportFromEPSG(4326)
        
        # create the layer
        layer = data_source.CreateLayer(name, srs, ogr.wkbMultiPolygon)
         
        # create the feature
        feature = ogr.Feature(layer.GetLayerDefn())
        ogrpoly =  ogr.CreateGeometryFromWkt(poly.wkt)
        feature.SetGeometry(ogrpoly)
        # Create the feature in the layer (shapefile)
        layer.CreateFeature(feature)
        
        return filepath
    

    @classmethod
    def create_filtlabels_from_vector(cls, params:dict) -> 'Raster':

        rastertifout = os.path.join(params["processrgpdir"],"RPG_FILTLABELS_"+params['mrgstile']+".TIF")
        if os.path.exists(rastertifout) and not params["overwrite"]:
            logger.info(f"File already exists : {rastertifout}. Passing...")
            return Raster(rastertifout)

        if params['zone'] == "s2tile":
            qtiles = mod.TileEnvelope.objects.filter(tile__exact = params['mrgstile'])
            if len(qtiles) != 1 :
                logger.error(f"Database request for tile {params['mrgstile']} failed.")
                raise 

            gtile = qtiles[0].geom

            ds = DataSource(params['rgpshp'])
            layer = ds[0]
            if layer.srs.srid != 4326:
                projdb = SpatialReference(4326)
                ct = CoordTransform(projdb, layer.srs)
                gtile.transform(ct, clone=False)

            clipsrc = gtile.ewkt.split(";")[1]
            logger.debug("Clip source = "+str(clipsrc))
            labelshpfile = os.path.join(params["processrgpdir"],"RPG_CLIP_"+params['mrgstile']+".shp")
            
            del ds

            logger.info("Clip shapefile with tile :"+params['mrgstile'])
            if os.path.exists(labelshpfile) and not params["overwrite"] :
                logger.info(f"File already exists : {labelshpfile}. Passing...")
            else:
                cmd=['ogr2ogr', '-clipsrc', clipsrc, labelshpfile, params['rgpshp']]
                utils.process_command(cmd)
                

            if not os.path.exists(labelshpfile):
                logger.error(f"File not found : {labelshpfile}")
                raise 
        else:
            labelshpfile = params['rgpshp']

        ds = DataSource(labelshpfile)
        layer = ds[0]
        pextent = OGRGeometry(layer.extent.wkt, srs=layer.srs)
        epsgcode = layer.srs.srid

        if layer.srs.geographic :
            #TODO reproject vector
            projforast = SpatialReference(3857)
            epsgcode = projforast.srid
            ct = CoordTransform(layer.srs, projforast)
            pextent.transform(ct, clone=False)
        
        szx = int((pextent.envelope.max_x - pextent.envelope.min_x)/10)
        szy = int((pextent.envelope.max_y - pextent.envelope.min_y)/10)
        
        logger.info("Rasterizing  :"+params['rgpshp'])
        rastparams = {"szx": szx,
            "szy": szy,
            "epsg": epsgcode,
            "orx": pextent.envelope.min_x,
            "ory": pextent.envelope.max_y,
            "mode.attribute.field": params['labelsfield']}
        logger.debug(rastparams)


        app1 = otbApplication.Registry.CreateApplication("Rasterization")
        app1.SetParameterString("in", labelshpfile)
        app1.SetParameterString("out", "temprast.tif")
        app1.SetParameterInt("szx", szx)
        app1.SetParameterInt("szy", szy)
        app1.SetParameterInt("epsg", epsgcode)
        app1.SetParameterFloat("orx", pextent.envelope.min_x)
        app1.SetParameterFloat("ory", pextent.envelope.max_y)
        app1.SetParameterFloat("spx", 10.)
        app1.SetParameterFloat("spy", -10.)
        app1.SetParameterString("mode", "attribute")
        app1.SetParameterString("mode.attribute.field", params['labelsfield'])
        app1.Execute()
        
        agrivalues = params['agrivalues']
        if agrivalues == "all":  
            app2 = otbApplication.Registry.CreateApplication("BandMath")
            app2.AddImageToParameterInputImageList("il",app1.GetParameterOutputImage("out"))
            app2.SetParameterString("out", "mastmp.tif")
            app2.SetParameterString("exp", "im1b1>0?1:0")
            app2.Execute()
            agrivalues="1"
        else:
            app2 = otbApplication.Registry.CreateApplication("Rasterization")
            app2.SetParameterString("in",labelshpfile)
            app2.SetParameterString("out", "mastmp.tif")
            app2.SetParameterInt("szx", szx)
            app2.SetParameterInt("szy", szy)
            app2.SetParameterInt("epsg", epsgcode)
            app2.SetParameterFloat("orx", pextent.envelope.min_x)
            app2.SetParameterFloat("ory", pextent.envelope.max_y)
            app2.SetParameterFloat("spx", 10.)
            app2.SetParameterFloat("spy", -10.)
            app2.SetParameterString("mode", "attribute")
            app2.SetParameterString("mode.attribute.field", params['landusefield'])
            app2.Execute()
        
        
        
        app4 = otbApplication.Registry.CreateApplication("SoilMoistureSegmentationFiltering")
        app4.SetParameterInputImage("labels",app1.GetParameterOutputImage("out"))
        app4.SetParameterInputImage("lulc",app2.GetParameterOutputImage("out"))
        app4.SetParameterStringList("agrivalues",agrivalues.split(" "))
        app4.SetParameterString("out",rastertifout)
        app4.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint32)
        
        logger.info("Masking segmentation labels")
        # The following line execute the applicationw
        app4.ExecuteAndWriteOutput()

        if not os.path.exists(rastertifout) :
            logger.error("File not found after rasterization")
            raise Exception("File not found after rasterization")

        return Raster(rastertifout)


    @classmethod
    def create_from_vector(cls, params:dict, agrifield:str) -> 'Raster':

        rastertifout = os.path.join(params["processrgpdir"],"RPG_"+agrifield.upper()+"_"+params['mrgstile']+".TIF")
        if os.path.exists(rastertifout) and not params["overwrite"]:
            logger.info(f"File already exists : {rastertifout}. Passing...")
            return rastertifout

        if params['zone'] == "s2tile":
            qtiles = mod.TileEnvelope.objects.filter(tile__exact = params['mrgstile'])
            if len(qtiles) != 1 :
                logger.error(f"Database request for tile {params['mrgstile']} failed.")
                raise 

            gtile = qtiles[0].geom

            ds = DataSource(params['rgpshp'])
            layer = ds[0]
            if layer.srs.srid != 4326:
                projdb = SpatialReference(4326)
                ct = CoordTransform(projdb, layer.srs)
                gtile.transform(ct, clone=False)

            clipsrc = gtile.ewkt.split(";")[1]
            logger.debug("Clip source = "+str(clipsrc))
            labelshpfile = os.path.join(params["processrgpdir"],"RPG_CLIP_"+params['mrgstile']+".shp")
            
            del ds

            logger.info("Clip shapefile with tile :"+params['mrgstile'])
            if os.path.exists(labelshpfile) and not params["overwrite"] :
                logger.info(f"File already exists : {labelshpfile}. Passing...")
            else:
                cmd=['ogr2ogr', '-clipsrc', clipsrc, labelshpfile, params['rgpshp']]
                utils.process_command(cmd)
                

            if not os.path.exists(labelshpfile):
                logger.error(f"File not found : {labelshpfile}")
                raise 
        else:
            labelshpfile = params['rgpshp']

        ds = DataSource(labelshpfile)
        layer = ds[0]
        pextent = OGRGeometry(layer.extent.wkt, srs=layer.srs)
        epsgcode = layer.srs.srid

        if layer.srs.geographic :
            projforast = SpatialReference(3857)
            epsgcode = projforast.srid
            ct = CoordTransform(layer.srs, projforast)
            pextent.transform(ct, clone=False)
        
        szx = int((pextent.envelope.max_x - pextent.envelope.min_x)/10)
        szy = int((pextent.envelope.max_y - pextent.envelope.min_y)/10)
        
        logger.info("Rasterizing  :"+params['rgpshp'])
        rastparams = {"szx": szx,
            "szy": szy,
            "epsg": epsgcode,
            "orx": pextent.envelope.min_x,
            "ory": pextent.envelope.max_y,
            "mode.attribute.field": params[agrifield]}
        logger.debug(rastparams)

        app1 = otbApplication.Registry.CreateApplication("Rasterization")
        app1.SetParameterString("in", params['rgpshp'])
        app1.SetParameterString("out", rastertifout)
        app1.SetParameterInt("szx", szx)
        app1.SetParameterInt("szy", szy)
        app1.SetParameterInt("epsg", epsgcode)
        app1.SetParameterFloat("orx", pextent.envelope.min_x)
        app1.SetParameterFloat("ory", pextent.envelope.max_y)
        app1.SetParameterFloat("spx", 10.)
        app1.SetParameterFloat("spy", -10.)
        app1.SetParameterString("mode", "attribute")
        app1.SetParameterString("mode.attribute.field", params[agrifield])
        if agrifield == "landusefield":
            app1.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
        else:
            app1.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint32)
        app1.ExecuteAndWriteOutput()

        if not os.path.exists(rastertifout) :
            logger.error("File not found after rasterization")
            raise Exception("File not found after rasterization")

        return Raster(rastertifout)

class S1OrthoRaster(Raster):

    
    def __init__(self, filepath:str):
        super().__init__(filepath)
        fsplit = os.path.basename(filepath).split("_")
        self.polarmode = fsplit[-1].split(".")[0]
        self.plateform = fsplit[0]
        fdate = re.findall("20\d{6}T\d{6}",os.path.basename(filepath))
        self.s1date = fdate[0]
        self.auxfilespath = {"type":"", "THETASURF":"","THETAELLI":"","THETALOC":""}
        
    def create_aux_filepath(self,auxdir:str, auxtmpdir:str, incidence:str):
        
        imgsp = os.path.basename(self.filepath).split("_")
         
        out_THETASURF = os.path.join(auxtmpdir,"_".join(imgsp[:-1])+"_THETASURF.TIF")
        out_THETAELLI = os.path.join(auxdir,"_".join(imgsp[:-1])+"_THETAELLI.TIF")
        out_THETALOC = os.path.join(auxdir,"_".join(imgsp[:-1])+"_THETALOC.TIF")
        
        self.auxfilespath["THETASURF"] = out_THETASURF
        if incidence == "loc" :
            self.auxfilespath["type"] = "LOC"
            self.auxfilespath["THETA"+self.auxfilespath["type"]] = out_THETALOC
        else :
            self.auxfilespath["type"] = "ELLI"
            self.auxfilespath["THETA"+self.auxfilespath["type"]] = out_THETAELLI
            
            
    def populate_auxfilepath(self, incidence:str):
        auxdir = os.path.join(os.path.dirname(self.filepath),"auxfiles")
        if not os.path.exists(auxdir):
            raise Exception("Find auxdir failed : "+auxdir)
        imgsp = os.path.basename(self.filepath).split("_")
        
        out_THETA = os.path.join(auxdir,"_".join(imgsp[:-1])+"_THETA"+incidence.upper()+".TIF")
        
        if not os.path.exists(out_THETA):
            logger.error(out_THETA+" does not exist")
            raise Exception(out_THETA+" does not exist")
        
        self.auxfilespath["type"] = incidence.upper()
        self.auxfilespath["THETA"+self.auxfilespath["type"]] = out_THETA
    
    def reprojectRaster(self,epsg:int, incidence:str):
        super().reprojectRaster(epsg)
        self.__init__(self.filepath)
        self.populate_auxfilepath(incidence)
    

class ImageZip:
    
    
    def __init__(self, filepath:str):
        if "file://" in filepath:
            filepath = filepath[7:]
        self.filepath = filepath
        self.extract_dir = ""
    
    def __str__(self) -> str:
        return os.path.basename(self.filepath)
    
    def isunzip(self, extract_dir:str) -> bool:
        test = self.get_zip_filetree()[0]
        if os.path.exists(os.path.join(extract_dir,test)):
            self.extract_dir = extract_dir
            return True
        else:
            return False
        
    def extract_in_tmp(self,extract_dir:str):
        self.extract_dir = extract_dir
        try:
            unzipfile(self.filepath, extract_dir)
        except:
            raise Exception("Unzip failed : "+self.filepath)
        
    def get_zip_filetree(self) -> List[str]:
        filetree = []
        with zipfile.ZipFile(self.filepath) as zf:
            for f in zf.namelist():
                filetree.append(f)
            
        return filetree
    
    def create_tmp_shp(self,name:str, poly:OGRGeometry, outdir:str) -> str:
        
        if not os.path.exists(outdir):
            os.mkdir(outdir)

        shpfile = name+".shp"
        outshp = os.path.join(outdir,shpfile)
        
        driver = ogr.GetDriverByName("ESRI Shapefile")
            
        # create the data source
        data_source = driver.CreateDataSource(outshp)
        
        # create the spatial reference, WGS84
        srs = osr.SpatialReference()
        srs.ImportFromEPSG(4326)
        
        # create the layer
        layer = data_source.CreateLayer(name, srs, ogr.wkbMultiPolygon)
         
        # create the feature
        feature = ogr.Feature(layer.GetLayerDefn())
        ogrpoly =  ogr.CreateGeometryFromWkt(poly.wkt)
        feature.SetGeometry(ogrpoly)
        # Create the feature in the layer (shapefile)
        layer.CreateFeature(feature)
        
        return outshp
    
    def check_zip_conformity(self) -> bool :
        try:
            filetree = self.get_zip_filetree()
            return True
        except :
            logger.warning(f"Unable to open {self.filepath}. Removing local file")
            shutil.rmtree(self.filepath, ignore_errors= True)
            return False
        
    def verify_checksum_zip(self)-> bool:
        basedir = os.path.dirname(self.filepath)
        zipfile = os.path.basename(self.filepath)
        s1name = zipfile.split(".")[0]
        cheksumfile = os.path.join(basedir,s1name+".SAFE.md5")
        if not os.path.exists(cheksumfile) :
            logger.error(f"Checksum file not found for {zipfile}")
            raise Exception("Checksum file not found")
        
        checksum = None
        with open(cheksumfile,"r") as ck:
            checksum = ck.readline().strip()

        if checksum == None or checksum == "":
            logger.error(f"No checksum in file {cheksumfile}")
            raise Exception("Checksum not found in file")
        
        logger.info(f"Checksum for {s1name} : {checksum}")
                
        safechecksum = utils.compute_md5(self.filepath)
        if checksum == safechecksum:
            logger.info(f"Checksum verified for {s1name}")
            return True
        else:
            logger.error(f"Checksum failed for {s1name}")
            return False
        

            
    
class Sentinel1Zip(ImageZip):
    
    
    def __init__(self, filepath:str, refraster:Raster, polarmode='vv'):
        super().__init__(filepath)
        self.polarmode = polarmode
        bname = os.path.basename(filepath).split(".")[0]
        splitbname = bname.split("_")
        self.prefix = "_".join(splitbname[:3])
        self.plateforme = splitbname[0]
        self.acqdate_start = splitbname[4]  
        self.acqdate_end = splitbname[5]
        self.absorbit = splitbname[6]
        
        self.ziptiff_file = self.__find_tiff_in_zip()
        self.extent = self.__get_gcp_extend()
    
        self.intersect_ref_poly = self.extent.intersection(refraster.enveloppe_wgs84)
        
        self.tiff_file=""
        self.anno_file=""

    
    def __find_tiff_in_zip(self) -> str:
        extension = ".tiff"
        mode = "-"+self.polarmode+"-"
        for f in self.get_zip_filetree():
            if f.lower().endswith(extension) and f.lower().find(mode) >= 0 :
                return VSIZIP + os.path.join(self.filepath, f)

    def get_pass_direction(self) -> str:

        with zipfile.ZipFile(self.filepath) as zf:
            anno_file = None
            for zc in zf.namelist():
                zcsplit = zc.split("/")
                if len(zcsplit) == 3 and zcsplit[1] == "annotation" and f"-{self.polarmode }-" in zcsplit[2] and ".xml" in zcsplit[2]:
                    anno_file = zc
                    break
            
            if None == anno_file:
                logger.error("Can't find annotation file in archive")
                raise Exception()
        
            anno_open = zf.open(anno_file)
            tree = ET.parse(anno_open)
            root = tree.getroot()
            orbtag = root.find(".//generalAnnotation/productInformation/pass").text
            if orbtag == "Descending":
                tif_orb = "DES"
            elif orbtag == "Ascending":
                tif_orb = "ASC"
            else:
                logger.error("Error on orbit metadata, found : "+orbtag)
                raise Exception()

        return tif_orb
            
    def find_annotiff_in_unzip(self, polarmode):
        
        mode = "-"+polarmode+"-"
        annofile = None
        tifffile = None
        for z in self.get_zip_filetree():
            
            if mode in z and z.lower().endswith("xml") and z.find("calibration") == -1 and z.find("noise") == -1:
                annofile = z
            
            if mode in z and z.lower().endswith("tiff"):
                tifffile = z
                
        if None == annofile :
            logger.error("Unable to find annotation*.xml in zip "+self.filepath)
            raise Exception("Unable to find annotation*.xml in zip "+self.filepath)
        
        if None == tifffile :
            logger.error("Unable to find s1a*.tiff in zip "+self.filepath)
            raise Exception("Unable to find s1a*.tiff in zip "+self.filepath)
        
        annofile = os.path.join(self.extract_dir,annofile)
        tifffile = os.path.join(self.extract_dir,tifffile)
        
        if not os.path.exists( annofile ):
            logger.error(f"{annofile} does not exist")
            raise Exception(f"{annofile} does not exist")
        
        if not os.path.exists( tifffile ):
            logger.error(f"{tifffile} does not exist")
            raise Exception(f"{tifffile} does not exist")
        
#         annofiles = glob.glob(os.path.join(os.path.join(self.extract_dir,safedir),"**/*"+mode+"*"+extension))
        self.anno_file = annofile
        self.tiff_file = tifffile
        
        logger.debug("Annotation found : "+annofile)
        logger.debug("Annotation found : "+tifffile)
        
        
    def __get_gcp_extend(self) -> OGRGeometry:
        raster = gdal.Open(self.ziptiff_file)
        rgcp = raster.GetGCPs()
#         logger.info([rgcp[0].GCPX,rgcp[0].GCPY])
#         logger.info([rgcp[-1].GCPX,rgcp[-1].GCPY])
        
        pointLL, pointUL, pointUR, pointLR = ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint)
        for r in rgcp:
            if r.GCPPixel == 0 and r.GCPLine == 0:
                pointLL.AddPoint(r.GCPX, r.GCPY)
            elif r.GCPPixel == 0 and r.GCPLine == rgcp[-1].GCPLine:
                pointUL.AddPoint(r.GCPX, r.GCPY)
            elif r.GCPPixel == rgcp[-1].GCPPixel and r.GCPLine == 0:
                pointLR.AddPoint(r.GCPX, r.GCPY)
            elif r.GCPPixel == rgcp[-1].GCPPixel and r.GCPLine == rgcp[-1].GCPLine:
                pointUR.AddPoint(r.GCPX, r.GCPY)
                
        ring = ogr.Geometry(ogr.wkbLinearRing)
        ring.AddPoint(pointLL.GetX(), pointLL.GetY())
        ring.AddPoint(pointUL.GetX(), pointUL.GetY())
        ring.AddPoint(pointUR.GetX(), pointUR.GetY())
        ring.AddPoint(pointLR.GetX(), pointLR.GetY())
        ring.AddPoint(pointLL.GetX(), pointLL.GetY())
        
        # Create polygon
        gcppoly = ogr.Geometry(ogr.wkbPolygon)
        gcppoly.AddGeometry(ring)
        
        ogrpoly = OGRGeometry(gcppoly.ExportToWkt(),srs=SpatialReference(4326))
   
        return ogrpoly

class Sentinel2Zip(ImageZip):
    searchfileparam={
        "S2-SEN2COR":{
            "extension":"jp2",
            "bandred":{"name":"B04","resolution":"10m"},
            "mask":{"name":"CLD","resolution":"20m","goodvalue":"0"},
            "bandnir":"B08",
            "indexdate":2
            },
        "S2-MUSCATE":{
            "extension":"tif",
            "bandred":{"name":"_FRE_B4","resolution":"SENTINEL2"},
            "mask":{"name":"_CLM_R2","resolution":"SENTINEL2","goodvalue":"0"},
            "bandnir":"B8.",
            "indexdate":1
            },
        "S2-L3A":{
            "extension":"tif",
            "bandred":{"name":"_FRC_B4","resolution":"SENTINEL2"},
            "mask":{"name":"_FLG_R2","resolution":"SENTINEL2","goodvalue":"4"},
            "bandnir":"_FRC_B8.",
            "indexdate":1
            },
        "L8-OLI/TIRS":{
            "extension":"tif",
            "bandred":{"name":"_sr_band3","resolution":"LC08_L1TP"},
            "mask":{"name":"_pixel_qa","resolution":"LC08_L1TP","goodvalue":"322 or im2b1==386 or im2b1==834 or im2b1==898 or im2b1==1346"},
            "bandnir":"band5",
            "indexdate":3
            }
        }
    
    def find_fileformat(self, filepath:str)->str:
        fileformats=["S2-SEN2COR","S2-MUSCATE","S2-L3A"]
        filename = os.path.basename(filepath)
        splitf = filename.split("_")
        if len(splitf) == 7 :
            return fileformats[0]
        elif len(splitf) == 5 :
            if splitf[0].endswith("2X"):
                return fileformats[2]
            elif splitf[0].endswith("2A") or splitf[0].endswith("2B"):
                return fileformats[1]
            else:
                raise Exception("Finding file format failed")
        else:
            raise Exception("Finding file format failed")


    
    def __init__(self, filepath:str):
        super().__init__(filepath)
        self.bandredvsi = None
        self.bandmaskvsi = None
        self.bandnirvsi = None
        self.fileformat = self.find_fileformat(filepath)
        fileformat = self.fileformat

        zft = self.get_zip_filetree()
        for f in zft :
            if (f.lower().find(Sentinel2Zip.searchfileparam[fileformat]["bandred"]["resolution"].lower()) >= 0) and (
                f.lower().find(Sentinel2Zip.searchfileparam[fileformat]["bandred"]["name"].lower()) >= 0) and (
                f.lower().endswith(Sentinel2Zip.searchfileparam[fileformat]["extension"].lower())):
                self.bandredvsi = "/vsizip/"+os.path.join(filepath,f)
            if (f.lower().find(Sentinel2Zip.searchfileparam[fileformat]["mask"]["resolution"].lower()) >= 0) and (
                f.lower().find(Sentinel2Zip.searchfileparam[fileformat]["mask"]["name"].lower()) >= 0) and (
                f.lower().endswith(Sentinel2Zip.searchfileparam[fileformat]["extension"].lower())):
                self.bandmaskvsi = "/vsizip/"+os.path.join(filepath,f)      
            if (f.lower().find(Sentinel2Zip.searchfileparam[fileformat]["bandred"]["resolution"].lower()) >= 0) and (
                f.lower().find(Sentinel2Zip.searchfileparam[fileformat]["bandnir"].lower()) >= 0) and (
                f.lower().endswith(Sentinel2Zip.searchfileparam[fileformat]["extension"].lower())):
                self.bandnirvsi = "/vsizip/"+os.path.join(filepath,f)  
                
        if self.bandredvsi == None :
            logger.error(f"Searching Red band failed in :{filepath}")
            # logger.error(f"Zipfile tree :")
            # for f in zft :
            #     logger.error(f"||||{f}")

            raise Exception()
        if self.bandmaskvsi == None :
            logger.error(f"Searching Mask band failed in :{filepath}")
            # logger.error(f"Zipfile tree :")
            # for f in zft :
            #     logger.error(f"||||{f}")

            raise Exception()
        if self.bandnirvsi == None :
            logger.error(f"Searching Nir band failed in :{filepath}")
            # logger.error(f"Zipfile tree :")
            # for f in zft :
            #     logger.error(f"||||{f}")

            raise Exception()


        s2date = os.path.basename(filepath).split("_")[Sentinel2Zip.searchfileparam[fileformat]["indexdate"]]
        if fileformat == "S2-L3A" or fileformat == "S2-MUSCATE":
            self.date = "T".join(s2date.split("-")[:2])
        else:
            self.date = s2date
        if not self.date[:8].isdigit():
            logger.error(f"Unable to find date in {filepath}")
            raise Exception(f"Unable to find date in {filepath}")
        
class Sentinel2PCStac():
    bands = ["B04","B08","SCL"]
    params = {
        "extension":"tif",
        "bandred":{"name":"BO4","resolution":"10m"},
        "mask":{"name":"SLC","resolution":"10m"},
        "bandnir":"B08",
        "indexdate":1
    }
    def __init__(self, directory:str):
        self.directory = directory
        self.date = os.path.basename(directory).split("_")[2]
        self.bandred = None
        self.bandnir = None
        self.mask = None

        imgfiles = utils.search_files(directory, resolution=self.date, extension="tif")
        if len(imgfiles) > 0:
            for b in imgfiles:
                if self.bands[0] in b:
                    self.set_bandred(b)
                if self.bands[1] in b:
                    self.set_bandnir(b)
                if self.bands[2] in b:
                    self.set_mask(b)
    
    def __str__(self) -> str:
        inbands = {os.path.basename(self.directory):[]}
        for b in self.bands:
            inbands[os.path.basename(self.directory)].append(os.path.basename(self.get_band(b)))
        return inbands.__str__()
    
    def is_all_bands_ok(self):
        if self.bandred != None and self.bandnir != None and self.mask != None :
            return True
        else:
            return False

    def get_directory(self)->str:
        return self.directory

    def get_date(self)->str:
        return self.date

    def set_band(self, band:str, file:str):
        if band == self.bands[0]:
            self.set_bandred(file)
        if band == self.bands[1]:
            self.set_bandnir(file)
        if band == self.bands[2]:
            self.set_mask(file)

    def set_bandred(self, bandred:str):
        self.bandred = bandred

    def set_bandnir(self, bandnir:str):
        self.bandnir = bandnir
    
    def set_mask(self, mask:str):
        self.mask = mask

    def get_band(self, band:str)->str:
        if band == self.bands[0] and self.bandred != None:
            return self.bandred
        if band == self.bands[1] and self.bandnir != None:
            return self.bandnir
        if band == self.bands[2] and self.mask != None:
            return self.mask

    def get_bandred(self)->str:
        return self.bandred

    def get_bandnir(self)->str:
        return self.bandnir
    
    def get_mask(self)->str:
        return self.mask
    
    
class SRTMTileRaster:
    
    
    def __init__(self, refimage:Raster, srtmhgtzip:str):
        if not os.path.exists(srtmhgtzip):
            try:
                os.makedirs(srtmhgtzip)
            except:
                raise Exception(f"Error creating folders {srtmhgtzip}")
        
        self.refimage = refimage
        xmin,ymin,xmax,ymax = refimage.enveloppe_wgs84.extent
        tilesname = self.find_srtm_hgt_name(xmin-1.5, ymin-1 , xmax+1.5, ymax+1 )
        
        self.failedtiles = []
        self.tilefiles, self.failedtiles = self.prepare_srtm_hgt(tilesname,srtmhgtzip)
        
        self.tmpsrtmKO = []
        self.OKtmpsrtm = []
        
    def __str__(self) -> str:
        tiles=[]
        for tile in self.tilefiles:
            tiles.append(os.path.basename(tile))
        return str(tiles)
    
    def find_srtm_hgt_name(self, llx:float, lly:float , urx:float, ury:float ) -> List[str]:
        pointLL, pointUR = ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint)
        pointLL.AddPoint(llx, lly)
        pointUR.AddPoint(urx, ury)
        
        tilesname=[]
        logger.debug("Get SRTM for LowerLeft="+pointLL.ExportToWkt()+";UpperRight="+pointUR.ExportToWkt())
        
        for o in range(math.ceil(pointUR.GetX()) - math.floor(pointLL.GetX())):
            for a in range(math.ceil(pointUR.GetY()) - math.floor(pointLL.GetY())):
                lat =  math.floor(pointLL.GetY()) + a
                lon =  math.floor(pointLL.GetX()) + o
                if lat >= 0 :
                    hem = 'N'
                else : 
                    hem = 'S'
                    lat = abs(lat)
                if lon >= 0 :
                    grw = 'E'
                else : 
                    grw = 'W'
                    lon = abs(lon)
                tilesname.append(hem+f'{lat:02}'+grw+f'{lon:003}')
        
        return tilesname 
    
    
    def prepare_srtm_hgt(self, tilesname:List[str], srtmhgtzip:str):    
        logger.debug("Checking SRTM tiles: "+str(tilesname))
        
        failedtiles=self.failedtiles
        if len(tilesname) == 0:
            raise Exception("Tiles name list is empty")
        
        srtmsuf = ".SRTMGL1.hgt.zip"
        srtmarch = ".hgt"   
        
        hgtziptiles=[]
        
        for tile in tilesname :
            if tile in failedtiles :
                continue
            tilef = os.path.join(srtmhgtzip,tile + srtmsuf)
            if not os.path.exists(tilef):
                logger.debug("Downloading SRTM tile : "+tilef)
                cmd=["wget","-P", srtmhgtzip,urlsrmt+tile + srtmsuf]
                srtmzip = os.path.join(tilef,tile+srtmarch)
                
                try:
                    process_command(cmd)
                    if not os.path.exists(tilef):
                        logger.warning("Failed to download. Not using: "+urlsrmt+tile + srtmsuf)
                        failedtiles.append(VSIZIP+srtmzip)
                        continue
                    
                    hgtziptiles.append(VSIZIP+srtmzip)
                    
                except Exception as err :
                    logger.warning("Failed to download. Not using: "+urlsrmt+tile + srtmsuf)
                    failedtiles.append(VSIZIP+srtmzip)
                
            else :
                hgtziptiles.append(VSIZIP+os.path.join(tilef,tile+srtmarch))
            
        return hgtziptiles, failedtiles
        
    
        
    def update_srtm_with_geom(self,geom:OGRGeometry):
        xmin,ymin,xmax,ymax = geom.extent
        tilesname = self.find_srtm_hgt_name(xmin-0.25, ymin-0.13 , xmax+0.25, ymax+0.13 )
        self.tilefiles, self.failedtiles = self.prepare_srtm_hgt(tilesname,srtmhgtzip)
        
                
        
    def prepare_tmpdemdir(self, tmpdemdir):
        
           
            
        for srtm in self.tilefiles:
            ex = srtm.split("/vsizip/")[1]
            zipfile = ex.split('.zip')[0]+'.zip'
            basesplt = os.path.basename(zipfile).split(".")
            hgtfile = os.path.join(tmpdemdir,".".join([basesplt[0],basesplt[2]]))
            
            if not basesplt[0] in self.OKtmpsrtm :
                
                if os.path.exists(hgtfile):
                    self.OKtmpsrtm.append(basesplt[0])
                    
                else:
                    if basesplt[0] in self.tmpsrtmKO :
                        logger.debug("Unzip failed once. Passing "+basesplt[0])
                    else:
                        try:
                            unzipfile(zipfile, tmpdemdir)
                        except Exception as warn:
                            logger.warning(f"Unzip SRTM tile failed : {zipfile}")
                            logger.warning(str(warn))
                    
                        if not os.path.exists(hgtfile):
                            self.tmpsrtmKO.append(basesplt[0])
                        else:
                            self.OKtmpsrtm.append(basesplt[0])
        if len(self.tmpsrtmKO)>0:
            logger.debug("Failed SRTM: "+str(self.tmpsrtmKO))   
        logger.info(f"Using SRTM: "+str(self.OKtmpsrtm))
        
        
class ERA5(Raster):
    #TODO
        
    def __init__(self, filepath:str):
        super().__init__(filepath)
        
    
        
class TiledRGP(Raster):

    def __init__(self, pub_year:int, rasterpath:str, vectorpath:str=None):
        super().__init__(rasterpath)
        self.pub_year = pub_year
        if vectorpath == None :
            basedir = os.path.dirname(rasterpath)
            fres = glob.glob(os.path.join(basedir,"*.shp"))
            if len(fres) == 0 :
                raise Exception("RPG shapefile do not exist.")
            
        self.rpgvector = vectorpath

    @classmethod
    def create(cls, pub_year:int, globalRGPvectfile:str, tilename:str,  labelsfield:str, outdir:str) -> 'TiledRGP':

        ds = DataSource(globalRGPvectfile)
        srs = ds[0].srs
        
        qtiles = mod.TileEnvelope.objects.filter(tile__eq = tilename)
        if len(qtiles) != 1 :
            logger.error(f"Database request for tile {tilename} failed.")
            raise 

        qtiles[0].geom.transform(srs)
        clipsrc = qtiles[0].ewkt.split(";")[1]
        logger.debug("Clip source = "+str(clipsrc))
        labelshpfile = os.path.join(outdir,"RPG_"+tilename+".shp")
        
        del ds
        
        logger.info("Clip Tile from RGP shapefile...")
        if not os.path.exists(labelshpfile) :
            cmd=['ogr2ogr', '-clipsrc', clipsrc, labelshpfile, globalRGPvectfile]
            utils.process_command(cmd)
        else:
            logger.info(f"File already exists : {labelshpfile}. Passing...")

        if not os.path.exists(labelshpfile):
            logger.error(f"File not found : {labelshpfile}")
            raise 

        
        labeltifout = os.path.join(outdir,"RPG_"+tilename+".TIF")
        # labelsfield="id_parcel"
        
        logger.info("Rasterizing tiled RGP shapefile")
        if os.path.exists(labelshpfile) :
            logger.info(f"File already exists : {labeltifout}. Passing...")
            return TiledRGP(labeltifout,labelshpfile)

        driver = ogr.GetDriverByName('ESRI Shapefile')
        
        dataSource = driver.Open(labelshpfile, 0)
        
        layer = dataSource.GetLayer()
        shpextent = layer.GetExtent() # [xmin,xmax,ymin,ymax]
        
        szx = int((shpextent[1] - shpextent[0])/10)
        szy = int((shpextent[3] - shpextent[2])/10)
        
        app1 = otbApplication.Registry.CreateApplication("Rasterization")
        app1.SetParameterString("in", labelshpfile)
        app1.SetParameterString("out", labeltifout)
        app1.SetParameterInt("szx", szx)
        app1.SetParameterInt("szy", szy)
        app1.SetParameterInt("epsg", 2154)
        app1.SetParameterFloat("orx", shpextent[0])
        app1.SetParameterFloat("ory", shpextent[3])
        app1.SetParameterFloat("spx", 10.)
        app1.SetParameterFloat("spy", -10.)
        app1.SetParameterString("mode", "attribute")
        app1.SetParameterString("mode.attribute.field", labelsfield)
        app1.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint32)
        app1.ExecuteAndWriteOutput()

        if not os.path.exists(labeltifout):
            logger.error(f"File not found : {labeltifout}")
            raise 
        
        return TiledRGP(pub_year, labeltifout, vectorpath=labelshpfile)

class CopernicusLanduse(Raster):

    def __init__(self, rasterpath:str,zonename:str):
        super().__init__(rasterpath)
        self.zonename = zonename

    @classmethod
    def create_from_clip(cls, params:dict, s2tilename:str=None, aoivector:'AOIvector'=None) -> 'CopernicusLanduse':
        
        globCoperRaster = Raster(params['landusemap'])
        globCoperGDALRaster = globCoperRaster.get_gdalraster()

        if s2tilename != None and aoivector == None:
            zonename = s2tilename
        elif s2tilename == None and aoivector != None:
            zonename = aoivector.get_zonename()
        else:
            raise Exception("s2tilename and aoivector are mutualy exclusive")

        outclip = os.path.join(params['processrgpdir'],"COPERLU_"+zonename+".TIF")

        if os.path.exists(outclip) and not params['overwrite']:
            return CopernicusLanduse(outclip,zonename)
    
        if s2tilename != None and aoivector == None:
            
            qtiles = mod.TileEnvelope.objects.filter(tile__exact = s2tilename)
            if len(qtiles) != 1 :
                logger.error(f"Database request for tile {s2tilename} failed.")
                raise 

            tileclip = qtiles[0].geom

            if globCoperGDALRaster.srid != 4326:
                tileclipCoper = tileclip.transform(globCoperGDALRaster.srs,clone=True)
                ogrtileclip = OGRGeometry(tileclipCoper.wkb)
            else:
                ogrtileclip = OGRGeometry(tileclip.wkb)
            
            if not globCoperRaster.enveloppe.contains(ogrtileclip):
                raise Exception(f"Geometry error : Tile {s2tilename} is not contained inside Agriref raster {params['landusemap']}")

            xmin,ymin,xmax,ymax = tileclip.extent

        elif s2tilename == None and aoivector != None:
            
            aoigeom = aoivector.get_geom(4326)
            if not globCoperRaster.enveloppe_wgs84.contains(aoigeom):
                raise Exception(f"Geometry error : AOI file {os.path.basename(aoivector.get_filepath())} is not contained inside Agriref raster {params['landusemap']}")
            
            xmin,ymin,xmax,ymax = aoigeom.extent

        # with rasterio.open(params['landusemap']) as ds :
        #     rowmin, colmin = ds.index(xmin, ymax)
        #     rowmax, colmax = ds.index(xmax, ymin)

        # trfm = globCoperGDALRaster.geotransform

        # colminymin = -(trfm[2]*(trfm[3]-ymin)+trfm[5]*xmin-trfm[0]*trfm[5])
        # colminymax = -(trfm[2]*(trfm[3]-ymax)+trfm[5]*xmin-trfm[0]*trfm[5])
        # colmin = round(min(colminymin,colminymax));
        # rowminxmin =  round((trfm[1]*(trfm[3]-ymin)+trfm[4]*xmin-trfm[0]*trfm[4]));
        # rowminxmax =  round((trfm[1]*(trfm[3]-ymin)+trfm[4]*xmax-trfm[0]*trfm[4]));
        # rowmin =  round(min(rowminxmin,rowminxmax));
        # colmaxymin = -(trfm[2]*(trfm[3]-ymin)+trfm[5]*xmax-trfm[0]*trfm[5])
        # colmaxymax = -(trfm[2]*(trfm[3]-ymax)+trfm[5]*xmax-trfm[0]*trfm[5])
        # colmax = round(max(colmaxymin,colmaxymax));
        # rowmaxxmin =  round((trfm[1]*(trfm[3]-ymax)+trfm[4]*xmin-trfm[0]*trfm[4]));
        # rowmaxxmax =  round((trfm[1]*(trfm[3]-ymax)+trfm[4]*xmax-trfm[0]*trfm[4]));
        # rowmax =  round(max(rowmaxxmin,rowmaxxmax));

        # logger.debug(f"geotransform = {trfm}")
        # logger.debug(f"colminymin = {colminymin} / colminymax = {colminymax} ")
        # logger.debug(f"rowminxmin = {rowminxmin} / rowminxmax = {rowminxmax} ")
        # logger.debug(f"xmin = {xmin} / ymin = {ymin} ")
        # logger.debug(f"xmax = {xmax} / ymax = {ymax} ")

        # logger.debug(f"rowmin = {rowmin} / colmin = {colmin} ")
        # logger.debug(f"rowmax = {rowmax} / colmax = {colmax} ")

        # app1 = otbApplication.Registry.CreateApplication("ExtractROI")
        # app1.SetParameterString("in", params['landusemap'])
        # app1.SetParameterString("out", outclip)
        # app1.SetParameterString("mode", "extent")
        # #en mode lonlat ou phy : ulx -> llx, uly -> lly, lrx -> urx, lry -> ury
        # app1.SetParameterFloat("mode.extent.ulx", colmin)
        # app1.SetParameterFloat("mode.extent.uly", rowmin)
        # app1.SetParameterFloat("mode.extent.lrx", colmax)
        # app1.SetParameterFloat("mode.extent.lry", rowmax)
        # app1.SetParameterString("mode.extent.unit", "pxl")
        # app1.ExecuteAndWriteOutput()

        app1 = otbApplication.Registry.CreateApplication("ExtractROI")
        app1.SetParameterString("in", params['landusemap'])
        app1.SetParameterString("out", outclip)
        app1.SetParameterString("mode", "extent")
        #en mode lonlat ou phy : ulx -> llx, uly -> lly, lrx -> urx, lry -> ury
        app1.SetParameterFloat("mode.extent.ulx", xmin)
        app1.SetParameterFloat("mode.extent.uly", ymin)
        app1.SetParameterFloat("mode.extent.lrx", xmax)
        app1.SetParameterFloat("mode.extent.lry", ymax)
        app1.SetParameterString("mode.extent.unit", "lonlat")
        app1.ExecuteAndWriteOutput()

        # elif s2tilename == None and aoivector != None:

        #     app1 = otbApplication.Registry.CreateApplication("ExtractROI")
        #     app1.SetParameterString("in", params['landusemap'])
        #     app1.SetParameterString("out", outclip)
        #     app1.SetParameterString("mode", "fit")
        #     #en mode lonlat ou phy : ulx -> llx, uly -> lly, lrx -> urx, lry -> ury
        #     app1.SetParameterString("mode.fit.vect", aoivector.get_filepath())
            
        #     app1.ExecuteAndWriteOutput()

        return CopernicusLanduse(outclip,zonename)

        
    def reprojectRaster(self,epsg:int, res=None, resampling=None):    
        reproj = Raster(self.filepath)
        reproj.reprojectRaster(epsg, res=res, resampling=resampling)
        self.__init__(self.filepath,self.zonename)

    def create_labelRaster(self, agrivalues:str, overwrite:bool=False)->Raster:
        outdir = os.path.dirname(self.filepath)
        outlabel = os.path.join(outdir,"COPERLU_LABELS_"+self.zonename+".TIF")

        if os.path.exists(outlabel) and not overwrite:
            return Raster(outlabel)

        gdalself = self.get_gdalraster()
        xres = gdalself.geotransform[1]
        if gdalself.srs.geographic :
            if xres < 0.00042 :
                self.reprojectRaster(3857, res=100, resampling="mode")
            else:
                self.reprojectRaster(3857)
        else:
            if xres < 50 :
                self.reprojectRaster(3857, res=100, resampling="mode")
        
        gdalself = self.get_gdalraster()

        selfwidth = gdalself.width

        scalex = abs(gdalself.scale.x/10.)
        scaley = abs(gdalself.scale.y/10.)

        logger.debug(f"in pixel x={str(gdalself.scale.x)} y={str(gdalself.scale.y)}")
        logger.debug(f"Using scale x={str(scalex)} y={str(scaley)}")

        condexpr = "im1b1=="+agrivalues.replace(" "," or im1b1==")
        
        app1 = otbApplication.Registry.CreateApplication("BandMathX")
        app1.SetParameterStringList("il", [self.filepath])
        app1.SetParameterString("out", outlabel)
        app1.SetParameterString("exp", condexpr+"?(idxX+1)+(idxY*"+str(selfwidth)+"):0")
        app1.Execute()

        app = otbApplication.Registry.CreateApplication("RigidTransformResample")
        app.SetParameterInputImage("in", app1.GetParameterOutputImage("out"))
        app.SetParameterString("out", outlabel)
        app.SetParameterString("transform.type","id")
        app.SetParameterFloat("transform.type.id.scalex", scalex)
        app.SetParameterFloat("transform.type.id.scaley", scaley)
        app.SetParameterString("interpolator","nn") 
        app.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint32)

        app.ExecuteAndWriteOutput()
        
        return Raster(outlabel)



class AOIvector():

    AERAMAX = 5000000000

    def __init__(self, path:str) -> None:
        try:
            ds = DataSource(path)
        except Exception:
            raise
        
        if ds.layer_count != 1:
            raise Exception("Vector file must have only 1 layer.")
        
        layer = ds[0]
        
        if layer.num_feat !=1 :
            raise Exception("Vector file must have only 1 polygon.")

        aoigeom = layer[0].geom
        self.aoigeom = aoigeom
        aoisrs = aoigeom.srs

        #geojson always in WGS84
        if aoisrs == None :
            aoisrs = SpatialReference(4326)
        
        if aoisrs.geographic :
            projforarea = SpatialReference(3857)
            ct = CoordTransform(aoisrs, projforarea)
            aoiproj = aoigeom.transform(ct, clone=True)
            aoiarea = aoiproj.area 
        else:
            aoiarea = aoigeom.area 

        if aoiarea > self.AERAMAX :
            raise Exception("AOI polygon area must be under 5000km2 (half S2Tile area). Please use S2Tile mode instead.")

        self.path = path
        if not aoisrs.geographic :
            projforarea = SpatialReference(4326)
            ct = CoordTransform(aoisrs, projforarea)
            aoiwgs84 = aoigeom.transform(ct, clone=True)
        else:
            aoiwgs84 = aoigeom
        #Create zone for nomenclature from centroid coords
        c = aoiwgs84.centroid
        if c.x >= 0 :
            hemx="W"
        else:
            hemx="E"
        if c.y >= 0 :
            hemy="N"
        else:
            hemy="S"
        lat = hemy + "{:04}".format(int(abs(c.y*100)))
        lon = hemx + "{:05}".format(int(abs(c.x*100)))
        self.zonename = lat+lon

    @classmethod
    def create_from_extent(cls, vectorfile:str)->'AOIvector':
        try:
            ds = DataSource(vectorfile)
        except Exception:
            raise
        
        inlayer = ds[0]

        vsplit = os.path.basename(vectorfile).split(".")
        name = vsplit[0]+"_BBOX"
        outshp = os.path.join(os.path.dirname(vectorfile),name+".shp")
        
        driver = ogr.GetDriverByName("ESRI Shapefile")
            
        # create the data source
        data_source = driver.CreateDataSource(outshp)
        
        # create the spatial reference, WGS84
        srs = osr.SpatialReference()
        srs.ImportFromEPSG(inlayer.srs.srid)
        
        # create the layer
        layer = data_source.CreateLayer(name, srs, ogr.wkbPolygon)
         
        # create the feature
        feature = ogr.Feature(layer.GetLayerDefn())
        ogrpoly =  ogr.CreateGeometryFromWkt(inlayer.extent.wkt)
        feature.SetGeometry(ogrpoly)
        # Create the feature in the layer (shapefile)
        layer.CreateFeature(feature)
        feature = None
        data_source = None
        ds = None

        if not os.path.exists(outshp):
            raise Exception(f"Failed creating file : {outshp}")

        return AOIvector(outshp)
                                

    def get_intersected_s2tiles(self)-> List[str]:
        
        qs_tiles = mod.TileEnvelope.objects.filter(geom__intersects=self.aoigeom)
        numtiles = len(qs_tiles) 
        if numtiles == 0:
            raise Exception("Intersection between AOI and S2Tile is null")

        tilelist = []
        for t in qs_tiles:
            tilelist.append(t.tile)

        return tilelist

    def get_geom(self,epsg:int)-> OGRGeometry:
        if self.aoigeom.srid != epsg :
            targetsrs = SpatialReference(epsg)
            ct = CoordTransform(self.aoigeom.srs, targetsrs)
            return self.aoigeom.transform(ct,clone=True)
        else:
            return self.aoigeom

    def get_filepath(self)->str:
        return self.path

    def get_zonename(self)->str:
        return self.zonename

    




        


