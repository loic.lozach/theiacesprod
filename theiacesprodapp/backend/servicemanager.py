import os, sys
from datetime import datetime, timedelta
from theiacesprodapp.backend import utils
from theiacesprodapp.backend.data import Sentinel2Zip, TiledRGP, Raster, AOIvector, CopernicusLanduse, Sentinel2PCStac
from theiacesprodapp.backend.iodb import AddParcelEvents, AddParcels, AddSentinel1Images
from theiacesprodapp.backend.service.calibration import OrthorectifySentinel1
from theiacesprodapp.backend.service.download import ClimateDataSystem, Microsoft_Stac_Catalog, SentinelEodag, Dataspace_Copernicus_Catalog
from theiacesprodapp.backend.service.ssmproduction import SSMProduction, SSMConverter
from theiacesprodapp.backend.service.preprocess import FreezeStatsGeneration, ndvi_pipeline, gapfilling_pipeline, processrgp_pipeline, gpm_pipeline
from theiacesprodapp.backend.service.freezedetection import FreezeDetect
import logging 
from django.conf import settings

logger = logging.getLogger(__name__)

WORK_DIR='/data'
# class ArgsWrapper:
    
#     #'exmosS1'
#     indir=""
#     inref=""
#     zone=""
#     polar=""
#     incid=""
#     dem=True
#     outdir=""
#     overwrite=False

#     #'ndvi'
#     s2l2dir=""
#     format="" #choices=['S2-MUSCATE','S2-SEN2COR','S2-L3A','L8-OLI/TIRS']
#     ndvidir=""
    
#     #'gapfilling'
#     ndvidir=""
#     outmaskdir=""
#     interpolation=""
#     outdir=""
    
#     # RGP
#     #'processrgp'
#     rgpshp=""
#     labelsfield="ID_PARCEL"
#     inref=""
#     zone=""
#     outdir=""
    
    
#     # Stack and mask before segm pipeline
#     #'stack'
#     ndvidir=""
#     lulc=""
#     agrivalues=""
#     output=""
    
#     # Extract SRTM1SecHGT from Snap download and compute slope
#     #'slope'
#     srtmdir=""
#     inref=""
#     zone=""
#     demtype="" #choices=['srtmhgt1sec', 'srtm3sec']
#     deletedem=""
#     outdir=""
    
#     # Select wet or dry model for inversion from GPM data
#     #WARNING: adresse gpm nasa a modifier (voir mail actu gpm) 
#     #'gpm'
#     sardir=""
#     sardirtype=""
#     sarmode=""
#     gpmdir=""
#     zone=""
#     outtxt=""
#     resampling=True


class SSMManager():
    def __init__(self,params:dict):
        
        if params["debug"] :
            logger.setLevel(logging.DEBUG)
            settings.DEBUG = True
        else:
            settings.DEBUG = False
            

        logger.debug("Initiating SoilMoisture Service Manager")
        self.parametres={
            #attribut pipeline
                "agriref":"",
                "zone":"",
                "aoifile":"",
                "landusemap":"",
                "skipdownloads":False,
            #attribut download
                "mrgstile":"", #define workdir on user zone
                "startdate":"",
                "enddate":"", #0=S1-GRD,1=S2-L2A,2=S2-L3A
            #attribut calibration
                "sentinel1_L1A_zip_dir" : None,
                "reference_image_file" : None,
                #"geographic_ref_tag" : None, == mrgstile
                "s1_ortho_dir" : None,
                "sentinel1_polar_mode" : 'vv', #choices=['vv', 'vh', 'vvvh']
                "sentinel1_incidence_type" : 'loc', # ou 'elli'
                "urlsrmt":"http://step.esa.int/auxdata/dem/SRTMGL1/",
                "srtmhgtzip":"/data/SRTM/",
                "geoid":"/work/python/data/egm96.grd",
            #attribut ndvi
                's2l2zipdir':"",
                # 'format':"",
                'ndvidir':"",
            #attribut gapfilling
                #ndvidir=""
                'gapfmaskdir':"",
                'interpolation':"linear",
                'gapfndvidir':"",
            #attribut rpg
                'rpg_pub_year':"",
                "rgpshp":"",
                "labelsfield":"ID_PARCEL",
                "landusefield":"CODE_GROUP",
                "agrivalues": "1 2 3 4 5 6 7 8 9 14 15 16 17 18 19 20 21 22 23 25 26",
        #         "inref":"",
        #         "mrgstile":"",
                "processrgpdir":"",
            #attribut production ssm
                "tfmodel_dir": "",
                'tfmodel_choice':"dry", #["dry","wet"],
        #         's1_ortho_dir':"",                                                                 AND Sentinel-1 images processed by calibration_service.py or preprocess_service.py exmosS1')
        #         'ndvi_dir':"",
                'labels_file':"",
        #         'zone':"",
                'slope_file':"",
                'slope_value':20,
        #         's1_polar_mode':"vv", # choices=['vv', 'vh','vvvh'],
        #         's1_incid_mode':"loc", # choices=['loc','elli'],
                'outformat':"raster", #  choices=['raster', 'csv'],
                'data_output_dir':"",
                "csv_output_dir":"",
                "tif_output_dir":"",
                "skipdownloads":"no",
                "addtodb":None,
                'overwrite':False,
                "ssmfootprint_shpname":"",
                "debug":False
                
                }
        
        prodtypenum=["S1-GRD","S2-L2A","S2-L3A"]

        for key in params:
            self.parametres[key] = params[key]

        
        logger.debug(self.parametres)

        if self.parametres["agriref"] == "vector" and self.parametres["zone"] == None:
            self.aoivec = AOIvector.create_from_extent(self.parametres["rgpshp"])
            params['mrgstile'] = self.aoivec.get_zonename()
        elif self.parametres["agriref"] == "raster" and self.parametres["zone"] == "aoi":
            self.aoivec = AOIvector(self.parametres["aoifile"])
            params['mrgstile'] = self.aoivec.get_zonename()

        logger.info(f"Using zone naming : {params['mrgstile'] }")

        self.parametres["mrgstile"] = params["mrgstile"]
        self.parametres["startdate"] = params["startdate"]
        self.parametres["enddate"] = params["enddate"]
        startdate = datetime.strptime(params["startdate"], "%Y-%m-%d")
        enddate = datetime.strptime(params["enddate"], "%Y-%m-%d")
        if enddate <= startdate :
            logger.error("Start date and end date are inverted !")
            raise Exception("Start date and end date are inverted !")
        # self.parametres["rpg_pub_year"] = params["rpg_pub_year"]
        self.parametres["rgpshp"] = params["rgpshp"]
        self.parametres["agrivalues"] = params["agrivalues"]
        self.parametres["tfmodel_dir"] = params["tfmodel_dir"]
        
        projectname = "_".join([self.parametres["mrgstile"].upper(),
                                self.parametres["startdate"],
                                 self.parametres["enddate"]
                                ])
        projectdir = os.path.join(WORK_DIR , projectname)
        
        self.parametres["sentinel1_L1A_zip_dir"] = os.path.join(projectdir,"S1_DOWN")
        self.parametres["s1_ortho_dir"] = os.path.join(projectdir,"S1_ORTHO")
        self.parametres["s2l2zipdir"] = os.path.join(projectdir,"S2_DOWN")
        self.parametres["ndvidir"] = os.path.join(projectdir,"NDVI")
        self.parametres["gapfndvidir"] = os.path.join(projectdir,"NDVI_GAPF")
        self.parametres["gapfmaskdir"] = os.path.join(self.parametres["gapfndvidir"],"MASK")
        self.parametres["processrgpdir"] = os.path.join(projectdir,"RPG")
        self.parametres["data_output_dir"] = os.path.join(projectdir,"SSM")
        self.parametres["csv_output_dir"] = os.path.join(self.parametres["data_output_dir"] ,"CSV")
        self.parametres["tif_output_dir"] = os.path.join(self.parametres["data_output_dir"] ,"TIF")
        
        try:
            logger.info(f"""#Preparing working dir : {projectdir}
                            -downloaded S1 : {self.parametres["sentinel1_L1A_zip_dir"]}
                            -downloaded S2 : {self.parametres["s2l2zipdir"]}
                            -processed landuse : {self.parametres["processrgpdir"]}
                            -processed NDVI : {self.parametres["ndvidir"]}
                            -processed gapfilled NDVI : {self.parametres["gapfndvidir"]}
                            -processed S1 : {self.parametres["s1_ortho_dir"]}
                            -processed SSM : {self.parametres["data_output_dir"]}
                            """)
            if not os.path.exists(projectdir):
                os.makedirs(projectdir)
            if not os.path.exists(self.parametres["sentinel1_L1A_zip_dir"]):
                os.makedirs(self.parametres["sentinel1_L1A_zip_dir"])
            if not os.path.exists(self.parametres["s1_ortho_dir"]):
                os.makedirs(self.parametres["s1_ortho_dir"])
            if not os.path.exists(self.parametres["s2l2zipdir"]):
                os.makedirs(self.parametres["s2l2zipdir"])
            if not os.path.exists(self.parametres["ndvidir"]):
                os.makedirs(self.parametres["ndvidir"])
            if not os.path.exists(self.parametres["gapfndvidir"]):
                os.makedirs(self.parametres["gapfndvidir"])
            if not os.path.exists(self.parametres["gapfmaskdir"]):
                os.makedirs(self.parametres["gapfmaskdir"])
            if not os.path.exists(self.parametres["processrgpdir"]):
                os.makedirs(self.parametres["processrgpdir"])
            if not os.path.exists(self.parametres["csv_output_dir"]) and self.parametres["outformat"] == "csv":
                os.makedirs(self.parametres["csv_output_dir"])
            if not os.path.exists(self.parametres["tif_output_dir"]) and self.parametres["outformat"] == "raster" :
                os.makedirs(self.parametres["tif_output_dir"])
        except Exception as err :
            logger.error("Folder creation failed.")
            raise 

    def autoprodssmww(self):
        logger.info(f"""### 
                Auto SoilMoisture Production for {self.parametres['mrgstile']} 
                from {self.parametres['startdate']} to {self.parametres['enddate']} 
                ###""")

        self.prepare_reference()

        if self.parametres["skipdownloads"] == 'all':

            logging.info("Skipping Sentinel downloads...")
            
        else:
            #s2level=1 L2-L2A, =2 L2L3A
            self.sentinel_download(1)
        
        self.process_ndvi()
        self.process_ssm()

    def autoprodssmfr(self):
        logger.info(f"""### 
                Auto SoilMoisture Production over France for {self.parametres['mrgstile']} 
                from {self.parametres['startdate']} to {self.parametres['enddate']} 
                ###""")

        self.prepare_reference()

        if self.parametres["skipdownloads"] == 'all':

            logging.info("Skipping Sentinel downloads...")
            
        else:
            self.sentinel_download(2)
        
        self.process_ndvi()
        self.process_ssm()


    def prepare_reference(self):

        
    # Agrivect or CopernicusLanduse
        if self.parametres["agriref"] == "vector":
            logger.info(f"""#Creating labels raster reference from :
                                {self.parametres['rgpshp']}""")

            filteredlabels_raster = Raster.create_filtlabels_from_vector(self.parametres)
            
            

        elif self.parametres["agriref"] == "raster":
            logger.info(f"""#Creating labels raster reference from :
                                {self.parametres['landusemap']}""")
            # S2 tile or none
            if self.parametres["zone"] == "aoi":
                
                logger.info("Using AOI file: "+self.parametres["aoifile"])

                reflulc = CopernicusLanduse.create_from_clip(self.parametres, aoivector=self.aoivec)

            elif self.parametres["zone"] == "s2tile":

                logger.info("Clipping Landuse on tile : "+self.parametres["mrgstile"])
                reflulc = CopernicusLanduse.create_from_clip(self.parametres, s2tilename=self.parametres['mrgstile'])
                
            else:
                raise Exception("Incorrect zone parameter")
            
            # self.parametres['landusemap'] = reflulc.filepath

            logger.info("Creating labels file from clip : "+reflulc.filepath)

            filteredlabels_raster = reflulc.create_labelRaster(self.parametres['agrivalues'], overwrite=self.parametres['overwrite'])

        
        else:
            raise Exception("Incorrect zone parameter")

        self.parametres['labels_file'] = filteredlabels_raster.filepath
        self.parametres["reference_image_file"] = filteredlabels_raster.filepath

        logger.info(f"Labels raster has been created into {filteredlabels_raster.filepath}")


    def sentinel_download(self, s2level:int):
        #download & process NDVI
        if self.parametres["skipdownloads"] == "s1" :
            logging.info("Skipping Sentinel 1 downloads...")
            S1filelist = utils.search_files(directory=self.parametres['sentinel1_L1A_zip_dir'],resolution="S1",extension="zip")

        else:
            if self.parametres["zone"] == "s2tile":
                #download S1 from Tile, start date and end date
                tilegeom = Dataspace_Copernicus_Catalog.get_wkt_tile(self.parametres['mrgstile'])
                s1wktsearch = tilegeom.wkt
            else:
                refraster = Raster(self.parametres['reference_image_file'])
                s1wktsearch = refraster.enveloppe_wgs84.wkt
            
            logger.info(f"#Starting S1 download into {self.parametres['sentinel1_L1A_zip_dir'] }")
            dataspace = Dataspace_Copernicus_Catalog("SENTINEL-1",self.parametres["startdate"], self.parametres["enddate"], 
                                                     s1wktsearch, None,self.parametres['sentinel1_L1A_zip_dir'])
            dataspace.search_dataspace_odata()
            S1filelist = dataspace.download_data()

        if len(S1filelist) == 0:
            logger.error(f"Failed to find/download Sentinel1 images in {self.parametres['sentinel1_L1A_zip_dir']}")
            logger.info("Exiting")
            sys.exit()
        
        if self.parametres["skipdownloads"] == "s2" :
            logging.info("Skipping Sentinel 2 downloads...")
            if self.parametres["zone"] == "s2tile":
                S2filelist = utils.search_files(directory=self.parametres['s2l2zipdir'],resolution="S2",extension="zip")
            else:
                S2filelist = utils.search_files(directory=self.parametres['s2l2zipdir'],resolution="S2",extension="",fictype="d")
        
        else:
            #download S2-L2A from Tile and date +- 1 mois    
            bufferdates = utils.get_S2L3Adates_from_S1dates(self.parametres["startdate"],self.parametres["enddate"])
            
            if self.parametres["zone"] == "s2tile":
                if s2level == 1 :
                    logger.info(f"#Starting S2L2A download into {self.parametres['s2l2zipdir'] }")
                    s2wktsearch = tilegeom.centroid.wkt    
                    dataspace = Dataspace_Copernicus_Catalog("SENTINEL-2",bufferdates[0], bufferdates[1], 
                                                        s2wktsearch, self.parametres['mrgstile'], self.parametres['s2l2zipdir'])
                    dataspace.search_dataspace_odata()
                    S2filelist = dataspace.download_data()
                else:
                    logger.info(f"#Starting S2L3A download into {self.parametres['s2l2zipdir'] }")
                    eod = SentinelEodag()
                    S2filelist = eod.search_and_download_from_tile(s2level, self.parametres['mrgstile'], 
                                            bufferdates[0], bufferdates[1], 
                                            self.parametres['s2l2zipdir'] )

            else:
                resetStac=True
                downproc = Microsoft_Stac_Catalog("sentinel-2-l2a",self.aoivec,("/").join(bufferdates),self.parametres["s2l2zipdir"])
                downproc.search()
                while(resetStac):
                    resetStac, S2filelist = downproc.download()

        if len(S2filelist) == 0:
            logger.error(f"Failed to find/download Sentinel2 images in {self.parametres['s2l2zipdir']}")
            logger.info("Exiting")
            sys.exit()
            
        # eod = SentinelEodag()
        # if self.parametres["zone"] == "s2tile":
        #     #download S1 from Tile, start date and end date
        #     tilegeom = Dataspace_Copernicus_Catalog.get_wkt_tile(self.parametres['mrgstile'])
        #     s1wktsearch = tilegeom.wkt
            
            
            
            
            
        #     if self.parametres["skipdownloads"] == "s1" :
        #         logging.info("Skipping Sentinel 1 downloads...")
        #         S1filelist = utils.search_files(directory=self.parametres['sentinel1_L1A_zip_dir'],resolution="S1",extension="zip")
        #     else:
        #         logger.info(f"#Starting S1 download into {self.parametres['sentinel1_L1A_zip_dir'] }")
        #         S1filelist = eod.search_and_download_from_tile(0, self.parametres['mrgstile'], 
        #                                     self.parametres["startdate"], self.parametres["enddate"], 
        #                                     self.parametres['sentinel1_L1A_zip_dir'] )
        #     if len(S1filelist) == 0:
        #         logger.info("Failed to download Sentinel1 images. You should verify your parameters.")
        #         logger.info("Exiting")
        #         return 1

        #     if self.parametres["skipdownloads"] == "s2" :
        #         logging.info("Skipping Sentinel 2 downloads...")
        #         S2filelist = utils.search_files(directory=self.parametres['s2l2zipdir'],resolution="S2",extension="zip")
        #     else:
        #         logger.info(f"#Starting S2 download into {self.parametres['s2l2zipdir'] }")
        #         #download S2-L2A from Tile and date +- 1 mois
        #         bufferdates = utils.get_S2L3Adates_from_S1dates(self.parametres["startdate"],self.parametres["enddate"])
        #         S2filelist = eod.search_and_download_from_tile(s2level, self.parametres['mrgstile'], 
        #                                     bufferdates[0], bufferdates[1], 
        #                                     self.parametres['s2l2zipdir'] )
            
        #     if len(S2filelist) == 0:
        #         logger.info("Failed to download Sentinel2 images. You should verify your parameters.")
        #         logger.info("Exiting")
        #         return 1

        # else:
        #     if self.parametres["skipdownloads"] == "s1" :
        #         logging.info("Skipping Sentinel 1 downloads...")
        #         S1filelist = utils.search_files(directory=self.parametres['sentinel1_L1A_zip_dir'],resolution="S1",extension="zip")
        #     else:
        #         refraster = Raster(self.parametres['reference_image_file'])
        #         refenv = refraster.enveloppe_wgs84
        #         #download S1 from Tile, start date and end date
        #         logger.info(f"#Starting S1 download into {self.parametres['sentinel1_L1A_zip_dir'] }")
        #         S1filelist = eod.search_and_download_from_wkt(0, self.parametres['mrgstile'], refenv.wkt,
        #                                     self.parametres["startdate"], self.parametres["enddate"], 
        #                                     self.parametres['sentinel1_L1A_zip_dir'] )
        #     if len(S1filelist) == 0:
        #         logger.info("Failed to download Sentinel1 images. You should verify your parameters.")
        #         logger.info("Exiting")
        #         return 1

        #     if self.parametres["skipdownloads"] == "s2" :
        #         logging.info("Skipping Sentinel 2 downloads...")
        #         s2sdirlist = utils.search_files(directory=self.parametres['s2l2zipdir'],resolution="S2",extension="",fictype="d")
        #         s2pcstaclist = []
        #         for d in s2sdirlist:
        #             s2pcstaclist.append(Sentinel2PCStac(d))
        #     else:
        #         logger.info(f"#Starting S2 download into {self.parametres['s2l2zipdir'] }")
        #         #download S2-L2A from Tile and date +- 1 mois
        #         bufferdates = utils.get_S2L3Adates_from_S1dates(self.parametres["startdate"],self.parametres["enddate"])
        #         resetStac=True
        #         downproc = Microsoft_Stac_Catalog("sentinel-2-l2a",self.aoivec,("/").join(bufferdates),self.parametres["s2l2zipdir"])
        #         downproc.search()
        #         while(resetStac):
        #             resetStac, s2pcstaclist = downproc.download()

        #     if len(s2pcstaclist) == 0:
        #         logger.info("Failed to download Sentinel2 images. You should verify your parameters.")
        #         logger.info("Exiting")
        #         return 1

    def process_ndvi(self):
        if self.parametres["zone"] != "s2tile":
            # self.parametres["format"] = "S2-MUSCATE"
            #process ndvi + gapfilling
            logger.info(f"""#Starting NDVI Production into :
                                    {self.parametres['ndvidir']}""")
            ndviproc = ndvi_pipeline(self.parametres)
            ndvi_rasterlist = ndviproc.process_pcstac()

        else:
            # self.parametres["format"] = "S2-L3A"
            logger.info(f"""Starting NDVI Production into :
                                    {self.parametres['ndvidir']}""")
            p = ndvi_pipeline(self.parametres)
            ndvi_rasterlist = p.process()
            #TODO test existance ndvi

        #process gapfilling
        logger.info(f"#Gapfilling NDVI Production into {self.parametres['gapfndvidir']}")
        p = gapfilling_pipeline(self.parametres)
        ndvi_rasterlist = p.process()
        #test existance gapf
        if len(ndvi_rasterlist) == 0:
            logger.error("Gapfilling return 0 ndvi file")
            raise Exception("Gapfilling_pipeline error")

    def process_ssm(self):
        
        #process S1 calibration
        logger.info(f"Starting S1 calibration and orthorectifiation into {self.parametres['s1_ortho_dir']}")
        p = OrthorectifySentinel1(self.parametres)
        s1ortho_rasterlist = p.start()
        self.parametres['slope_file'] = p.get_sloperaster().filepath
        

        if self.parametres['addtodb'] == None:
            self.parametres['addtodb'] = False
        
        if self.parametres['outformat'] == 'raster' and self.parametres['addtodb'] == False:
            #process SSM production as raster
            logger.info(f"Starting SSM production into {self.parametres['tif_output_dir']}")
            p = SSMProduction(self.parametres)
            ssmprod_filelist = p.start()

        elif self.parametres['outformat'] == 'csv' or self.parametres['addtodb'] == True:
            #process SSM production as csv
            logger.info(f"Starting SSM production into {self.parametres['csv_output_dir']}")
            p = SSMProduction(self.parametres)
            ssmprod_csvlist = p.start()

            #TODO SSM raster production
            p = SSMConverter(self.parametres["reference_image_file"], self.parametres['csv_output_dir'], self.parametres['tif_output_dir'])
            ssmprod_tiflist = p.start()

        if self.parametres['addtodb'] == True:
            #load parcels  in database
            logger.info(f"Updating DB with Agricultural parcels")
            labelrast = TiledRGP(self.parametres['rpg_pub_year'],self.parametres['labels_file'])
            p = AddParcels.loadByTile(labelrast)
            #load sentinel image in database
            logger.info(f"Updating DB with Sentinel image")
            p = AddSentinel1Images.loadByTile(self.parametres['mrgstile'],s1ortho_rasterlist,self.parametres['sentinel1_L1A_zip_dir'])
            #load SSM and input parameters in database
            logger.info(f"Updating DB with SSM csv")
            p = AddParcelEvents.loadSSMByTile(self.parametres['rpg_pub_year'],self.parametres['mrgstile'],ssmprod_csvlist)




class AutoProcessFreeze:
    
    
    def __init__(self,params:dict):
        logger.debug("Initiating AutoProcessFreeze")
        self.parametres={
            #load parcels rgp
                'globalRGPvectfile':"",
                'rpgclipdir':"",
                'mrgstile':"",
                'rpg_pub_year':"",
            #attribut download
                # "mrgstile":"", #define workdir
                "startdate":"",
                "enddate":"", #0=S1-GRD,1=S2-L2A,2=S2-L3A
            #attribut calibration
                "sentinel1_L1A_zip_dir" : None,
                "ERA5_dir" : None,
                #"geographic_ref_tag" : None, == mrgstile
                "s1_ortho_dir" : None,
                "sentinel1_polar_mode" : 'vh', #choices=['vv', 'vh', 'vvvh']
                "sentinel1_incidence_type" : 'loc', # ou 'elli'
                "urlsrmt":"http://step.esa.int/auxdata/dem/SRTMGL1/",
                "srtmhgtzip":"/data/SRTM/",
                "geoid":"/work/python/data/egm96.grd",
            #attribut freezeDetect
                "fconfig": {
                            1:[3.48,5.25],
                            2:[2.81,3.53],
                            3:[2.0,2.86],
                            },
                "lowtemp_threshold":276.15,
                "findmax":"closest",
                "orbit":"all",
                "wseason":"all",

        }

        self.parametres["rpg_pub_year"] = params["rpg_pub_year"]
        self.parametres["globalRGPvectfile"] = params["globalRGPvectfile"]
        self.parametres["mrgstile"] = params["mrgstile"]
        self.parametres["startdate"] = params["startdate"]
        self.parametres["enddate"] = params["enddate"]
        
        projectname = "_".join([self.parametres["mrgstile"].upper(),
                                self.parametres["startdate"],
                                 self.parametres["enddate"]
                                ])
        projectdir = os.path.join(WORK_DIR , projectname)
        
        self.parametres["sentinel1_L1A_zip_dir"] = os.path.join(projectdir,"S1_ZIP")
        self.parametres["ERA5_dir"] = os.path.join(projectdir,"ERA5")
        self.parametres["s1_ortho_dir"] = os.path.join(projectdir,"S1_ORTHO")
        self.parametres["s1_ortho_s40_dir"] = os.path.join(projectdir,"S1_ORTHO_S40")
        self.parametres["rpg_clip_dir"] = os.path.join(projectdir,"RPG_CLIP")
        self.parametres["gen_csv_dir"] = os.path.join(projectdir,"GEN_CSV")
    
    def start(self, params:dict):
        logger.info(f"""### 
            Auto Freeze Detection Production for {self.parametres['mrgstile']} 
            from {self.parametres['startdate']} to {self.parametres['enddate']} 
            ###""")

        #fill db with clip tiled RPG and create ref image
        tiledrpg = TiledRGP.create(self.parametres["globalRGPvectfile"] , self.parametres["mrgstile"], self.parametres["rpg_clip_dir"])
        AddParcels.loadByTile(tiledrpg)
        

        #download S1 from Tile, start date and end date
        downdir=self.parametres["sentinel1_L1A_zip_dir"] 
        logger.info(f"Starting S1 download into {downdir}")
        eod = SentinelEodag()
        eod.load_mrgstile()
        S1filelist = eod.search_and_download(0, self.parametres["mrgstile"], 
                                    self.parametres["startdate"], self.parametres["enddate"], 
                                    downdir)
        
        #process S1 calibration
        logger.info(f"Starting S1 calibration and orthorectifiation into {self.parametres['s1_ortho_dir']}")
        p = OrthorectifySentinel1(self.parametres, s1_file_list = S1filelist)
        p.start()

        #sigma40
        sigma40list = p.start_sigma40(self.parametres["s1_ortho_s40_dir"])

        #download ERA5
        cds = ClimateDataSystem(sigma40list,tiledrpg,self.parametres["ERA5_dir"])
        era5list = cds.start()

        #generate parcelevents csv
        gen = FreezeStatsGeneration(self.parametres)
        csvs = gen.gen_csv()

        #fill db with S1
        AddSentinel1Images.loadByTile(self.parametres["mrgstile"],sigma40list,downdir)

        #fill database with parcelevents
        AddParcelEvents.loadFreezeByTile(self.parametres["rpg_pub_year"],self.parametres["mrgstile"],csvs[0],csvs[1])

        #process freeze detection
        myfreeze = FreezeDetect()
        if params["fconfig"] != None:
            self.parametres["fconfig"] = params["fconfig"]
            myfreeze.set_seuils(params["fconfig"])
        if params["lowtemp_threshold"] != None:
            self.parametres["lowtemp_threshold"] = params["lowtemp_threshold"]
            myfreeze.set_lowtemp_threshold(params["lowtemp_threshold"])
        if params["findmax"] != None:
            self.parametres["findmax"] = params["findmax"]
        if params["orbit"] != None:
            self.parametres["orbit"] = params["orbit"]
        if params["wseason"] != None:
            self.parametres["wseason"] = params["wseason"]

        myfreeze.batch_process_detectable(self.parametres['mrgstile'],self.parametres['orbit'],self.parametres["findmax"], self.parametres["wseason"])
        
