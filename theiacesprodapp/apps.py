from django.apps import AppConfig


class TheiacesprodappConfig(AppConfig):
    name = 'theiacesprodapp'
