from theiacesprodapp.backend.utils import search_files
import os, datetime
import otbApplication

class GenerateCsvForDB:
    
    def gen_csv_otb(self, datelist, sarfiles,erafiles, ref, outsigmacsv, outtempcsv, eratypes):
        
        print("processing.......................................................")
        
        afa = otbApplication.Registry.CreateApplication("AgriFrozenAreas")
        s1apps=[]
        eraapps=[]
        manapps=[]
        bmxapps=[]
        i=0
        for sarfile in sarfiles:
            
            s1apps.append(otbApplication.Registry.CreateApplication("Superimpose"))
            s1apps[i].SetParameterString("inm",sarfile)
            s1apps[i].SetParameterString("out", str(i)+"temp.tif")
            s1apps[i].SetParameterString("interpolator","nn")
            s1apps[i].SetParameterString("inr",ref)
            s1apps[i].Execute()
            
    #       otbcli_ManageNoData -in /data/BRETAGNE_T30UVU/CDS/ERA5-2m_T30UVU_20200121T175644.grib 
    #        -out /data/BRETAGNE_T30UVU/CDS/ERA5-2m_T30UVU_20200121T175644_no.tif 
    #        -mode changevalue -mode.changevalue.newv  283 
            manapps.append(otbApplication.Registry.CreateApplication("ManageNoData"))
            manapps[i].SetParameterString("in",erafiles[i])
            manapps[i].SetParameterString("out", str(i+100)+"temp.tif")
            manapps[i].SetParameterString("mode","changevalue")
            manapps[i].SetParameterFloat("mode.changevalue.newv",283)
            manapps[i].Execute()
            
    #       otbcli_BandMathX -il /data/BRETAGNE_T30UVU/CDS/ERA5-2m_T30UVU_20200121T175644.grib 
    #        -out /data/BRETAGNE_T30UVU/CDS/ERA5-2m_T30UVU_20200121T175644_bmx.tif 
    #        -exp "(im1b1 == 283 ? im1b1 = mean(im1b1N5x5):im1b1)"
            bmxapps.append(otbApplication.Registry.CreateApplication("BandMathX"))
            bmxapps[i].AddImageToParameterInputImageList("il",manapps[i].GetParameterOutputImage("out"))
            bmxapps[i].SetParameterString("out", str(i+200)+"temp.tif")
            if eratypes[i] == "2m":
                bmxapps[i].SetParameterString("exp","(im1b1 == 283 ? im1b1 = mean(im1b1N5x5):im1b1)")
            elif eratypes[i] == "1000hpa":
                bmxapps[i].SetParameterString("exp","(im1b1 + 273.15)")
            else:
                print("ERA5 files type not recognize. exiting")
                exit()
                
            bmxapps[i].Execute()
            
            eraapps.append(otbApplication.Registry.CreateApplication("Superimpose"))
            eraapps[i].SetParameterInputImage("inm",bmxapps[i].GetParameterOutputImage("out"))
            eraapps[i].SetParameterString("out", str(i+300)+"temp.tif")
            eraapps[i].SetParameterString("interpolator","bco")
            eraapps[i].SetParameterString("inr",ref)
            eraapps[i].Execute()
            
            afa.AddImageToParameterInputImageList("ilsigma",s1apps[i].GetParameterOutputImage("out"))
            afa.AddImageToParameterInputImageList("iltemp",eraapps[i].GetParameterOutputImage("out"))
            i+=1
        
        afa.SetParameterStringList("datelist",datelist)
        afa.SetParameterString("inlabels",ref)
        afa.SetParameterString("out.sigma",outsigmacsv) 
        afa.SetParameterString("out.temp",outtempcsv) 
        afa.ExecuteAndWriteOutput()
        
        
            
    
    def gen_csv(self, rpglabels_img, calibrated_S1_dir, resampled_ERA5_dir,
                out_dir, verbose=False):
        
        if not os.path.exists(calibrated_S1_dir):
            print("Error: Directory does not exist "+calibrated_S1_dir)
        if not os.path.exists(resampled_ERA5_dir):
            print("Error: Directory does not exist "+resampled_ERA5_dir)
        if not os.path.exists(out_dir):
            os.mkdir(out_dir)
            
            
        s1calfiles = search_files(calibrated_S1_dir,resolution='S1', extension='TIF', fictype='f')
        erafiles = search_files(resampled_ERA5_dir,resolution='ERA', extension='grib', fictype='f')
        if len(erafiles) != len(s1calfiles):
            print("Error: number of sentinel1 files and era5 files doesn't match")
            print("Exiting.")
            exit()
        
        tile_str=None
        s1strdates=[]
        s1datetimes=[]
        erafilesord=[]
        eratypes=[]
        i=0
        for s1 in s1calfiles:
            strdate=os.path.basename(s1)[:-4].split("_")[4]
            s1strdates.append(strdate)
            s1datetimes.append(datetime.datetime.strptime(strdate, "%Y%m%dT%H%M%S"))
            erafound=False
            for era in erafiles:
                if strdate in era:
                    erafilesord.append(era)
                    if "-2m" in era:
                        eratypes.append("2m")
                    elif "-1000hpa" in era:
                        eratypes.append("1000hpa")
                    else:
                        print("ERA5 files type not recognize for "+era+". exiting")
                        exit()
                    erafound=True
                    break
            if not erafound:
                print("Error: Can't find matching date for era file "+strdate)
                print("Exiting.")
                exit()
            tile_name = os.path.basename(s1)[:-4].split("_")[3]
            if tile_str == None :
                tile_str = tile_name
            else:
                if tile_str != tile_name:
                    print("Error: wrong tile_str in image filename: "+s1)
                    print("Exiting.")
                    exit()
            i+=1
        
        mindate = min(s1datetimes)
        maxdate = max(s1datetimes)
        out_sigma40_csv = os.path.join(out_dir,"MEANSIGMA_"+tile_str+"_"+mindate.strftime("%Y%m%dT%H%M%S")
                                                                    +"_"+maxdate.strftime("%Y%m%dT%H%M%S")
                                                                    +".csv")
        out_temperature_csv = os.path.join(out_dir,"MEANTEMP_"+tile_str+"_"+mindate.strftime("%Y%m%dT%H%M%S")
                                                                    +"_"+maxdate.strftime("%Y%m%dT%H%M%S")
                                                                    +".csv")
        
        self.gen_csv_otb(s1strdates,s1calfiles,erafilesord, rpglabels_img, out_sigma40_csv, out_temperature_csv, eratypes)
        print("Done.")
        
        