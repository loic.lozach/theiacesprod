import logging, datetime, os, math
from scipy import stats
from theiacesprodapp import models as mod
import osgeo.ogr as ogr
import osgeo.osr as osr

logger = logging.getLogger(__name__)


class MeanSigmaMaxReference:
    
    _meanSigmaMaxValue = None
    _sigmaMaxPEList = []
    
    def __init__(self,sigmaMaxParcelEventList):
        self._sigmaMaxPEList = sigmaMaxParcelEventList
        sigmax=0
        for pem in self._sigmaMaxPEList:
            sigmax += pem.meansigma40
        
        self._meanSigmaMaxValue = sigmax/len(sigmaMaxParcelEventList)
        
        
    def get_mean_value(self):
        return self._meanSigmaMaxValue
    
    def get_parcelEvent_list(self):
        return self._sigmaMaxPEList
        

class FreezeDetect:
    
    fconfig={}
    
    def __init__(self):
        self.fconfig = {
            1:[3.48,5.25],
            2:[2.81,3.53],
            3:[2.0,2.86],
            "lowtemp_threshold":276.15,
            }
        
    def __str__(self):
        return str(self.fconfig)
        
    def set_seuils(self,fconfig):
        self.fconfig[1]=fconfig[1]
        self.fconfig[2]=fconfig[2]
        self.fconfig[3]=fconfig[3]
    
    def set_lowtemp_threshold(self,threshold):
        self.fconfig["lowtemp_threshold"]=threshold
    
    def get_pe_with_maxsigma_on_period(self, parcelle, startdate, enddate, orbit):
        first_pe = mod.ParcelEvent.objects.filter(parcelle__exact=parcelle, 
                                                  sentinel1__sentinel1_date__gt=startdate, 
                                                  sentinel1__sentinel1_date__lte=enddate,
                                                  sentinel1__sentinel1_orbit__exact=orbit,
                                                  meansigma40__isnull=False,
                                                  meantemp__isnull=False
                                                  ).order_by("-meansigma40").first()
        #.aggregate(Max('sentinel1__meansigma40'))
        
        if first_pe is None:
            logger.error("QuerySet on ParcelEvent #"+parcelle.id_parcel+" failed")
            return None
        
        return first_pe
    
    def get_percent90_sigma_reference(self, parcelle, startdate, enddate, orbit):
        qs_pes = mod.ParcelEvent.objects.filter(parcelle__exact=parcelle, 
                                                  sentinel1__sentinel1_date__gt=startdate, 
                                                  sentinel1__sentinel1_date__lte=enddate,
                                                  sentinel1__sentinel1_orbit__exact=orbit,
                                                  meansigma40__isnull=False,
                                                  meantemp__isnull=False
                                                  ).order_by("-meansigma40")
        #.aggregate(Max('sentinel1__meansigma40'))
        
        if len(qs_pes) == 0:
            logger.error("QuerySet on ParcelEvent #"+parcelle.id_parcel+" failed")
            return None
        
        index90 = int(len(qs_pes)*9/10)-1
        result=[qs_pes[index90]]
        return MeanSigmaMaxReference(result)
    
    def get_3max_allperiod_sigma_reference(self, parcelgraphique, d_last, orbit):
        l_pe3max = []
        d_last15 = d_last - datetime.timedelta(days=15)
        for i in range(3) :
            pemax = self.get_pe_with_maxsigma_on_period(parcelgraphique, d_last15, d_last, orbit)
            if pemax == None:
                return None
            l_pe3max.append(pemax)
            d_last = d_last15
            d_last15 = d_last - datetime.timedelta(days=15)
        
        
        return MeanSigmaMaxReference(l_pe3max)
    
    def get_3max_closest_sigma_reference(self, parcelgraphique, d_last, orbit):
        l_pe3max = []
        d_last15 = d_last - datetime.timedelta(days=15)
        while len(l_pe3max) < 3 :
            pemax = self.get_pe_with_maxsigma_on_period(parcelgraphique, d_last15, d_last, orbit)
            if pemax == None:
                return None
            l_pe3max.append(pemax)
            d_last = pemax.sentinel1.sentinel1_date - datetime.timedelta(hours=12)
            d_last15 = d_last - datetime.timedelta(days=15)
        
        
        return MeanSigmaMaxReference(l_pe3max)
    
    def get_2max_closest_sigma_reference(self, parcelgraphique, d_last, orbit):
        
        def f(parcelEvent):
            return parcelEvent.meansigma40
        
        l_pe3max = []
        d_last15 = d_last - datetime.timedelta(days=15)
        while len(l_pe3max) < 3 :
            pemax = self.get_pe_with_maxsigma_on_period(parcelgraphique, d_last15, d_last, orbit)
            if pemax == None:
                return None
            l_pe3max.append(pemax)
            d_last = pemax.sentinel1.sentinel1_date - datetime.timedelta(hours=12)
            d_last15 = d_last - datetime.timedelta(days=15)
        
        pe3max = max(l_pe3max,key=f)
        indmax = l_pe3max.index(pe3max )
        l_pe3max.pop(indmax)
        
        return MeanSigmaMaxReference(l_pe3max)
    
    def filter_parcels_by_tempMax(self, tile_str,fdetect_date):
        qs_s1bytiledate = mod.Sentinel1Image.objects.filter(tile__tile__exact=tile_str, 
                                                  sentinel1_date__exact=fdetect_date 
                                                  )
        if len(qs_s1bytiledate) != 1:
            logger.error("QuerySet on Sentinel1Image failed")
            exit()
        
        parcelles = qs_s1bytiledate[0].tile.parcelles.all()
        
        filt_parcelles = mod.ParcelEvent.objects.filter(parcelle__in=parcelles,
                                                    meantemp__lte=self.fconfig["lowtemp_threshold"],
                                                    sentinel1__exact=qs_s1bytiledate[0],
                                                    meansigma40__isnull=False,
                                                    meantemp__isnull=False
                                                    )
        if len(filt_parcelles) == 0:
            logger.info("No parcel with low temperature found for input date with "+qs_s1bytiledate[0].sentinel1_orbit+" orbit.")
            
#             ParcelEvent.objects.filter(parcelle__in=parcelles,
#                                        sentinel1__exact=qs_s1bytiledate[0],
#                                                     ).update(frozen_type=-1)
                                                    
            exit()
            
        return filt_parcelles
    
    def detect_frozenarea(self, tile_str,fdetect_date, findmax="closest"):
        startproc = datetime.datetime.now()
        #check image in db
        qs_s1date = mod.Sentinel1Image.objects.filter(tile__tile__exact=tile_str, 
                                                  sentinel1_date__exact=fdetect_date
                                                  )
        
        if len(qs_s1date)== 0 :
            print("Can't find Sentinel1 for date "+ fdetect_date.strftime("%Y%m%dT%H%M%S")+" and tile "+tile_str)
            exit()
        if len(qs_s1date) > 1 :
            print("Multiple Sentinel1 found for date "+ fdetect_date.strftime("%Y%m%dT%H%M%S")+" and tile "+tile_str)
            exit()
        
        d_last = qs_s1date[0].sentinel1_date
        d_start = d_last - datetime.timedelta(days=45)
        d_last15 = d_last - datetime.timedelta(days=15)
       
        print("Processing freeze detection for image date "+fdetect_date.strftime("%Y%m%dT%H%M%S")+
                " with "+qs_s1date[0].sentinel1_orbit + " orbit."
              )
        ##Test si au moins 1 image par semaine pdt 45j
        nbimg_45j = mod.Sentinel1Image.objects.filter(tile__tile__exact=tile_str, 
                                                  sentinel1_date__gt=d_start,
                                                  sentinel1_date__lte=d_last,
                                                  sentinel1_orbit__exact = qs_s1date[0].sentinel1_orbit
                                                  ).count()
        if nbimg_45j < 12:
            print("You need at least 12 S1 images with "+qs_s1date[0].sentinel1_orbit+" orbit over a period of 45 days. ")
            print("Launch download_service.py for dates from " +
                    d_start.strftime("%Y%m%dT%H%M%S") + " to " +
                    fdetect_date.strftime("%Y%m%dT%H%M%S") 
                  )
            exit()
        
        
        qs_parcelevents = self.filter_parcels_by_tempMax( tile_str,fdetect_date)

        ls_data = len(qs_parcelevents)
        print("Found "+str(ls_data)+" parcels with low temperature")
        cf0=0
        cf1=0
        cf2=0
        cfm1=0
        opbar=0

        for pe in qs_parcelevents:
            #DEBUG
            if findmax == "closest":

                meanSigmaMaxRef = self.get_3max_closest_sigma_reference(pe.parcelle, d_last, qs_s1date[0].sentinel1_orbit)

            elif findmax == "closest2max":
                meanSigmaMaxRef = self.get_2max_closest_sigma_reference(pe.parcelle, d_last, qs_s1date[0].sentinel1_orbit)

            elif findmax == "range":

                meanSigmaMaxRef = self.get_3max_allperiod_sigma_reference(pe.parcelle, d_last, qs_s1date[0].sentinel1_orbit)

            elif findmax == "p90":

                meanSigmaMaxRef = self.get_percent90_sigma_reference(pe.parcelle,d_start, d_last, qs_s1date[0].sentinel1_orbit)
            
            if meanSigmaMaxRef == None:
                logger.error("Can't find SigmaMaxRef. Passing parcel")
                continue
            
            pe.maxrefsigma40 = meanSigmaMaxRef.get_mean_value()
            if pe.meansigma40 <= 0 :
                pe.frozen_type = -1
                pe.save()
                cfm1+=1
                continue
            
            dbmaxref = 10*math.log10(meanSigmaMaxRef.get_mean_value())
            dbpe = 10*math.log10(pe.meansigma40)
            delta_sigma = dbmaxref - dbpe

            
            if delta_sigma < self.fconfig[pe.parcelle_type][0] :
                pe.frozen_type = 0
                pe.save()
                cf0+=1
            elif self.fconfig[pe.parcelle_type][0] < delta_sigma < self.fconfig[pe.parcelle_type][1] :
                pe.frozen_type = 1
                pe.save()
                cf1+=1
            elif delta_sigma > self.fconfig[pe.parcelle_type][1] :
                pe.frozen_type = 2
                pe.save()
                cf2+=1
                
            pbar = int((cf0+cf1+cf2+cfm1)/ls_data*100)
            if pbar % 5 == 0 and pbar > opbar:
                opbar = pbar
                periodproc = datetime.datetime.now() - startproc
                print("#########################################################")
                print(str(pbar)+" percent done ( "+str(cf0+cf1+cf2+cfm1)+" / "+str(ls_data)+" )")
                print("Elapse time : "+str(periodproc))
                print("Remaining   : "+str(periodproc/(pbar/100)-periodproc))
            
        print(str(cf0/ls_data*100)+"% parcels with no freeze detected")
        print(str(cf1/ls_data*100)+"% parcels with mid freeze detected")
        print(str(cf2/ls_data*100)+"% parcels with severe freeze detected")
        periodproc = datetime.datetime.now() - startproc
        print("Total elapse time : "+str(periodproc))  
    
    def detectable_dates(self,tile_str, orbit, wseason):
        if wseason == 'all':
            qs_s1dates = mod.Sentinel1Image.objects.filter(tile__tile__exact=tile_str,
                                                   sentinel1_orbit__exact = orbit,
                                                  ).order_by("sentinel1_date")
        else:
            start_date = datetime.date(wseason-1, 8, 31)
            end_date = datetime.date(wseason, 5, 1)
            qs_s1dates = mod.Sentinel1Image.objects.filter(tile__tile__exact=tile_str,
                                                       sentinel1_orbit__exact = orbit, 
                                                       sentinel1_date__range=(start_date, end_date)
                                                      ).order_by("sentinel1_date")
        if len(qs_s1dates)== 0 :
            print("No image available for tile "+tile_str+" and wseason "+str(wseason))
            exit()
        
        detectable_dates=[]
        first_date =  qs_s1dates[0].sentinel1_date
        decable_date = first_date + datetime.timedelta(days=45)
        last_date = qs_s1dates[len(qs_s1dates)-1].sentinel1_date
        
        qs_s1dates = mod.Sentinel1Image.objects.filter(tile__tile__exact=tile_str,
                                                   sentinel1_orbit__exact = orbit,
                                                   sentinel1_date__gt=decable_date 
                                                  ).order_by("sentinel1_date")  
        
        if len(qs_s1dates)== 0 :
            print("Not enough image for tile "+tile_str)
            exit()
        
        valids1=[]
        for s1 in qs_s1dates:
            
            m45d_date = s1.sentinel1_date - datetime.timedelta(days=45)
            nbimg_45j = mod.Sentinel1Image.objects.filter(tile__tile__exact=tile_str, 
                                                  sentinel1_date__gte=m45d_date,
                                                  sentinel1_date__lt=s1.sentinel1_date,
                                                  sentinel1_orbit__exact = orbit
                                                  ).count()
            if nbimg_45j < 12:
                continue
            
            nblowtempparc = mod.ParcelEvent.objects.filter(meantemp__lte=self.fconfig["lowtemp_threshold"],
                                                    sentinel1__exact=s1,
                                                    meansigma40__isnull=False,
                                                    meantemp__isnull=False
                                                    ).count()
                                                    
            print(s1.sentinel1_date.strftime("%Y%m%dT%H%M%S")+" is detectable with "+str(nblowtempparc)+" parcels with low temperature.")
            if nblowtempparc > 0:
                valids1.append(s1)
                
        return valids1
            
    def batch_process_detectable(self,tile_str, orbit, findmax, wseason):
        
        
        valids1 = self.detectable_dates(tile_str, orbit, wseason)
        
        for s1 in valids1:
            self.detect_frozenarea(tile_str, s1.sentinel1_date, findmax)
        
            
            
    def isfloat(self, value):
        try:
            float(value)
            return True
        except ValueError:
            return False        
                                
    def normalize_rsquare(self, tile_str, year, outdir=None):
        
        qs_pe = mod.ParcelEvent.objects.filter(sentinel1__tile__tile__exact=tile_str,
                                           sentinel1__sentinel1_date__month__in=[3,4],
                                           sentinel1__sentinel1_date__year=year,
                                               frozen_type__gte=1,
                                               parcelle_type__exact=1
                                           )
        if len(qs_pe)== 0 :
            print("Can't find frozen parcel in march-april "+str(year))
            exit()
        
        if outdir == None:
            exportregr = False
        else:
            exportregr = True
            
        exportregr_data=[]
        exportregr_header="parcelid;normalizingdate;day1;day2;day3;day4;day5;day6;sigmadb1;sigmadb2;sigmadb3;sigmadb4;sigmadb5;sigmadb6;r_square;"
        exportregr_data.append(exportregr_header)
        exporterr_header="parcelid;normalizingdate;nbdays;"
        exporterr=[]
        exporterr.append(exporterr_header)
        for pe in qs_pe :
            sartdate = pe.sentinel1.sentinel1_date - datetime.timedelta(days=30)
            qs_pesel = mod.ParcelEvent.objects.filter(sentinel1__tile__tile__exact=tile_str,
                                               sentinel1__sentinel1_date__lte=pe.sentinel1.sentinel1_date,
                                               sentinel1__sentinel1_date__gte=sartdate,
                                               sentinel1__sentinel1_orbit__exact=pe.sentinel1.sentinel1_orbit,                                               
                                               parcelle__exact = pe.parcelle
                                               ).order_by('-sentinel1__sentinel1_date')
            qs_6dates=[]
            i=0
            for pe6d in qs_pesel:
                if len(qs_6dates)==6:
                    break
                if i == 0 :
                    i+=1
                    if pe6d != pe :
                        print("Error : first occurrence is not reference for "+str(pe))
                        break
                if pe6d.meansigma40 != None:
                    if pe6d.meansigma40 > 0:
                        qs_6dates.append(pe6d)
                
                
            if len(qs_6dates)< 6 :
                print("Can't find 6 ParcelEvent in month before "+str(pe))
                exporterr.append(pe.parcelle.id_parcel +";"+ pe.sentinel1.sentinel1_date.strftime("%Y%m%dT%H%M%S")  +";"+ str(len(qs_6dates))  +";")
                continue
                
            x=[]
            y=[]
            for pe6d in qs_6dates: 
                y.append(10*math.log10(pe6d.meansigma40))
                xtd = pe.sentinel1.sentinel1_date - pe6d.sentinel1.sentinel1_date
                x.append(xtd.days)
            
            slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
            regr=r_value**2
            
            if regr > 0.5:
                pe.frozen_type = 0
                pe.save()
                print("Froze normalized for "+str(pe))
                
            else:
                print("Froze confirmed for "+str(pe))
            
            if exportregr:
                exstr = pe.parcelle.id_parcel+";"+pe.sentinel1.sentinel1_date.strftime("%Y%m%dT%H%M%S")+";"
                for i in range(6):
                    exstr += str(x[i])+";"
                for i in range(6):
                    exstr += str(y[i])+";"
                exstr += str(regr)+";"
                exportregr_data.append(exstr)
                
        if exportregr:
            outcsv = os.path.join(outdir,"REG_"+tile_str+"_"+str(year)+".csv")
            with open(outcsv, 'w') as csv:
                for line in exportregr_data:
                    csv.write(line+"\n")
            
            outerr = os.path.join(outdir,"REG_"+tile_str+"_"+str(year)+"_errors.csv")
            with open(outerr, 'w') as csv:
                for line in exporterr:
                    csv.write(line+"\n")
    

    def export_shapefile(self, tile_str,fdetect_date, outdir, suffix):
        startproc = datetime.datetime.now()
        #check image in db
        qs_s1date = mod.Sentinel1Image.objects.filter(tile__tile__exact=tile_str, 
                                                  sentinel1_date__exact=fdetect_date
                                                  )
        if len(qs_s1date)== 0 :
            print("Can't find Sentinel1 for date "+ fdetect_date.strftime("%Y%m%dT%H%M%S")+" and tile "+tile_str)
            exit()
        
        qs_pe = mod.ParcelEvent.objects.filter(sentinel1__exact=qs_s1date[0],
                                               frozen_type__gte=0
                                           )
        if len(qs_pe)== 0 :
            print("Can't find Parcel Event for date "+ fdetect_date.strftime("%Y%m%dT%H%M%S")+" and tile "+tile_str)
            exit()
        
        #Create shapefile
        shpname = "FREEZEDETECT_"+tile_str+"_"+fdetect_date.strftime("%Y%m%dT%H%M%S")
        if None != suffix :
            shpname = shpname + "_"+ suffix
        shpfile = shpname + ".shp"
        outshp = os.path.join(outdir,shpfile)
        print("Exporting to : "+outshp)
        # set up the shapefile driver
        driver = ogr.GetDriverByName("ESRI Shapefile")
        
        # create the data source
        data_source = driver.CreateDataSource(outshp)
        
        # create the spatial reference, WGS84
        srs = osr.SpatialReference()
        srs.ImportFromEPSG(2154)
        
        # create the layer
        layer = data_source.CreateLayer(shpname, srs, ogr.wkbMultiPolygon)
        
        # Add the fields we're interested in
        id_parcel = ogr.FieldDefn("ID_PARCEL", ogr.OFTString)
        id_parcel.SetWidth(10)
        layer.CreateField(ogr.FieldDefn("SURF_PARC", ogr.OFTReal))
        layer.CreateField(id_parcel)
        code_cultu = ogr.FieldDefn("CODE_CULTU", ogr.OFTString)
        code_cultu.SetWidth(3)
        layer.CreateField(code_cultu)
        code_group = ogr.FieldDefn("CODE_GROUP", ogr.OFTString)
        code_group.SetWidth(2)
        layer.CreateField(code_group)
        code_group = ogr.FieldDefn("PARC_TYPE", ogr.OFTString)
        code_group.SetWidth(1)
        layer.CreateField(code_group)
        frozen_type = ogr.FieldDefn("FROZ_TYPE", ogr.OFTString)
        frozen_type.SetWidth(1)
        layer.CreateField(frozen_type)
        layer.CreateField(ogr.FieldDefn("MREFSIGMA", ogr.OFTReal))
        layer.CreateField(ogr.FieldDefn("MEANSIGMA", ogr.OFTReal))
        layer.CreateField(ogr.FieldDefn("MEANTEMP", ogr.OFTReal))
        
        # Process the qs and add the attributes and features to the shapefile
        opbar=0
        i=0
        ls_data = len(qs_pe)
        for row in qs_pe:
            if self.isfloat(row.maxrefsigma40) and row.maxrefsigma40 > 0:
                refdb = 10*math.log10(row.maxrefsigma40)
            else:
                refdb = None
            if self.isfloat(row.meansigma40)and row.meansigma40 > 0:
                sigdb = 10*math.log10(row.meansigma40)
            else:
                sigdb = None
            if self.isfloat(row.meantemp):
                tempc = row.meantemp - 273.15
            else:
                tempc = None 
            # create the feature
            feature = ogr.Feature(layer.GetLayerDefn())
            # Set the attributes using the values from the delimited text file
            feature.SetField("ID_PARCEL", row.parcelle.id_parcel)
            feature.SetField("SURF_PARC", row.parcelle.surf_parc)
            feature.SetField("CODE_CULTU", row.parcelle.code_cultu)
            feature.SetField("CODE_GROUP", row.parcelle.code_group)
            feature.SetField("PARC_TYPE", row.parcelle_type)
            feature.SetField("FROZ_TYPE", row.frozen_type)
            feature.SetField("MREFSIGMA", refdb)
            feature.SetField("MEANSIGMA", sigdb)
            feature.SetField("MEANTEMP", tempc)
            
            # Create the point from the Well Known Txt
            mpolywkt= str(row.parcelle.mpoly.ewkt).split(";")
            polyparcel = ogr.CreateGeometryFromWkt(mpolywkt[1])
            # Set the feature geometry using the point
            feature.SetGeometry(polyparcel)
            # Create the feature in the layer (shapefile)
            layer.CreateFeature(feature)
            # Dereference the feature
            feature = None
            
#             pbar = int((i)/ls_data*100)
#             if pbar % 5 == 0 and pbar > opbar:
#                 opbar = pbar
#                 periodproc = datetime.datetime.now() - startproc
#                 print("#########################################################")
#                 print(str(pbar)+" percent done ( "+str(i)+" / "+str(ls_data)+" )")
#                 print("Elapse time : "+str(periodproc))
#                 print("Remaining   : "+str(periodproc/(pbar/100)-periodproc))
#             i+=1
        # Save and close the data source
        data_source = None
        periodproc = datetime.datetime.now() - startproc
        print("Total elapse time : "+str(periodproc))  
        
    def export_stats(self, tile_str,s1list,orbit, outdir, suffix):
        
        csvname = "FREEZESTATS_"+tile_str+"_"+orbit
        if None != suffix :
            csvname = csvname + "_"+ suffix
        csvfile = csvname + ".csv"
        outcsv = os.path.join(outdir,csvfile)
        print("Exporting to : "+outcsv)
        # set up the shapefile driver
        csvheader = ["S1_DATE","P_LC1","P_LC1_FROZ0","P_LC1_FROZ1","P_LC1_FROZ2","P_LC2","P_LC2_FROZ0","P_LC2_FROZ1","P_LC2_FROZ2"]
        
        with open(outcsv,'w') as ocsv:
            ocsv.write(";".join(csvheader)+"\n")
            
            for row in s1list:
                
                ntot = mod.ParcelEvent.objects.filter(sentinel1__exact=row,
                                                  sentinel1__tile__tile__exact=tile_str,
                                                  frozen_type__gte=0
                                                   ).count()
                nLC1 = mod.ParcelEvent.objects.filter(sentinel1__exact=row,
                                                  sentinel1__tile__tile__exact=tile_str,
                                                  parcelle_type=1,
                                                  frozen_type__gte=0
                                                   ).count()
                nLC1_FRZ0 = mod.ParcelEvent.objects.filter(sentinel1__exact=row,
                                                  sentinel1__tile__tile__exact=tile_str,
                                                  parcelle_type=1,
                                                  frozen_type=0
                                                   ).count()
                nLC1_FRZ1 = mod.ParcelEvent.objects.filter(sentinel1__exact=row,
                                                  sentinel1__tile__tile__exact=tile_str,
                                                  parcelle_type=1,
                                                  frozen_type=1
                                                   ).count()
                nLC1_FRZ2 = mod.ParcelEvent.objects.filter(sentinel1__exact=row,
                                                  sentinel1__tile__tile__exact=tile_str,
                                                  parcelle_type=1,
                                                  frozen_type=2
                                                   ).count()
                nLC2 = mod.ParcelEvent.objects.filter(sentinel1__exact=row,
                                                  sentinel1__tile__tile__exact=tile_str,
                                                  parcelle_type=2,
                                                  frozen_type__gte=0
                                                   ).count()
                nLC2_FRZ0 = mod.ParcelEvent.objects.filter(sentinel1__exact=row,
                                                  sentinel1__tile__tile__exact=tile_str,
                                                  parcelle_type=2,
                                                  frozen_type=0
                                                   ).count()
                nLC2_FRZ1 = mod.ParcelEvent.objects.filter(sentinel1__exact=row,
                                                  sentinel1__tile__tile__exact=tile_str,
                                                  parcelle_type=2,
                                                  frozen_type=1
                                                   ).count()
                nLC2_FRZ2 = mod.ParcelEvent.objects.filter(sentinel1__exact=row,
                                                  sentinel1__tile__tile__exact=tile_str,
                                                  parcelle_type=2,
                                                  frozen_type=2
                                                   ).count()
                
                if ntot == 0 :
                    pLC1, pLC2 = 0, 0
                else :
                    pLC1, pLC2 = nLC1/ntot*100, nLC2/ntot*100
                if nLC1 == 0 :
                    pLC1_FRZ0, pLC1_FRZ1, pLC1_FRZ2 = 0, 0, 0
                else :
                    pLC1_FRZ0, pLC1_FRZ1, pLC1_FRZ2 = nLC1_FRZ0/nLC1*100, nLC1_FRZ1/nLC1*100, nLC1_FRZ2/nLC1*100
                if nLC2 == 0 :
                    pLC2_FRZ0, pLC2_FRZ1, pLC2_FRZ2 = 0, 0, 0
                else :
                    pLC2_FRZ0, pLC2_FRZ1, pLC2_FRZ2 = nLC2_FRZ0/nLC2*100, nLC2_FRZ1/nLC2*100, nLC2_FRZ2/nLC2*100
                
                rec = [row.sentinel1_date.strftime("%Y%m%dT%H%M%S"),
                       str(pLC1),str(pLC1_FRZ0),str(pLC1_FRZ1),str(pLC1_FRZ2),
                       str(pLC2),str(pLC2_FRZ0),str(pLC2_FRZ1),str(pLC2_FRZ2)]
                
                ocsv.write(";".join(rec)+"\n")
                

        print("Export Done.")  
        
    def batch_export_shapefile(self, tile_str, orbit, stats, outdir, suffix, wseason):
        if wseason == 'all':
            qs_s1date = mod.Sentinel1Image.objects.filter(tile__tile__exact=tile_str,
                                                   sentinel1_orbit__exact = orbit,
                                                  ).order_by("sentinel1_date")
        else:
            start_date = datetime.date(wseason-1, 8, 31)
            end_date = datetime.date(wseason, 5, 1)
            qs_s1date = mod.Sentinel1Image.objects.filter(tile__tile__exact=tile_str,
                                                       sentinel1_orbit__exact = orbit, 
                                                       sentinel1_date__range=(start_date, end_date)
                                                      ).order_by("sentinel1_date")
        if len(qs_s1date)== 0 :
            print("No image available for tile "+tile_str+" and wseason "+str(wseason))
            exit()
            
        
        statslist=[]
        for s1 in qs_s1date:
            
            nb_pe = mod.ParcelEvent.objects.filter(sentinel1__exact=s1,
                                               frozen_type__gte=0
                                               ).count()
            
        
            if nb_pe > 0 :
                if not stats:
                    self.export_shapefile(tile_str, s1.sentinel1_date, outdir, suffix)
                else:
                    statslist.append(s1)
                    
        if not stats:
            return
        
        self.export_stats(tile_str, statslist,orbit, outdir, suffix)
            
            
        
    def export_parcel_csvtimeserie(self,tile_str,parcel_id, outdir, suffix):
        startproc = datetime.datetime.now()
        
        qs_pe = mod.ParcelEvent.objects.filter(parcelle__id_parcel__exact=parcel_id,
                                           sentinel1__tile__tile__exact=tile_str,
                                           meansigma40__isnull=False,
                                           meantemp__isnull=False
                                           )
        
        if len(qs_pe)== 0 :
            print("No event found for parcel "+parcel_id+" at tile "+tile_str)
            exit()
        
        #Create shapefile
        csvname = "TIMESERIE_"+tile_str+"_"+parcel_id
        if None != suffix :
            csvname = csvname + "_"+ suffix
        csvfile = csvname + ".csv"
        outcsv = os.path.join(outdir,csvfile)
        print("Exporting to : "+outcsv)
        
        with open(outcsv,"x") as xcsv:
            xcsv.write("Date;MeanSigma40dB;MeanTemperatureC;ParcelType;FreezeDetect\n")
            for pe in qs_pe :
                if self.isfloat(pe.meansigma40) and pe.meansigma40 > 0:
                    sigdb = 10*math.log10(pe.meansigma40)
                else:
                    sigdb = None
                if self.isfloat(pe.meantemp):
                    tempc = pe.meantemp - 273.15
                else:
                    tempc = None 
                xcsv.write(pe.sentinel1.sentinel1_date.strftime("%Y%m%dT%H%M%S")+";"+
                           str(sigdb)+";"+str(tempc)+";"+str(pe.parcelle_type)+";"+str(pe.frozen_type)+"\n")
                
        periodproc = datetime.datetime.now() - startproc
        print("Total elapse time : "+str(periodproc))  
        
        