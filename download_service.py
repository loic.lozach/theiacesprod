#!/usr/local/bin/python
import os, glob, argparse,time
import cdsapi
from subprocess import Popen, PIPE

PROJECTDIR="/work/python/"

def process_command(cmd):
    print("Starting : "+" ".join(cmd))
    
    cmd[1]=PROJECTDIR+cmd[1]

    p = Popen(cmd, stdout=PIPE)
#    p.wait()
    output = p.communicate()[0]
    if p.returncode != 0: 
        print("process failed %d : %s" % (p.returncode, output))
    print("#################################################")
    return p.returncode


if __name__ == "__main__":
    
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        Download services
        """)

    subparsers = parser.add_subparsers(help='Choose server', dest="pipeline")

    # Short pipeline
    list_parser = subparsers.add_parser('eodag', help="Download Sentinel2-L3A (over FRANCE) through Eodag API with Theia provider")
    list_parser.add_argument('-downdir', action='store', required=True, help="Download directory")
    # list_parser.add_argument('-prod', choices=["0", "1", "2"],  default=0, required=False, help="Product type to download, choices 0=S1-GRD, 1=S2-L2A, 2=S2-L3A(FRANCE)")  #, 3=LANDSAT8-OLI/TIRS"
    list_parser.add_argument('-tile', action='store', required=True, help="S2 Tile name, ex. T30UVU")
    list_parser.add_argument('-sd', action='store', required=True, help="Starting date S1, ex. 2020-08-31")
    list_parser.add_argument('-ed', action='store', required=True, help="Ending date S1, ex. 2020-09-31")

    list_parser = subparsers.add_parser('dataspace', help="Download Sentinel1-GRD and Sentinel2-L2A with Copernicus Dataspace SentinelHub")
    list_parser.add_argument('-downdir', action='store', required=True, help="Download directory")
    list_parser.add_argument('-prod', choices=["s1", "s2"],  default=0, required=False, help="Product type to download, choices s1=S1-GRD, s2=S2-L2A")  #, 3=LANDSAT8-OLI/TIRS"
    list_parser.add_argument('-tile', action='store', required=True, help="S2 Tile name, ex. T30UVU")
    list_parser.add_argument('-sd', action='store', required=True, help="Starting date S1, ex. 2020-08-31")
    list_parser.add_argument('-ed', action='store', required=True, help="Ending date S1, ex. 2020-09-31")
    
    # list_parser = subparsers.add_parser('cds', help="Download ERA5 temperature image file according to S1 dates")
    # list_parser.add_argument('-s1orthodir', action='store', required=True, help="Orthorectified Sentinel1 directory")
    # list_parser.add_argument('-gap', choices=['3m', '6d'], required=False, default='land', help="[Optional] Query ERA5-Land reanalysis collection with 3 month gap (default '3m') OR ERA5 basic reanalysis collection with 6 days gap ('6d') ")
    # list_parser.add_argument('-outdir', action='store', required=True, help='Output directory')
    
    args=parser.parse_args()
    
    #TODO option de tléchargement à partir d'un shape

    cmd=["python", "manage.py", "runscript", "ServiceWrapper", "--script-args"] #"--traceback",

    if args.pipeline == 'eodag' :
        
        cmd += [
            "eodag",
            "prodtypenum@" +"2"
            "mrgstile@"+args.tile,
            "startdate@"+args.sd,
            "enddate@"+args.ed,
            "downdir@"+args.downdir
            ]
        process_command(cmd)

    elif args.pipeline == 'dataspace' :
        
        cmd += [
            "dataspace",
            "prodtype@" +args.prod,
            "mrgstile@"+args.tile,
            "startdate@"+args.sd,
            "enddate@"+args.ed,
            "downdir@"+args.downdir
            ]
        process_command(cmd)
              
    # elif args.pipeline == 'cds' :
    #     outabs = os.path.abspath(args.outdir)
    #     if not os.path.exists(outabs):
    #         os.makedirs(outabs)
        
#         TODO: A integrer
#         s1files = glob.glob(os.path.join(args.s1dir, 'S1*.TIF'))
#         extend = get_ref_extend(args.ref)
#         errfile = os.path.join(args.outdir, "download_error.log")
#         with open(errfile, 'w') as err:
#             for s1 in s1files:
#                 stdatetime = os.path.basename(s1)[:-4].split("_")[4]
#                 stdate = stdatetime.split("T")[0]
#                 sttime = stdatetime.split("T")[1]
#                 rdtime = round_s1time(sttime) 
#                 
#                 if args.gap == '3m':
#                     outfile = os.path.join(args.outdir,"ERA5-2m_"+args.zone+"_"+stdatetime+".grib")
#                     print("Downloading CDS : "+outfile)
#                     request = download_cds(stdate[:4], stdate[4:6], stdate[6:8], rdtime, extend[0], extend[1], extend[2], extend[3], outfile)
#                 else:
#                     outfile = os.path.join(args.outdir,"ERA5-1000hpa_"+args.zone+"_"+stdatetime+".grib")
#                     print("Downloading CDS : "+outfile)
#                     request = download_cds_inst(stdate[:4], stdate[4:6], stdate[6:8], rdtime, extend[0], extend[1], extend[2], extend[3], outfile)
#                 
#                 if not os.path.exists(outfile):
#                     err.write(request[2]+";"+request[1]+"\n")
#                     err.write(request[0]+"\n")
        

        
        
        