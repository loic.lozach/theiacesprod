from theiacesprodapp.backend.servicemanager import SSMManager
from theiacesprodapp.backend.service.calibration import OrthorectifySentinel1
from theiacesprodapp.backend.service.ssmproduction import SSMProduction
from theiacesprodapp.backend.service.preprocess import gapfilling_pipeline, gpm_pipeline, ndvi_pipeline, processrgp_pipeline, slope_pipeline, stack_pipeline
from theiacesprodapp.backend.service.download import ClimateDataSystem, Microsoft_Stac_Catalog, SentinelEodag, Dataspace_Copernicus_Catalog
from django.conf import settings

    
def run(*args):
    try:
        print(args)
        split_args = list(args)  #shlex.split(args[0])
        choix = args[0]
        split_args.pop(0)
        argsdict={}
        for arg in split_args:
            argsdict[arg.split("@")[0]]=arg.split("@")[1]
            if arg.split("@")[0] in ['overwrite', 'addtodb','debug']:
                argsdict[arg.split("@")[0]]=bool(int(arg.split("@")[1]))
            if arg.split("@")[0] in ['slopevalue', 'prodtypenum']:
                argsdict[arg.split("@")[0]] = int(arg.split("@")[1])
            if arg.split("@")[1] == 'none' :
                argsdict[arg.split("@")[0]]=None
                
    except :
        choix = None
    
    if "debug" in argsdict :
        if argsdict["debug"] :
            # logger.setLevel(logging.DEBUG)
            settings.DEBUG = True
        else:
            settings.DEBUG = False
    else:
        settings.DEBUG = False
    
    if choix == "autossmfr":
        parametres={
            #attribut download
                "agriref":argsdict["agriref"],
                "zone":argsdict["zone"],
                "aoifile":argsdict["aoifile"], 
                "landusemap":argsdict["landusemap"],
                "mrgstile":argsdict["s2tile"], #define workdir
                "startdate":argsdict["sd"],
                "enddate":argsdict["ed"], #0=S1-GRD,1=S2-L2A,2=S2-L3A
                "sentinel1_polar_mode" : argsdict["sarmode"], #choices=['vv', 'vh', 'vvvh']
                "rgpshp":argsdict["rgpshp"],
                "rpg_pub_year":argsdict["rpg_pub_year"],
                "labelsfield":argsdict["labelsfield"],
                "landusefield":argsdict["landusefield"],
                "agrivalues": argsdict["agrivalues"],
                "tfmodel_dir": argsdict["modeldir"],
                'tfmodel_choice':argsdict["modelchoice"], #["dry","wet"],
                'slope_value':argsdict["slopevalue"],
                'outformat':argsdict["outformat"],#  choices=['raster', 'csv'],
                "skipdownloads":argsdict["skipdownloads"],
                'addtodb':argsdict["addtodb"],
                "overwrite":argsdict["overwrite"],
                "debug":argsdict["debug"],
                }
        autop = SSMManager(parametres)
        autop.autoprodssmfr()
        print("Wrapper done.")
        
    elif choix == "autossmww":
        parametres={
            #attribut download
                "agriref":argsdict["agriref"], #user zone
                "zone":argsdict["zone"],
                "aoifile":argsdict["aoifile"], 
                "landusemap":argsdict["landusemap"],
                "mrgstile":argsdict["s2tile"], #user zone
                "startdate":argsdict["sd"],
                "enddate":argsdict["ed"], 
                "rgpshp":argsdict["rgpshp"],
                "labelsfield":argsdict["labelsfield"],
                "landusefield":argsdict["landusefield"],
                "agrivalues": argsdict["agrivalues"],
                "tfmodel_dir": argsdict["modeldir"],
                'tfmodel_choice':argsdict["modelchoice"], #["dry","wet"],
                'slope_value':argsdict["slopevalue"],
                "sentinel1_polar_mode":argsdict["sarmode"],
                'outformat':argsdict["outformat"],
                "skipdownloads":argsdict["skipdownloads"],
                "overwrite":argsdict["overwrite"],
                "debug":argsdict["debug"],
                }
        autop = SSMManager(parametres)
        autop.autoprodssmww()
        print("Wrapper done.")

    elif choix == "calibration":
        parametres={
            #attribut download
                "sentinel1_L1A_zip_dir":argsdict["indir"], #define workdir
                "reference_image_file":argsdict["inref"],
                "mrgstile":argsdict["mrgstile"], #0=S1-GRD,1=S2-L2A,2=S2-L3A
                "s1_ortho_dir" : argsdict["outdir"], #choices=['vv', 'vh', 'vvvh']
                "sentinel1_polar_mode":argsdict["polar"],
                "sentinel1_incidence_type":argsdict["theta"],
                "overwrite":argsdict["overwrite"],
                "urlsrmt":"http://step.esa.int/auxdata/dem/SRTMGL1/",
                "srtmhgtzip":"/data/SRTM/",
                "geoid":"/work/python/data/egm96.grd"
                }
        autop = OrthorectifySentinel1(parametres)
        autop.start()
        print("Wrapper done.")
        
    elif choix == "production":
        parametres={
            #attribut download
                "agriref":"vector", 
                "landusemap":"",
                "tfmodel_dir":argsdict["modeldir"], #define workdir
                "tfmodel_choice":argsdict["modelchoice"],
                "s1_ortho_dir" : argsdict["s1orthodir"],
                "mrgstile":argsdict["mrgstile"], 
                "gapfndvidir" : argsdict["ndvidir"], #choices=['vv', 'vh', 'vvvh']
                "labels_file" : argsdict["labels_file"],
                "slope_file" : argsdict["slope"],
                "slope_value" : argsdict["slopevalue"],
                "outformat" : argsdict["outformat"],
                "data_output_dir" : argsdict["outdir"],
                "sentinel1_polar_mode":argsdict["polar"],
                "sentinel1_incidence_type":argsdict["incid"],
                "overwrite":argsdict["overwrite"],
                "ssmfootprint_shpname":""
                }
        autop = SSMProduction(parametres)
        autop.start()
        print("Wrapper done.")
        
    elif choix == "ndvi":
        parametres={
            #attribut download
                "s2l2zipdir":argsdict["zipdir"], #define workdir
                # "format":argsdict["format"],
                "ndvidir" : argsdict["ndvidir"],
                "mrgstile":argsdict["mrgstile"],
                "overwrite":argsdict["overwrite"]
                }
        
        autop = ndvi_pipeline(parametres)
        if parametres["mrgstile"]=="aoi":
            autop.process_pcstac()
        else:
            autop.process()
        print("Wrapper done.")
        
    elif choix == "gapfill":
        parametres={
            #attribut download
                "ndvidir":argsdict["ndvidir"], #define workdir
                "interpolation":argsdict["interpolation"],
                "gapfmaskdir" : argsdict["outmaskdir"],
                "gapfndvidir":argsdict["outdir"],
                "interpolation":argsdict["interpolation"],
                "overwrite":argsdict["overwrite"]
                }
        autop = gapfilling_pipeline(parametres)
        autop.process()
        print("Wrapper done.")
        
    elif choix == "processrgp":
        parametres={
            #attribut download
                "rgpshp":argsdict["rgpshp"], #define workdir
                "labelsfield":argsdict["labelsfield"],
                "landusefield" : argsdict["landusefield"],
                "mrgstile":argsdict["mrgstile"], #0=S1-GRD,1=S2-L2A,2=S2-L3A
                "agrivalues" : argsdict["agrivalues"], #choices=['vv', 'vh', 'vvvh']
                "reference_image_file" : argsdict["inref"],
                "processrgpdir" : argsdict["outdir"],
                "overwrite":argsdict["overwrite"]
                }
        autop = processrgp_pipeline(parametres)
        autop.process()
        print("Wrapper done.")
        
    elif choix == "stack":
        parametres={
            #attribut download
                "ndvidir":argsdict["ndvidir"], #define workdir
                "lulc":argsdict["lulc"],
                "agrivalues" : argsdict["agrivalues"],
                "outdir":argsdict["outdir"]
                }
        autop = stack_pipeline(parametres)
        autop.process()
        print("Wrapper done.")
        
    elif choix == "slope":
        parametres={
            #attribut download
                "srtmhgtzip":argsdict["srtmdir"], #define workdir
                "reference_image_file":argsdict["modelchoice"],
                "mrgstile" : argsdict["mrgstile"],
                "outdir":argsdict["outdir"]
                }
        autop = slope_pipeline(parametres)
        autop.process()
        print("Wrapper done.")
        
    elif choix == "gpm":
        parametres={
            #attribut download
                "s1_ortho_dir":argsdict["s1orthodir"], #define workdir
                "GPM_DIR":argsdict["gpmdir"],
                "mrgstile" : argsdict["mrgstile"],
                "modelchoice_outdir":argsdict["outtxt"]
                }
        autop = gpm_pipeline(parametres)
        autop.process()
        print("Wrapper done.")
        
    elif choix == "eodag":

        eod = SentinelEodag()
        eod.search_and_download_from_tile(argsdict["prodtypenum"], argsdict["mrgstile"], argsdict["startdate"], argsdict["enddate"], argsdict["downdir"])
            
        print("Wrapper done.")

    elif choix == "dataspace":
        tilegeom = Dataspace_Copernicus_Catalog.get_wkt_tile(argsdict['mrgstile'])
        collection={"s1":["SENTINEL-1",tilegeom.wkt],"s2":["SENTINEL-2",tilegeom.centroid.wkt]}
        
        dataspace = Dataspace_Copernicus_Catalog(collection[argsdict["prodtype"]][0],argsdict["startdate"], argsdict["enddate"], 
                                                     collection[argsdict["prodtype"]][1], argsdict['mrgstile'],argsdict["downdir"])
        dataspace.search_dataspace_odata()
        S1filelist = dataspace.download_data()

    else:
        print("Wrong argument.")
        exit()






















