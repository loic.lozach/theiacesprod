# THEIA CES Production Services

Cette application Django contient plusieurs modules pour la production opérationnelle de cartographie utilisant des méthodologies des CES (Centre d'Expertise Scientifique) du pôle THEIA basée sur l'imagerie satellitaire Sentinel 1 et 2. Les méthodologies implémentées sont:
* Sentinel Soil Moisture at Plot scale (S2MP, Bagdadi et al.) 
* Frozen Plot Detection (under development)
* Irrigation Plot Detection (under development) 


## Manuel utilisateur

* The [Soil Moisture Pipeline for agricultural areas user manual](https://gitlab.irstea.fr/loic.lozach/theiacesprod/-/blob/master/TheiaCESProd_Manual.pdf) is available.

# Pre-requis

TheiaCESProd only requires docker and docker-compose to work. Les applications git et make sont généralement installé par default sur la pluspart des distributions Linux. 

* install [docker Server](https://docs.docker.com/engine/install/). Not Desktop
* configure docker with [non-root user](https://docs.docker.com/engine/install/linux-postinstall/)
* install [docker-compose](https://docs.docker.com/compose/install/other/)

# Install

L’application est encapsulée dans une image Docker, ce qui permet une grande portabilité. Toutefois certaines configurations sont spécifiques à Linux, l’installation est donc limitée à ce type d’OS.

```
user@machine:~$ mkdir theiacesprod
user@machine:~$ cd theiacesprod/
user@machine:~/theiacesprod$ git clone https://gitlab.irstea.fr/loic.lozach/theiacesprod.git .
user@machine:~/theiacesprod$ gedit config.ini
```
Dans le dossier projet, ouvrir le fichier config.ini et modifier les valeurs des variables DATAPATH et DBPATH, pour le dossier d'accès à vos données et le dossier de sauvegarde de la base de données, ainsi que les logins et mots de passe de vos comptes sur les sites distributeurs des images SENTINEL: 
- SCIHUB : [The Copernicus Data Space ](https://dataspace.copernicus.eu/)
- THEIA : [Produits à valeur ajoutée et algorithmes pour les surfaces continentales](https://theia.cnes.fr/atdistrib/rocket/#/home)

A l’intérieur de l’image docker, votre répertoire de données DATAPATH sera accessible dans /data.
Puis lancer la commande make pour acceder à l'aide sur les options :
```
user@machine:~/theiacesprod$ make

Useful targets:

  install          > Install or rebuild TheiaCESProd
  start            > Start containers
  dbmigrate        > Initialize database
  shell            > Shell into client container
  restart          > Restart containers
  stop             > Stop and kill running containers
  status           > Display stack containers status
  logs             > Display containers logs


```

Pour installer TheiaCESProd il suffit donc de lancer la `make install`. Si vous souhaitez modifier une des variables du fichier config.ini, il sera nécessaire de relancer `make install` pour que les changements soient pris en compte.
```
user@machine:~/theiacesprod$ make install
```   

# Lancement de l’image Docker

Pour lancer les containers en commande d'arrière plan:
```
user@machine:~/theiacesprod$ make start
```

Pour initialiser la base de données (opération à ne réaliser qu'une seule fois):
```
user@machine:~/theiacesprod$ make dbmigrate
```

Pour accéder au shell (bash) du container webtheia :
```
user@machine:~/theiacesprod$ make shell
```

# Usage

## Auto Production Service
```
otbuser@c50154f03c40:/work/python$ autoprodssm_service.py -h
usage: autoprodssm_service.py [-h] {ssmww,ssmfr} ...

Automatic production of THEIA CES products

positional arguments:
  {ssmww,ssmfr}  Choose production over France (S2-L3A) or World Wide (S2-L2A)
                 (not available)
    ssmww        Automatic production of Soil Moisture maps with S2-L2A.
    ssmfr        Automatic production of Soil Moisture maps over France with S2-L3A and RPG
                 agricultural reference vector.

optional arguments:
  -h, --help    show this help message and exit
```

Auto product SoilMoisture Service world wide allows a process with Sentinel2 L2A against an agricultural plot reference in vector or raster format.
```
otbuser@c50154f03c40:/work/python$ autoprodssm_service.py ssmww -h
usage: autoprodssm_service.py ssmww [-h] -sd YYYY-MM-DD -ed YYYY-MM-DD -modeldir MODELDIR
                                    [-skipdown {s1,s2,all,no}] [-overwrite] -agriref
                                    {vector,raster} [-agri.vect.file FILE]
                                    [-agri.vect.idfield STRING] [-agri.vect.lufield STRING]
                                    [-agri.vect.luvalues LIST]
                                    [-agri.vect.clipchoice {s2tile,none}]
                                    [-agri.vect.clip.s2tile T##XXX] [-agri.rast.file FILE]
                                    [-agri.rast.luvalues LIST] [-agri.rast.clipchoice {s2tile,aoi}]
                                    [-agri.rast.clip.s2tile T##XXX] [-agri.rast.clip.aoi FILE]

optional arguments:
  -h, --help            show this help message and exit
  -sd YYYY-MM-DD        Starting date S1, ex. 2020-08-31
  -ed YYYY-MM-DD        Ending date S1, ex. 2020-09-31
  -modeldir MODELDIR    Directory to find both "SavedModel_wet" and "SavedModel_dry" tensorflow
                        model
  -skipdown {s1,s2,all,no}
                        [Optional] Skip sentinel 1 or 2 or both downloading step (default no). This
                        allows user to filter cloudy Sentinel2 images by removing them from project
                        directory in order to obtain better quality on gapfilled NDVI. Also useful
                        if offline images can not be retrieve.
  -overwrite            [Optional] Overwrite already existing files (default False)
  -agriref {vector,raster}
                        Choose between agricultural plot reference vector file or Copernicus
                        landcover map
  -agri.vect.file FILE  Agricultural plot reference vector file, required if agriref=vector
  -agri.vect.idfield STRING
                        Labels field name , default ID_PARCEL (France RPG 2019 shapefile)
  -agri.vect.lufield STRING
                        LandUse field name , default CODE_GROUP (France RPG 2019 shapefile)
  -agri.vect.luvalues LIST
                        List of agricultural areas values in LandUse vector, default "all"(all
                        CODE_GROUP in France RPG 2019 shapefile), ex: "1 2 3 4"
  -agri.vect.clipchoice {s2tile,none}
                        Choose between clippng with a sentinel2 tile (s2tile) or use whole vector
                        file (none), area size must be under half S2 tile (<5000km2).
  -agri.vect.clip.s2tile T##XXX
                        Clip agricultural plot reference vector with Sentinel2 Tile, ex: T30UVU
  -agri.rast.file FILE  Copernicus landcover map at 20x20 degree tiles raster file and 100 meters
                        pixel resolution, required if agriref=raster
  -agri.rast.luvalues LIST
                        List of agricultural areas values in Copernicus landuse map, default "30
                        40"(grassland and crops)
  -agri.rast.clipchoice {s2tile,aoi}
                        Choose between clippng with a sentinel2 tile (s2tile) or a polygon vector
                        file (aoi), area size must be under half S2 tile (<5000km2).
  -agri.rast.clip.s2tile T##XXX
                        Clip landcover map over Sentinel2 Tile, ex: T30UVU
  -agri.rast.clip.aoi FILE
                        An user's AOI vector file, area size must be under half S2 tile (<5000km2).
```

Auto product SoilMoisture Service over France allows a process with Sentinel2 L3A against french agricultural plot reference (RPG).
```
otbuser@39a616d684e8:/data$ autoprodssm_service.py ssmfr -h
usage: autoprodssm_service.py ssmfr [-h] -sd YYYY-MM-DD -ed YYYY-MM-DD -modeldir MODELDIR
                                    [-modelchoice MODELCHOICE] [-skipdown {s1,s2,all,no}]
                                    [-addtodb] [-overwrite] -agri.vect.file FILE -agri.vect.pubyear
                                    AAAA -agri.vect.s2tile T##XXX [-agri.vect.idfield STRING]
                                    [-agri.vect.lufield STRING] [-agri.vect.luvalues LIST]

optional arguments:
  -h, --help            show this help message and exit
  -sd YYYY-MM-DD        Starting date S1, ex. 2020-08-31
  -ed YYYY-MM-DD        Ending date S1, ex. 2020-09-31
  -modeldir MODELDIR    Directory to find both "SavedModel_wet" and "SavedModel_dry" tensorflow
                        model
  -modelchoice MODELCHOICE
                        [Optional] dry, wet or text file resulting from gpm pipeline, default dry
  -skipdown {s1,s2,all,no}
                        [Optional] Skip sentinel 1 or 2 or both downloading step (default no). This
                        allows user to filter cloudy Sentinel2 images by removing them from project
                        directory in order to obtain better quality on gapfilled NDVI. Also useful
                        if offline images can not be retrieve.
  -addtodb              [Optional] Add soilmoisture results in database (default False)
  -overwrite            [Optional] Overwrite already existing files (default False)
  -agri.vect.file FILE  Reseau Parcellaire Graphique (RPG) vector file
  -agri.vect.pubyear AAAA
                        Year of publication of RPG
  -agri.vect.s2tile T##XXX
                        Clip agricultural plot reference vector with Sentinel2 Tile, ex: T30UVU
  -agri.vect.idfield STRING
                        Labels field name , default ID_PARCEL (France RPG 2019 shapefile)
  -agri.vect.lufield STRING
                        LandUse field name , default CODE_GROUP (France RPG 2019 shapefile)
  -agri.vect.luvalues LIST
                        List of agricultural areas values in LandUse vector, default "all"(all
                        CODE_GROUP in France RPG 2019 shapefile), ex: "1 2 3 4"

```

## Download Service
```
otbuser@50edfb299028:/data$ download_service.py -h
usage: download_service.py [-h] {eodag,cds} ...

Download services

positional arguments:
  {eodag,dataspace}  Choose server
    eodag            Download Sentinel2-L3A (over FRANCE) through Eodag API with Theia provider
    dataspace        Download Sentinel1-GRD and Sentinel2-L2A with Copernicus Dataspace SentinelHub
```

Eodag is an API to wrap multiple imagery provider.
```
otbuser@39a616d684e8:/data$ download_service.py eodag -h
usage: download_service.py eodag [-h] -downdir DOWNDIR -tile TILE -sd SD -ed ED

optional arguments:
  -h, --help        show this help message and exit
  -downdir DOWNDIR  Download directory
  -tile TILE        S2 Tile name, ex. T30UVU
  -sd SD            Starting date S1, ex. 2020-08-31
  -ed ED            Ending date S1, ex. 2020-09-31
```

Copernicus Dataspace 
```
otbuser@39a616d684e8:/data$ download_service.py dataspace -h
usage: download_service.py dataspace [-h] -downdir DOWNDIR [-prod {s1,s2}] -tile TILE -sd SD -ed ED

optional arguments:
  -h, --help        show this help message and exit
  -downdir DOWNDIR  Download directory
  -prod {s1,s2}     Product type to download, choices s1=S1-GRD, s2=S2-L2A
  -tile TILE        S2 Tile name, ex. T30UVU
  -sd SD            Starting date S1, ex. 2020-08-31
  -ed ED            Ending date S1, ex. 2020-09-31
```

## Calibration Service
```
otbuser@50edfb299028:/work/python$ calibration_service.py -h
usage: calibration_service.py [-h] -indir INDIR -inref INREF -zone ZONE -outdir OUTDIR [-polar {vv,vh,vvvh}] [-theta {loc,elli}] [-overwrite]

S1 Calibration, orhorectification, dem, slope, aspect and incidence angle processing

optional arguments:
  -h, --help           show this help message and exit
  -indir INDIR         Directory containing Sentinel1 .zip files
  -inref INREF         Image reference, generally corresponding to a S2 tile
  -zone ZONE           Geographic zone reference for output files naming
  -outdir OUTDIR       Output directory
  -polar {vv,vh,vvvh}  [Optional] Process VV, VH or both polarization, default vv
  -theta {loc,elli}    [Optional]Create local incidence raster or incidence raster from ellipsoid, default loc
  -overwrite           [Optional] Overwrite already existing files (default False)


```

## Preprocess Service
```
otbuser@50edfb299028:/work/python$ preprocess_service.py -h
usage: preprocess_service.py [-h] {ndvi,gapfilling,processrgp,stack,slope,gpm} ...

Preprocessing Services

positional arguments:
  {ndvi,gapfilling,processrgp,stack,slope,gpm}
                        Preprocess pipeline
    ndvi                Compute cloud-masked NDVI from Sentinel2 or Landsat8.
    gapfilling          Perform Gapfilling over NDVI time serie.
    processrgp          Create plot label image and agricultural mask from RGP
    stack               Create stacked and masked image from NDVI directory to the input of a segmentation process
    slope               Extract SRTM1SecHGT with image ref and compute slope
    gpm                 Downloads, extracts and computes synthesis of NASA Global Precipitation Model raster for TF model selection (wet or dry)

optional arguments:
  -h, --help            show this help message and exit


```

## Production Service
```
otbuser@50edfb299028:/work/python$ production_service.py -h
usage: production_service.py [-h] -modeldir MODELDIR [-modelchoice MODELCHOICE] -s1orthodir S1ORTHODIR -ndvidir NDVIDIR -maskedlabels MASKEDLABELS -zone ZONE [-slope SLOPE] [-slopevalue SLOPEVALUE]
                             [-polar {vv,vh,vvvh}] [-incid {loc,elli}] [-outformat {raster,csv}] -outdir OUTDIR [-overwrite]

"Batch inversion pipeline over a directory of Sentinel-1 images"

optional arguments:
  -h, --help            show this help message and exit
  -modeldir MODELDIR    Directory to find both wet and dry tensorflow model
  -modelchoice MODELCHOICE
                        [Optional] dry, wet or text file resulting from gpm pipeline, default dry
  -s1orthodir S1ORTHODIR
                        Directory to find orthorectified Sentinel-1 .TIF images, (regex: "S1X_IW_GRDH_[ZONE]_AAAAMMDDTHHMMSS_[POLAR].TIF)
  -ndvidir NDVIDIR      Monthly NDVI image directory computed from Sentinel-2 (regex: "NDVIXXX_[ZONE]_AAAAMMDD.TIF)
  -filtlabels FILTLABELS
                        Filtered agricultural plot labels image
  -zone ZONE            Geographic zone reference for output files naming
  -slope SLOPE          [Optional] Slope image to filter SarVV
  -slopevalue SLOPEVALUE
                        [Optional] Max value of slope to use to filter SarVV, default 20
  -polar {vv,vh,vvvh}   [Optional] Choose between VV, VH modes or both for input SAR image, default vv
  -incid {loc,elli}     [Optional] Use Local or from ellipsoid incidence angle image, default loc
  -outformat {raster,csv}
                        [Optional] Format of output when calling otbcli_InvertSARModel, cvs or raster
  -outdir OUTDIR        Directory of Soil moisture map over agricultural areas results
  -overwrite            [Optional] Overwrite already existing files (default False)


```


## License

Please see the license for legal issues on the use of the software (GNU Affero General Public License v3.0).

## Contact
Loïc Lozac'h (INRAE)
