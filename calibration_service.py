#!/usr/local/bin/python
# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.


import argparse
from subprocess import Popen, PIPE


PROJECTDIR="/work/python/"

def process_command(cmd):
    print("Starting : "+" ".join(cmd))
    
    cmd[1]=PROJECTDIR+cmd[1]

    p = Popen(cmd, stdout=PIPE)
#    p.wait()
    output = p.communicate()[0]
    if p.returncode != 0: 
        print("process failed %d : %s" % (p.returncode, output))
    print("#################################################")
    return p.returncode

    
if __name__ == "__main__":
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        S1 Calibration, orhorectification, dem, slope, aspect and incidence angle processing
        """)
    
    parser.add_argument('-indir', action='store', required=True, help='Directory containing Sentinel1 .zip files')
    parser.add_argument('-inref', action='store', required=True, help='Image reference, generally corresponding to a S2 tile')
    parser.add_argument('-zone', action='store', required=True, help='Geographic zone reference for output files naming')
    parser.add_argument('-outdir', action='store', required=True, help='Output directory')
    parser.add_argument('-polar', choices=['vv', 'vh', 'vvvh'],  default='vv', required=False, help='[Optional] Process VV, VH or both polarization, default vv')
    parser.add_argument('-theta', choices=['loc', 'elli'],  default='loc', required=False, help='[Optional]Create local incidence raster or incidence raster from ellipsoid, default loc')
    parser.add_argument('-overwrite', dest='overwrite', action='store_true', default=False, required=False, help='[Optional] Overwrite already existing files (default False)')
    
    
    args=parser.parse_args()
    
    cmd=["python", "manage.py", "runscript", "ServiceWrapper", "--traceback", "--script-args",
          "calibration", 
          "indir@"+args.indir,
          "inref@"+args.inref,
          "mrgstile@"+args.zone,
          "outdir@"+args.outdir,
          "polar@"+args.polar,
          "theta@"+args.theta,
          "overwrite@"+str(int(args.overwrite))
        ]
    process_command(cmd)
    
    

    
        
        
    
