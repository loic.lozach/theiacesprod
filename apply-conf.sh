#!/bin/bash
EOYML=theiacesprodapp/backend/service/resources/user_conf.yml
CONF=config.ini


if test -f "$EOYML"; then
    rm -f $EOYML
fi
cp -f ${EOYML}.model ${EOYML}

source <(cat ${CONF}  | sed -e '/^#/d;/^\s*$/d' -e "s/'/'\\\''/g" -e "s/=\(.*\)/='\1'/g")


sed -i "s|SCIHUB_LOGIN|${SCIHUB_LOGIN}|" ${EOYML}
sed -i "s|SCIHUB_PASSWD|${SCIHUB_PASSWD}|" ${EOYML}
sed -i "s|THEIA_LOGIN|${THEIA_LOGIN}|" ${EOYML}
sed -i "s|THEIA_PASSWD|${THEIA_PASSWD}|" ${EOYML}