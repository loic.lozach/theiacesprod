#!/usr/local/bin/python
'''
Created on 13 nov. 2019

@author: loic
'''

import argparse, os
from subprocess import Popen, PIPE

PROJECTDIR="/work/python/"

def process_command(cmd):
    print("Starting : "+" ".join(cmd))
    
    cmd[1]=PROJECTDIR+cmd[1]

    p = Popen(cmd, stdout=PIPE)
#    p.wait()
    output = p.communicate()[0]
    if p.returncode != 0: 
        print("process failed %d : %s" % (p.returncode, output))
    print("#################################################")
    return p.returncode

class Nestedspace(argparse.Namespace):
    def __setattr__(self, name, value):
        if '.' in name:
            group,name = name.split('.',1)
            ns = getattr(self, group, Nestedspace())
            setattr(ns, name, value)
            self.__dict__[group] = ns
        else:
            self.__dict__[name] = value


if __name__ == "__main__":
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        Automatic production of THEIA CES products
        """)
    
    prod_parsers = parser.add_subparsers(help='Choose production over France (S2-L3A) or World Wide (S2-L2A)', dest="pipeline")

    ssmww_parser = prod_parsers.add_parser('ssmww', help="Automatic production of Soil Moisture maps with S2-L2A.")
    ssmww_parser.add_argument('-sd', action='store', metavar='YYYY-MM-DD', required=True, help="Starting date S1, ex. 2020-08-31")
    ssmww_parser.add_argument('-ed', action='store', metavar='YYYY-MM-DD', required=True, help="Ending date S1, ex. 2020-09-31")
    ssmww_parser.add_argument('-modeldir', action='store', required=True, help='Directory to find both "SavedModel_wet" and "SavedModel_dry" tensorflow model')
    # ssmww_parser.add_argument('-modelchoice', action='store', required=False, default="dry", help='[Optional] dry, wet or text file resulting from gpm pipeline, default dry')
    # ssmww_parser.add_argument('-outformat', choices=['raster', 'csv'],  default='raster', required=False, help='[Optional] Format of output when calling otbcli_InvertSARModel, cvs or raster')
    ssmww_parser.add_argument('-skipdown', choices=['s1', 's2', 'all', 'no'],  default='no', required=False, help='[Optional] Skip sentinel 1 or 2 or both downloading step (default no). This allows user to filter cloudy Sentinel2 images by removing them from project directory in order to obtain better quality on gapfilled NDVI. Also useful if offline images can not be retrieve.')
    ssmww_parser.add_argument('-overwrite', dest='overwrite', action='store_true', default=False, required=False, help='[Optional] Overwrite already existing files (default False)')
    ssmww_parser.add_argument('-v', dest='debug', action='store_true', default=False, required=False, help='[Optional] Change logging level to DEBUG (default False)')

    ssmww_parser.add_argument('-agriref', choices=['vector', 'raster'], required=True, help='Choose between agricultural plot reference vector file or Copernicus landcover map')

    ssmww_parser.add_argument('-agri.vect.file', dest='agri.vect.file', metavar='FILE', required=False, help='Agricultural plot reference vector file, required if agriref=vector')
    ssmww_parser.add_argument('-agri.vect.idfield', dest='agri.vect.idfield', metavar='STRING', required=False, default="ID_PARCEL", help='Labels field name , default ID_PARCEL (France RPG 2019 shapefile)')
    ssmww_parser.add_argument('-agri.vect.lufield', dest='agri.vect.lufield', metavar='STRING', required=False, default="CODE_GROUP", help='LandUse field name , default CODE_GROUP (France RPG 2019 shapefile)')
    ssmww_parser.add_argument('-agri.vect.luvalues', dest='agri.vect.luvalues', metavar='LIST', required=False, default="all", help='List of agricultural areas values in LandUse vector, default "all"(all CODE_GROUP in France RPG 2019 shapefile), ex: "1 2 3 4" ')
    ssmww_parser.add_argument('-agri.vect.clipchoice', choices=['s2tile', 'none'], required=False, help='Choose between clippng with a sentinel2 tile (s2tile) or use whole vector file (none), area size must be under half S2 tile (<5000km2).')
    ssmww_parser.add_argument('-agri.vect.clip.s2tile', dest='agri.vect.clip.s2tile', metavar='T##XXX', required=False, help='Clip agricultural plot reference vector with Sentinel2 Tile, ex: T30UVU')
    
    ssmww_parser.add_argument('-agri.rast.file', dest='agri.rast.file', metavar='FILE', required=False, help='Copernicus landcover map at 20x20 degree tiles raster file and 100 meters pixel resolution, required if agriref=raster')
    ssmww_parser.add_argument('-agri.rast.luvalues', dest='agri.rast.luvalues', metavar='LIST', required=False, default="30 40", help='List of agricultural areas values in Copernicus landuse map, default "30 40"(grassland and crops)')
    ssmww_parser.add_argument('-agri.rast.clipchoice', choices=['s2tile', 'aoi'], required=False, help='Choose between clippng with a sentinel2 tile (s2tile) or a polygon vector file (aoi), area size must be under half S2 tile (<5000km2).')
    ssmww_parser.add_argument('-agri.rast.clip.s2tile', dest='agri.rast.clip.s2tile', metavar='T##XXX', required=False, help='Clip landcover map over Sentinel2 Tile, ex: T30UVU')
    ssmww_parser.add_argument('-agri.rast.clip.aoi', dest='agri.rast.clip.aoi', metavar='FILE', required=False, help="An user's AOI vector file, area size must be under half S2 tile (<5000km2).")
    

    ssmfr_parser = prod_parsers.add_parser('ssmfr', help="Automatic production of Soil Moisture maps over France with S2-L3A and RPG agricultural reference vector.")
    ssmfr_parser.add_argument('-sd', action='store', metavar='YYYY-MM-DD', required=True, help="Starting date S1, ex. 2020-08-31")
    ssmfr_parser.add_argument('-ed', action='store', metavar='YYYY-MM-DD', required=True, help="Ending date S1, ex. 2020-09-31")
    ssmfr_parser.add_argument('-modeldir', action='store', required=True, help='Directory to find both "SavedModel_wet" and "SavedModel_dry" tensorflow model')
    ssmfr_parser.add_argument('-modelchoice', action='store', required=False, default="dry", help='[Optional] dry, wet or text file resulting from gpm pipeline, default dry')
    ssmfr_parser.add_argument('-skipdown', choices=['s1', 's2', 'all', 'no'],  default='no', required=False, help='[Optional] Skip sentinel 1 or 2 or both downloading step (default no). This allows user to filter cloudy Sentinel2 images by removing them from project directory in order to obtain better quality on gapfilled NDVI. Also useful if offline images can not be retrieve.')
    ssmfr_parser.add_argument('-addtodb', dest='addtodb', action='store_true', required=False, help='[Optional] Add soilmoisture results in database (default False)')
    ssmfr_parser.add_argument('-overwrite', dest='overwrite', action='store_true', default=False, required=False, help='[Optional] Overwrite already existing files (default False)')
    ssmfr_parser.add_argument('-v', dest='debug', action='store_true', default=False, required=False, help='[Optional] Change logging level to DEBUG (default False)')

    ssmfr_parser.add_argument('-agri.vect.file', dest='agri.vect.file', metavar='FILE', required=True, help='Reseau Parcellaire Graphique (RPG) vector file')
    ssmfr_parser.add_argument('-agri.vect.pubyear', dest='agri.vect.pubyear', metavar="AAAA", required=True, help='Year of publication of RPG')
    ssmfr_parser.add_argument('-agri.vect.s2tile', dest='agri.vect.s2tile', metavar='T##XXX', required=True, help='Clip agricultural plot reference vector with Sentinel2 Tile, ex: T30UVU')
    ssmfr_parser.add_argument('-agri.vect.idfield', dest='agri.vect.idfield', metavar='STRING', required=False, default="ID_PARCEL", help='Labels field name , default ID_PARCEL (France RPG 2019 shapefile)')
    ssmfr_parser.add_argument('-agri.vect.lufield', dest='agri.vect.lufield', metavar='STRING', required=False, default="CODE_GROUP", help='LandUse field name , default CODE_GROUP (France RPG 2019 shapefile)')
    ssmfr_parser.add_argument('-agri.vect.luvalues', dest='agri.vect.luvalues', metavar='LIST', required=False, default="all", help='List of agricultural areas values in LandUse vector, default "all"(all CODE_GROUP in France RPG 2019 shapefile), ex: "1 2 3 4" ')
    
    
    args = parser.parse_args(namespace=Nestedspace())
    
    cmd=["python", "manage.py", "runscript", "ServiceWrapper", "--script-args"] #"--traceback",
         
    if args.pipeline == 'ssmww' :
        if args.agriref == "vector":
            if args.agri.vect.file == None:
                print("Error: Argument -agri.vect.file is required!")
                exit(1)
            if args.agri.vect.clipchoice == None:
                print("Error: Argument -agri.vect.clipchoice is required!")
                exit(1)

            if args.agri.vect.clipchoice == 's2tile':
                if args.agri.vect.clip.s2tile == None:
                    print("Error: Argument -agri.vect.clip.s2tile is required!")
                    exit(1)

                cmd += [
                    "autossmww",
                    "agriref@" +args.agriref,
                    "zone@"+args.agri.vect.clipchoice,
                    "aoifile@"+"none",
                    "landusemap@"+"none",
                    "s2tile@"+args.agri.vect.clip.s2tile,
                    "sd@"+args.sd,
                    "ed@"+args.ed,
                    "rgpshp@"+args.agri.vect.file,
                    "labelsfield@"+args.agri.vect.idfield,
                    "landusefield@"+args.agri.vect.lufield,
                    "agrivalues@"+args.agri.vect.luvalues,
                    "modeldir@"+args.modeldir,
                    "modelchoice@"+"dry",
                    "slopevalue@"+str(20), 
                    "sarmode@"+"vv",
                    "outformat@"+"raster",
                    "skipdownloads@" +args.skipdown,
                    "overwrite@"+str(int(args.overwrite)),
                    "debug@"+str(int(args.debug))
                    ]
                process_command(cmd)

            elif args.agri.vect.clipchoice == 'none':
                if args.agri.vect.clip.s2tile != None:
                    print("Error: Argument -agri.vect.clip.s2tile is not required!")
                    exit(1)

                cmd += [
                    "autossmww",
                    "agriref@" +args.agriref,
                    "zone@"+args.agri.vect.clipchoice,
                    "aoifile@"+"none",
                    "landusemap@"+"none",
                    "s2tile@"+"none",
                    "sd@"+args.sd,
                    "ed@"+args.ed,
                    "rgpshp@"+args.agri.vect.file,
                    "labelsfield@"+args.agri.vect.idfield,
                    "landusefield@"+args.agri.vect.lufield,
                    "agrivalues@"+args.agri.vect.luvalues,
                    "modeldir@"+args.modeldir,
                    "modelchoice@"+"dry",
                    "slopevalue@"+str(20), 
                    "sarmode@"+"vv",
                    "outformat@"+"raster",
                    "skipdownloads@" +args.skipdown,
                    "overwrite@"+str(int(args.overwrite)),
                    "debug@"+str(int(args.debug))
                    ]
                process_command(cmd)
            
        elif args.agriref == "raster":
            if args.agri.rast.file == None:
                print("Error: Argument -agri.rast.file is required!")
                exit(1)
            if args.agri.rast.clipchoice == None:
                print("Error: Argument -agri.rast.clipchoice is required!")
                exit(1)

            if args.agri.rast.clipchoice == 's2tile':
                if args.agri.rast.clip.s2tile == None:
                    print("Error: Argument -agri.rast.clip.s2tile is required!")
                    exit(1)
                if args.agri.rast.clip.aoi != None:
                    print("Error: Argument -agri.rast.clip.aoi is not required!")
                    exit(1)

                cmd += [
                    "autossmww",
                    "agriref@" +args.agriref,
                    "zone@"+args.agri.rast.clipchoice,
                    "aoifile@"+"none",
                    "landusemap@"+args.agri.rast.file,
                    "s2tile@"+args.agri.rast.clip.s2tile,
                    "sd@"+args.sd,
                    "ed@"+args.ed,
                    "rgpshp@"+"none",
                    "labelsfield@"+"none",
                    "landusefield@"+"none",
                    "agrivalues@"+args.agri.rast.luvalues,
                    "modeldir@"+args.modeldir,
                    "modelchoice@"+"dry",
                    "slopevalue@"+str(20), 
                    "sarmode@"+"vv",
                    "outformat@"+"raster",
                    "skipdownloads@" +args.skipdown,
                    "overwrite@"+str(int(args.overwrite)),
                    "debug@"+str(int(args.debug))
                    ]
                process_command(cmd)

            elif args.agri.rast.clipchoice == 'aoi':
                if args.agri.rast.clip.s2tile != None:
                    print("Error: Argument -agri.rast.clip.s2tile is not required!")
                    exit(1)
                if args.agri.rast.clip.aoi == None:
                    print("Error: Argument -agri.rast.clip.aoi is required!")
                    exit(1)

                cmd += [
                    "autossmww",
                    "agriref@" +args.agriref,
                    "zone@"+args.agri.rast.clipchoice,
                    "aoifile@"+args.agri.rast.clip.aoi,
                    "landusemap@"+args.agri.rast.file,
                    "s2tile@"+"none",
                    "sd@"+args.sd,
                    "ed@"+args.ed,
                    "rgpshp@"+"none",
                    "labelsfield@"+"none",
                    "landusefield@"+"none",
                    "agrivalues@"+args.agri.rast.luvalues,
                    "modeldir@"+args.modeldir,
                    "modelchoice@"+"dry",
                    "slopevalue@"+str(20), 
                    "sarmode@"+"vv",
                    "outformat@"+"raster",
                    "skipdownloads@" +args.skipdown,
                    "overwrite@"+str(int(args.overwrite)),
                    "debug@"+str(int(args.debug))
                    ]
                process_command(cmd)

        

    elif args.pipeline == 'ssmfr' :
        cmd += [
            "autossmfr", 
            "agriref@" +"vector",
            "zone@"+"s2tile",
            "aoifile@"+"none",
            "landusemap@"+"none",
            "s2tile@"+args.agri.vect.s2tile,
            "sd@"+args.sd,
            "ed@"+args.ed,
            "rgpshp@"+args.agri.vect.file,
            "rpg_pub_year@"+args.agri.vect.pubyear,
            "labelsfield@"+args.agri.vect.idfield,
            "landusefield@"+args.agri.vect.lufield,
            "agrivalues@"+args.agri.vect.luvalues,
            "modeldir@"+args.modeldir,
            "modelchoice@"+args.modelchoice,
            "slopevalue@"+str(20), 
            "sarmode@"+"vv",
            "outformat@"+"csv",
            "skipdownloads@" +args.skipdown,
            "addtodb@" +str(int(args.addtodb)),
            "overwrite@"+str(int(args.overwrite)),
            "debug@"+str(int(args.debug))
            ]
        process_command(cmd)

    
        
        