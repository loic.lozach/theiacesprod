#FROM registry.gitlab.com/latelescop/docker/otbtf/cpu:2.5-dev
FROM mdl4eo/otbtf:3.3.3-cpu-dev
SHELL ["/bin/bash", "-c"]

MAINTAINER Loic Lozach <loic.lozach[at]irstea[dot]fr>

ARG UID
ARG GID
ARG VBOX
# ----------------------------------------------------------------------------
# Build geotiff 1.7
# ----------------------------------------------------------------------------
USER root
RUN apt-get update && apt-get install -y python3-dev libpq-dev
# RUN mkdir /opt/geotiff && cd /opt/geotiff \
# 	&& git clone https://github.com/OSGeo/libgeotiff.git . \
# 	&& git checkout tags/1.7.0 \
# 	&& cd libgeotiff \
# 	&& ./autogen.sh \
# 	&& ./configure \
# 	&& make dist \
# 	&& tar xvzf libgeotiff-1.7.0.tar.gz \
# 	&& cd libgeotiff-1.7.0 \
# 	&& mkdir build_autoconf \
# 	&& cd build_autoconf \
# 	&& CFLAGS="-Wall -Wextra -Werror" ../configure \
# 	&& make -j3 \
# 	&& make check \
# 	&& cd .. \
# 	&& mkdir build_cmake \
# 	&& cd build_cmake \
# 	&& cmake .. -DCMAKE_C_COMPILER_LAUNCHER=ccache -DCMAKE_C_FLAGS="-Wall -Wextra -Werror" \
# 	&& make -j3 \
# 	&& cd ../.. \
# 	&& make install


# ----------------------------------------------------------------------------
# Build OTB SoilMoisture
# ----------------------------------------------------------------------------

ENV GIT_SSL_NO_VERIFY=true

#checkout OTB tag 8.1.1
RUN cd /src/otb/otb/ \
 && git fetch \
 && git switch --detach 8.1.1

RUN cd /src/otb/otb/Modules/Remote \
 && git clone https://gitlab.irstea.fr/loic.lozach/AgriSoilMoisture.git \
 && git clone https://gitlab.irstea.fr/loic.lozach/agrifrozenareas.git \
 && cd /src/otb/otb/Modules/Remote/AgriSoilMoisture \
 && git checkout otbtf-3.3.3
 
RUN cd /src/otb/build/OTB/build \
 && cmake /src/otb/otb \
 	# -DGEOTIFF_LIBRARY=/usr/local/lib/libgeotiff.so \
 	# -DGEOTIFF_INCLUDE_DIR=/usr/local/include/ \
 	-DCMAKE_INSTALL_PREFIX=/opt/otbtf \
	-DModule_AgriSoilMoisture=ON \
	-DModule_AgriFrozenAreas=ON \
	-DModule_OTBTemporalGapFilling=ON \
	-DModule_OTBQGIS:BOOL=OFF \ 
	-DOTBQGIS_IS_TEST:BOOL=ON \
 && make install -j $(grep -c ^processor /proc/cpuinfo)


RUN mkdir -p /work/python
ADD requirements.txt /work/python
WORKDIR /work/python

COPY . /work/python

#RUN source useruid.txt \
RUN usermod otbuser -u $UID \
    && groupmod otbuser -g $GID \
    && chown -R otbuser:otbuser /home/otbuser \
    && mkdir /data && chown otbuser:otbuser /data \
    && chown otbuser:otbuser -R /work/python
RUN if [[ ! -z "$VBOX" ]] ; then groupadd -g $VBOX vboxsf && usermod -a -G vboxsf otbuser ; fi

ENV PYTHONPATH=/work/python:/work/python/ausemdom:$PYTHONPATH
ENV PATH=$PATH:/work/python:/home/otbuser/.local/bin
# ENV GDAL_DATA=/usr/share/gdal/2.2/
# ENV GDAL_DRIVER_PATH="disable"
# ENV LC_NUMERIC=C
# ENV PROJ_LIB=/usr/share/proj
# ENV OTB_MAX_RAM_HINT=512
RUN alias ll='ls -al'

RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt --upgrade

USER otbuser

#CDS ERA5-T API ACCES
RUN echo "url: https://cds.climate.copernicus.eu/api/v2" > $HOME/.cdsapirc
RUN echo "key: 49622:2ddcb71d-95fd-4a6e-a07f-384902df2eed" >> $HOME/.cdsapirc
RUN sudo pip3 install cdsapi



