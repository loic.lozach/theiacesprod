UID := $(shell id -u)
GID := $(shell id -g)
VBOX := $(shell getent group vboxsf | cut -d: -f3) 
include config.ini
export


list:
	@echo ""
	@echo "Useful targets:"
	@echo ""
	@echo "  install          > Install or rebuild TheiaCESProd"
	@echo "  start            > Start containers"
	@echo "  dbmigrate        > Initialize database"
	@echo "  shell            > Shell into client container"
	@echo "  restart          > Restart containers"
	@echo "  stop             > Stop and kill running containers"
	@echo "  status           > Display stack containers status"
	@echo "  logs             > Display containers logs"
	@echo ""

start:
	@docker-compose up -d

restart: stop start

stop:
	@docker-compose kill
	@docker-compose rm -v --force

dbmigrate:
	@docker-compose exec webtheia python manage.py migrate

status:
	@docker-compose ps

logs:
	@docker-compose logs -f -t

install: check conf
	@docker-compose build --build-arg UID=${UID} --build-arg GID=${GID} --build-arg VBOX=${VBOX} --build-arg DATAPATH=${DATAPATH} --build-arg DBPATH=${DBPATH}

#--no-cache

shell:
	@docker-compose exec webtheia bash

conf:
	$(shell ./apply-conf.sh)
	
#rcopy:
#ifeq ("$(wildcard $(EOYML))","")
#	@cp -f ${EOYML}.model ${EOYML}
#else
#	@rm -f ${EOYML}
#	@cp -f ${EOYML}.model ${EOYML}
#endif

check:
#	@[ "${DATAPATH}" ] || ( $(error DATAPATH var is undefined. Edit config.ini file) )
	@: $(if ${DATAPATH},,$(error DATAPATH var is undefined. Edit config.ini file))
	@: $(if ${DBPATH},,$(error DBPATH var is undefined. Edit config.ini file))
        
#eodag:
#	@echo $(value ${SCIHUB_PASSWD})
#	@sed -i "s!SCIHUB_LOGIN!${SCIHUB_LOGIN}!" ${EOYML}
#	@sed -i "s!SCIHUB_PASSWD!${SCIHUB_PASSWD}!" ${EOYML}
#	@sed -i "s!PEPS_LOGIN!${PEPS_LOGIN}!" ${EOYML}
#	@sed -i "s!PEPS_PASSWD!${PEPS_PASSWD}!" ${EOYML}
#	@sed -i "s!THEIA_LOGIN!${THEIA_LOGIN}!" ${EOYML}
#	@sed -i "s!THEIA_PASSWD!${THEIA_PASSWD}!" ${EOYML}
