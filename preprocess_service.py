#!/usr/local/bin/python
# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.


import argparse
from subprocess import Popen, PIPE


PROJECTDIR="/work/python/"

def process_command(cmd):
    print("Starting : "+" ".join(cmd))
    
    cmd[1]=PROJECTDIR+cmd[1]

    p = Popen(cmd, stdout=PIPE)
#    p.wait()
    output = p.communicate()[0]
    if p.returncode != 0: 
        print("process failed %d : %s" % (p.returncode, output))
    print("#################################################")
    return p.returncode

    
    
if __name__ == "__main__":
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        Preprocessing Services
        """)

    subparsers = parser.add_subparsers(help='Preprocess pipeline', dest="pipeline")

    list_parser = subparsers.add_parser('ndvi', help="Compute cloud-masked NDVI from Sentinel2 images.")
    list_parser.add_argument('-s2dir', action='store', required=True, help='Directory containing Sentinel2 images directories')
    list_parser.add_argument('-zone', choices=['mrgs', 'aoi'], required=True, help='Process NDVI directory containing MRGS Sentinel2 Tiled files or AOI extracted S2 files')
    list_parser.add_argument('-zone.mrgs', action='store', required=False, help='MRGS Sentinel2 Tile name for output files naming, ex:T30UVU')
    list_parser.add_argument('-zone.aoi', action='store', required=False, help='[Optional] Set custom zone for NDVI files naming, otherwise automatically get autoprod zone name')
    # list_parser.add_argument('-format', choices=['S2-MUSCATE','S2-SEN2COR','S2-L3A'], required=True, help='Origin of archive creation, MUSCATE comes from PEPS ro Theia website, SEN2COR comes from Scihub website, L3A comes from Theia website') #,'L8-OLI/TIRS'
    list_parser.add_argument('-ndvidir', action='store', required=True, help='Output directory for computed NDVIs')
    list_parser.add_argument('-overwrite', dest='overwrite', action='store_true', default=False, required=False, help='[Optional] Overwrite already existing files (default False)')
    
    list_parser = subparsers.add_parser('gapfilling', help="Perform Gapfilling over NDVI time serie.")
    list_parser.add_argument('-ndvidir', action='store', required=True, help='Directory containing time serie NDVI to be gapfilled')
    list_parser.add_argument('-outmaskdir', action='store', required=True, help='Output directory for NDVIs mask used for gapfilling')
    list_parser.add_argument('-interpolation', choices=['linear', 'spline'], default='linear', required=False, help='Interpolation mode for OTB ImageTimeSeriesGapFilling')
    list_parser.add_argument('-outdir', action='store', required=True, help='Output directory for time serie gapfilled NDVIs image')
    list_parser.add_argument('-overwrite', dest='overwrite', action='store_true', default=False, required=False, help='[Optional] Overwrite already existing files (default False)')
    
    # RGP
    list_parser = subparsers.add_parser('processrgp', help="Create plot label image and agricultural mask from RGP")
    list_parser.add_argument('-rgpshp', action='store', required=True, help='RGP (Reseau Graphique Parcellaire) shapefile or geopackage')
    list_parser.add_argument('-labelsfield', action='store', required=False, default="ID_PARCEL", help='[Optional] Labels field name , default ID_PARCEL (France RPG 2019 shapefile)')
    list_parser.add_argument('-landusefield', action='store', required=False, default="CODE_GROUP", help='[Optional] LandUse field name , default CODE_GROUP (France RPG 2019 shapefile)')
    list_parser.add_argument('-agrivalues', action='store', required=False, default="all", help='[Optional] List of agricultural areas values in LandUse raster, default "all"(all CODE_GROUP in France RPG 2019 shapefile), ex: "1 2 3 4" ')
    list_parser.add_argument('-inref', action='store', required=True, help='Image reference for RGP extraction')
    list_parser.add_argument('-zone', action='store', required=True, help='Geographic Sentinel2 zone reference for output files naming')
    list_parser.add_argument('-outdir', action='store', required=True, help='Output directory for masked labels images')
    list_parser.add_argument('-overwrite', dest='overwrite', action='store_true', default=False, required=False, help='[Optional] Overwrite already existing files (default False)')
    
    
    # Stack and mask before segm pipeline
    list_parser = subparsers.add_parser('stack', help="Create stacked and masked image from NDVI directory to the input of a segmentation process")
    list_parser.add_argument('-ndvidir', action='store', required=True, help='Directory to find all the NDVI file to stack. Files naming must contain "NDVI" and ".tif" extension')
    list_parser.add_argument('-lulc', action='store', required=True, help='LandUseLandCover raster to be used for labels masking (Default THEIA OSO)')
    list_parser.add_argument('-agrivalues', action='store', required=False, help='[Optional] List of agricultural areas values in LandUseLandCover raster, default "11 12"')
    list_parser.add_argument('-output', action='store', required=True, help='Stacked and masked NDVIs output file name')
    
    # Extract SRTM1SecHGT from Snap download and compute slope
    list_parser = subparsers.add_parser('slope', help="Extract SRTM1SecHGT with image ref and compute slope")
    list_parser.add_argument('-srtmdir', action='store', required=True, help='DEM SRTM 1Sec HGT directory containing zip files')
    list_parser.add_argument('-inref', action='store', required=True, help='Image reference for DEM extraction in projected reference system')
    list_parser.add_argument('-zone', action='store', required=True, help='Geographic Sentinel2 zone reference for output files naming')
    list_parser.add_argument('-outdir', action='store', required=True, help='Output directory for slope image')
    
    # Select wet or dry model for inversion from GPM data
    #WARNING: adresse gpm nasa a modifier (voir mail actu gpm) 
    list_parser = subparsers.add_parser('gpm', help="Downloads, extracts and computes synthesis of NASA Global Precipitation Model raster for TF model selection (wet or dry)")
    list_parser.add_argument('-s1orthodir', action='store', required=True, help='Directory to find orthorectified Sentinel-1 images')
    list_parser.add_argument('-gpmdir', action='store', required=True, help='Directory where GPM data will be downloaded, autodetect previous downloads')
    list_parser.add_argument('-zone', action='store', required=True, help='Geographic zone reference for output files naming')
    list_parser.add_argument('-outtxt', action='store', required=True, help='Output text file where each Sentinel-1 dates correspond their model dry or wet to use')
    
    
    args=parser.parse_args()
    

    if args.pipeline == 'processrgp' or args.pipeline == 'slope' or args.pipeline == 'gpm' or args.pipeline == 'exmosS1':
        if args.zone != None and args.zone.find("_") >= 0 :
            print("Error: '_' character is forbidden in -zone string")
            exit()
        
    if args.pipeline == 'ndvi' :
        if args.zone == "mrgs":
            if args.zone.mrgs == None:
                print("Error: Argument -zone.mrgs  is required!")
                exit(1)
        print("Starting NDVI computation")
        if args.zone == "aoi":
            thezone = "aoi"
        else:
            thezone = args.zone.mgrs
        cmd=["python", "manage.py", "runscript", "ServiceWrapper", "--traceback", "--script-args",
          "ndvi", 
          "zipdir@"+args.s2dir,
          # "format@"+args.format,
          "mrgstile@"+thezone,
          "ndvidir@"+args.ndvidir,
          "overwrite@"+str(int(args.overwrite))
        ]
        process_command(cmd)
        
    elif args.pipeline == 'gapfilling' :
        print("Starting NDVI Gapfilling")
        cmd=["python", "manage.py", "runscript", "ServiceWrapper", "--traceback", "--script-args",
          "gapfill", 
          "ndvidir@"+args.ndvidir,
          "outmaskdir@"+args.outmaskdir,
          "interpolation@"+args.interpolation,
          "outdir@"+args.outdir,
          "overwrite@"+str(int(args.overwrite))
        ]
        process_command(cmd)
    
    elif args.pipeline == 'processrgp' :
        print("Starting RGP processing")
        cmd=["python", "manage.py", "runscript", "ServiceWrapper", "--traceback", "--script-args",
          "processrgp", 
          "rgpshp@"+args.rgpshp,
          "inref@"+args.inref,
          "mrgstile@"+args.zone,
          "outdir@"+args.outdir,
          "labelsfield@"+args.labelsfield,
          "landusefield@"+args.landusefield,
          "agrivalues@"+args.agrivalues,
          "overwrite@"+str(int(args.overwrite))
        ]
        process_command(cmd)
            
    elif args.pipeline == 'stack' :
        print("Starting Stack NDVI")
        cmd=["python", "manage.py", "runscript", "ServiceWrapper", "--traceback", "--script-args",
          "stack", 
          "ndvidir@"+args.ndvidir,
          "lulc@"+args.lulc,
          "agrivalues@"+args.agrivalues,
          "outdir@"+args.outdir,
        ]
        process_command(cmd)

    elif args.pipeline == 'slope' :
        print("Starting slope generation")
        cmd=["python", "manage.py", "runscript", "ServiceWrapper", "--traceback", "--script-args",
          "ndvi", 
          "srtmdir@"+args.srtmdir,
          "inref@"+args.inref,
          "mrgstile@"+args.zone,
          "outdir@"+args.outdir
        ]
        process_command(cmd)

    elif args.pipeline == 'gpm' :
        print("Starting Model Choice from GPM")
        cmd=["python", "manage.py", "runscript", "ServiceWrapper", "--traceback", "--script-args",
          "gpm", 
          "s1orthodir@"+args.s1orthodir,
          "gpmdir@"+args.gpmdir,
          "mrgstile@"+args.zone,
          "outtxt@"+args.outtxt
        ]
        process_command(cmd)

    else:
        print("Error: wrong pipeline argument "+ args.pipeline )
        parser.exit()
        
        
        
        
        
        